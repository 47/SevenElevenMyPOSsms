<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
<!--
table{
border-collapse: collapse;
}
th,td{
border: thin solid black;
width:150px;
}
-->
</style>
  </head>
  
<body>
       <table>
       <tr><th>用户id</th><th>用户No.</th><th>用户名</th><th>电话</th><th>地址</th><th>修改</th><th>删除</th></tr>
       <s:iterator value="#request['customers']" id="customer"> 
       <tr><td><s:property value="#customer.customerId"/></td>
       <td><s:property value="#customer.customerNo"/></td>
       <td><s:property value="#customer.customerName"/></td>
       <td><s:property value="#customer.telephone"/></td>
       <td><s:property value="#customer.address"/></td>
       <td><a href="detailCustomer.action?customer.customerId=<s:property value="#customer.customerId"/>">修改</a></td>
       <td><a href="deleteCustomer.action?customer.customerId=<s:property value="#customer.customerId"/>"  onclick="if(!confirm('你确定要删除吗？')){return false;};">确认删除</a></td>
       </tr>
       </s:iterator>
        </table>
        <!-- <br /><a href="adminlogin_success.jsp">管理员首页</a> -->
</body>
</html>
