<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
<!--
table{
border-collapse: collapse;
}
th,td{
border: thin solid black;
width:150px;
}
-->
</style>
  </head>
  
<body>
       <table>
       <tr><th>商品类别ID</th><th>商品类别名</th><th>描述</th><th>修改</th><th>删除</th></tr>
       <s:iterator value="#request['producttypes']" id="producttype"> 
       <tr><td><s:property value="#producttype.producttypeId"/></td>
       <td><s:property value="#producttype.producttypeName"/></td>
       <td><s:property value="#producttype.description"/></td>
       <td><a href="detailProducttype.action?producttype.producttypeId=<s:property value="#producttype.producttypeId"/>">修改</a></td>
       <td><a href="deleteProducttype.action?producttype.producttypeId=<s:property value="#producttype.producttypeId"/>"  onclick="if(!confirm('你确定要删除吗？')){return false;};">确认删除</a></td>
       </tr>
       <%-- <tr>
       <s:iterator value="#producttype.products" id="product">
       <td><s:property value="#product.productId"/></td>
       <td><s:property value="#product.productName"/></td>
       <td><s:property value="#product.productNo"/></td>
       </s:iterator>
       </tr> --%>
       </s:iterator>
        </table>
        <!-- <br /><a href="adminlogin_success.jsp">管理员首页</a> -->
</body>
</html>
