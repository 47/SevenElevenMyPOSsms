<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'showOrders.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
<!--
table{
border-collapse:collapse;
}
th{
border: thin solid black;
}
tr,td{
border: thin solid black;
width:150px;
text-align:center;
height:50px;
}
-->
</style>
  </head>
  
  <body>
    <s:if test="#request.saleorders.size!=0">

        我的订单:
    <s:iterator value="#request.saleorders" id="saleorder"> 
    <table>
    <tr><th>单据编号</th><th>单据日期</th><th>结算时间</th><th>会员</th><th>收款人</th><th>订单总价</th><th>付款情况</th></tr>
    <tr><td><s:property value="#saleorder.saleorderNo"/></td><td><s:property value="#saleorder.createDatetime"/></td><td><s:property value="#saleorder.payment.createDatetime"/></td><td><s:property value="#saleorder.customer.customerName"/></td><td><s:property value="#saleorder.payment.user.username"/></td><td><s:property value="#saleorder.totalPrice"/></td><td><s:if test="#saleorder.payment==null"><input type="button" value="点击付款"></s:if><s:else>已付款</s:else></td></tr>
    <tr><th>商品编号</th><th>商品名称</th><th>商品单价</th><th>商品数量</th><th>商品总价</th></tr>
    <s:iterator value="#saleorder.saleorderitems" id="saleorderitem"> 
       <tr><td><s:property value="#saleorderitem.product.productNo"/></td><td><s:property value="#saleorderitem.product.productName"/></td>
       <td><s:property value="#saleorderitem.product.price"/></td>
       <td><s:property value="#saleorderitem.quantity"/></td>
       <td><s:property value="#saleorderitem.price"/></td>
       </tr>
       </s:iterator>
       </table>
       </br></br></br>
        </s:iterator>
            </s:if>
               <s:else>当前订单为空</s:else>
  </body>
</html>
