<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'showCart.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
<!--
table{
border-collapse:collapse;
}
tr,td{
border: thin solid black;
width:150px;
}
-->
</style>
  </head>
  
  <body>
    <s:if test="#session.cart.items.size!=0">

        购物车:
    <table>
     <tr><th>商品id</th><th>书名</th><th>类别名</th><th>单价格</th><th>总价格</th><th>数量</th><!-- <th>修改数量</th><th>删除商品</th> --></tr>
    <s:iterator value="#session.cart.items"> 
       <tr><td><s:property value="value.product.productId"/></td><td><s:property value="value.product.productName"/></td>
       <td><s:property value="value.product.producttype.producttypeName"/></td>
       <td><s:property value="value.product.price"/></td>
       <td><s:property value="value.price"/></td>
       <td><s:property value="value.quantity"/></td>
       <%-- <td><form action="updateCart.action">
       <input type="text" name="quantity"><input type="hidden" value="<s:property value="value.book.id"/>" name="bookid"><input type="submit" value="修改"> 
       </form></td>
       <td><a href="deleteCart.action?bookid=<s:property value="value.book.id"/>" onclick="if(!confirm('你确定要删除吗？')){return false;};">删除</a></td> --%>
       </tr>
       </s:iterator>
       <tr><td colspan="6" align=right>总价格为<s:property value="#session.cart.totalPrice"/>元</td>
       <!-- <td><a href="checkout.action">结算</a></td> -->
       </tr>
       </table>
               </s:if>
               <s:else>当前购物车为空</s:else>
  </body>
</html>
