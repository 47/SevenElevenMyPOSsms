<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>付款</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<base target="_self">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	div{
		margin-top:20px;
		height:20px;
		text-align:'center';
		font-size:20px;
		width:300px;
	}
	input{
		width:100px;
		height:38px;
		font-size:20px;
	}
	</style>
  </head>
  <script type="text/javascript"> 
  	function Load() { 
    	document.getElementById('totalPrice').innerText=window.dialogArguments[0] ; 
    	document.getElementById('discount').innerText=window.dialogArguments[1]+"%" ; 
    	document.getElementById('realtotalPrice').innerText=Math.round(window.dialogArguments[0]*window.dialogArguments[1]/100 * 100) / 100.0;
    	document.getElementById('cash').value=Math.round(window.dialogArguments[0]*window.dialogArguments[1]/100 * 100) / 100.0;
    } 
    function oddChange(){
    	document.getElementById('change').innerText=Math.round((document.getElementById('cash').value-window.dialogArguments[0]*window.dialogArguments[1]/100)* 100) / 100.0;
    }
    function pay(){
    	window.returnValue=Math.round(window.dialogArguments[0]*window.dialogArguments[1]/100 * 100) / 100.0;
    	window.close();
    }
    function cancelPay(){
    window.returnValue="no";
    	window.close();
    }
  </script> 
  <body onload="Load()" >
    <form  method="post">
  <div><label>总金额：</label><span id="totalPrice"></span></div>
<div><label>折扣：</label><span id="discount"></span></div>
<div><label>  实收：</label><span id="realtotalPrice"></span></div>
<div><label> 收款金额：</label><input id="cash" type="text" onchange="oddChange()"></div>
<div><label> 找零金额：</label><span id="change">0.0</span></div>
   <div> <input id="submit" type="button" value="确定"  onclick="pay()">
    <input type="button" value="取消"  onclick="cancelPay()"></div>
    
    </form>
  </body>
</html>
