<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
		<script type="text/javascript">
/* 显示Dialog*/
        function openDialog(title){
            $("#dialog").dialog({
                resizable: false,
                modal: true,
                buttons: [{ //设置下方按钮数组
                    text: '保存',
                    iconCls: 'icon-save',
                    handler: function () {
                       save();
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        closeDialog();
                    }
                }]
            });
            $("#dialog").dialog('setTitle', title);
            $("#dialog").dialog('open');
        }
/* 关闭Dialog*/
        function closeDialog() {  
            $("#form").form('clear'); // 清空form的数据
            $("#dialog").dialog('close');// 关闭dialog
        }
/* 添加数据*/
        function add() {
            openDialog('添加客户'); // 显示添加数据dialog窗口
            $("#form").form('clear'); // 清空form的数据
            url = 'registerCustomer.action'; //后台添加数据action
        }
/* 修改数据*/
        function edit() {
            var row = $('#grid').datagrid('getSelected'); //// 得到选中的一行数据
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            openDialog('修改客户'); // 显示更新数据dialog窗口
            /* $("#form").form('load', row); // 加载选择行数据 */
            $("#form").form('load', { "customer.customerName":row.customerName,"customer.customerNo":row.customerNo,"customer.telephone":row.telephone,"customer.address":row.address }); // 加载选择行数据
            url = 'modifyCustomer.action?customer.customerId='+row.customerId; //后台更新数据action
        }
/* 保存数据*/
        function save(){
            $('#form').form('submit',{
                url: url,  //提交地址
                onSubmit: function(){
                    return $(this).form('validate'); //前台字段格式校验
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        closeDialog();// 调用closeDialog;    
                        reload();// 重新加载
                        $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        });
                    } else {              
                        $.messager.show({   //显示错误信息
                            title: '错误',
                            msg: result.msg
                        });
                    }
                }
            });
        }
/* 删除数据*/
        function remove(){
            var row = $('#grid').datagrid('getSelected');
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            $.messager.confirm('确认', '确定要删除吗？', function (r) {
                    if (r) {
                        //提交到后台的action
                        $.post('deleteCustomer.action', { id: row.customerId }, function (result) { 
                            if (result.success) {
                                reload();
                                $.messager.show({   //显示正确信息
                                    title: '提示',
                                    msg: result.msg
                                });                                 
                            } else {
                                $.messager.show({   //显示错误信息
                                    title: '错误',
                                    msg: result.msg
                                });
                            }
                        }, 'json');
                    }
                });
             
        }
/* 刷新grid*/
        function reload(){
            $('#grid').datagrid('reload');
        }
/* jquery入口*/
$(function() {
    loadgrid(); //加载datagrid
});
/* 加载datagrid列表*/
    function loadgrid(){
        $('#grid').datagrid({
        title : '客户',
        url : 'getCustomers.action',
        loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        fit : true,  //窗口自适应
        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        fitColumns : true, // 自动适应列宽
        singleSelect : true, // 每次只选中一行
        sortName : 'customerId', //默认排序字段
        sortOrder : 'asc', // 升序asc/降序desc
        striped : true,  // 隔行变色  
        pagination : true,  // 在底部显示分页工具栏
        pageNumber : 1, //初始化页码 
        pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        pageList : [ 5, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        //rownumbers : true, // 在最前面显示行号 
        idField : 'id', // 主键属性
        // 冻结列,当很多咧出现滚动条时该列不会动
        frozenColumns : [ [
        {title : '序号', width : '100', field : 'customerId', sortable : true}, 
        {title : '客户编号', width : '100', field : 'customerNo', sortable : true}, 
        {title : '客户姓名', width : '100',    field : 'customerName',sortable : true}
        ] ],
        columns : [ [ 
        {title : '联系电话', width : '100', field : 'telephone', sortable : false}, 
        {title : '联系地址', width : '200',    field : 'address', sortable : false}
        ] ],       
        // 工具栏按钮
        toolbar: [
"-", {id: 'add', text: '添加',    iconCls: 'icon-add', handler: function () { add()} },
"-", {id: 'edit', text: '修改',   iconCls: 'icon-edit',   handler: function () { edit()} }, 
"-", {id: 'remove', text: '删除',iconCls: 'icon-remove', handler: function () {remove()} }, 
"-", {id: 'reload',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload()} } 
        ]
    });
}
</script>
    </head>
    <body>
       <div style="width:100%;height:100%;padding:0px;overflow:hidden">
           <table id="grid"></table>
       </div>
       <div id="dialog" class="easyui-dialog" data-options="closed:true" title="客户管理" style="width:250px;height:200px;text-align:center" >
              <form id="form" method="post">
                     <div>
                        <label>客户编号</label>
                        <input name="customer.customerNo" data-options="field:'customerNo',required:true" />
                 </div>
                 <div>
                        <label>客户名称</label>
                        <input name="customer.customerName" data-options="field:'customerName',required:true" />
                 </div>
                 <div>
                        <label>联系电话</label>
                        <input name="customer.telephone" data-options="field:'telephone',required:false" />
                 </div>
                 <div>
                        <label>联系地址</label>
                        <input name="customer.address" data-options="field:'address',required:false"/>
                 </div>
              </form>
       </div>
    </body>
</html>