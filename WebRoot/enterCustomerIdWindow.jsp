<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>请问有会员卡吗</title>
    <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
    <script type="text/javascript" src="dwr/engine.js"></script>
	<script type="text/javascript" src="dwr/util.js"></script>
	<script type="text/javascript" src="dwr/interface/SaleorderDAOAjax.js"></script>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<base target="_self">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	div{
		margin-top:20px;
		height:20px;
		text-align:'center';
		font-size:20px;
		width:300px;
	}
	input{
		width:100px;
		height:38px;
		font-size:20px;
	}
	#submitId{
		width:50px;
		height:25px;
		font-size:12px;
		margin-top:3px;
		margin-left:5px;
	}
	</style>
  </head>
  <script type="text/javascript"> 
    function getCustomer(){
    	SaleorderDAOAjax.validateCustomer(document.getElementById('customerId').value,showCustomer);
    }
    function showCustomer(arr){
    	document.getElementById('customerName').innerText=arr[0] ; 
    	document.getElementById('discount').innerText=arr[1] ; 
    }
    function confirm(){
    	if(document.getElementById('customerName').innerText=="此用户不存在"||document.getElementById('customerId').value==0){
    		alert("此用户不存在");
    	}else{
    		window.returnValue=document.getElementById('customerId').value;
    		window.close();
    	}
    }
    function normal(){
    	window.returnValue=1;
    	window.close();
    }
    function cancel(){
    	window.returnValue="no";
    	window.close();
    }
  </script> 
  <body>
<!--     <form  method="post"> -->
    <div><input type="button" value="散客"  onclick="normal()"></div>
    <div>客户Id:：<input id="customerId" class="easyui-numberbox"  validType="length[0,3]" invalidMessage="不能超过3个字符！" type="text"><input id="submitId" type="button" value="确定"  onclick="getCustomer()"></div>
 	<div> 客户名：<span id="customerName"></span></div>
	<div>折扣：<span id="discount"></span></div>
    <div><input type="button" id="submit" value="确定"  onclick="confirm()">
    <input type="button" value="取消"  onclick="cancel()"></div>
    
   <!--  </form> -->
  </body>
</html>
