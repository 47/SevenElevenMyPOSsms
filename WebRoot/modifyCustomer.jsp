<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'test.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <s:set name="customer" value="#request['customer']"></s:set>
    <form action="modifyCustomer.action?customer.customerId=<s:property value="#customer.customerId"/>" method="post">
    客户号<input type="text" name="customer.customerNo" value="<s:property value="#customer.customerNo"/>"><br/>
    客户名<input type="text" name="customer.customerName" value="<s:property value="#customer.customerName"/>"><br/>
    电话<input type="text" name="customer.telephone" value="<s:property value="#customer.telephone"/>"><br/>
    地址<input type="text" name="customer.address" value="<s:property value="#customer.address"/>"><br/>
    <input type="submit" value="提交">
    
    </form>
  </body>
</html>
