<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'test.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <s:set name="product" value="#request['product']"></s:set>
    <form action="modifyProduct.action?product.productId=<s:property value="#product.productId"/>" method="post">
 商品名<input type="text" name="product.productName" value="<s:property value="#product.productName"/>"><br/>
产品号<input type="text" name="product.productNo" value="<s:property value="#product.productNo"/>"><br/>
  价格<input type="text" name="product.price" value="<s:property value="#product.price"/>"><br/>
  产品类别<select name="product.producttype.producttypeId">
  <option>-------</option>
  <s:iterator value="#request['producttypes']" id="producttype"> 
     <option value="<s:property value="#producttype.producttypeId"/>" <s:if test="#producttype.producttypeId==#product.producttype.producttypeId">selected</s:if>><s:property value="#producttype.producttypeName"/></option>
  </s:iterator>
  </select><br/>
   描述<input type="text" name="product.description" value="<s:property value="#product.description"/>"><br/>
    <input type="submit" value="提交">
    </form>
  </body>
</html>
