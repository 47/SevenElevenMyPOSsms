<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
		<script type="text/javascript">
			$(function(){
	
	/*修改datagrid单元格的样式*/
	/*function cellStyler(value,row,index){
		return 'font-weight:bold;';
	}*/
	
	$("#add_gender").combobox({
		required:true,
		valueField: 'label',
		textField: 'value',
		value:"男",
		data:[{
		    "label":"男",
		    "value":"男",
		},{
		    "label":"女",
		    "value":"女",
		}]
	});
	
	/*初始化添加会员的对话框*/
	$('#customer_add_dialog').dialog({
		closed:true,
		left:350,
		top:30,
	});
	
	/*输入框获取焦点*/
	$("#goods_input").focus();
	
	/*隐藏搜索框工具栏*/
	$("#customer_tb").hide();
	
	/*隐藏搜索框工具栏*/
	$("#tb_goods").hide();
	
	/*清空输入框*/
	function reset_goods_input(){
		$('#goods_input').val('');
    	$("#goods_input").focus();
	}
	
	/*显示时间*/
	setInterval(function() {
	    var now = (new Date()).toLocaleString();
	    $('#time').text(now);
	}, 1000);
	
	/*初始化选择会员对话框*/
	$('#select_customer').dialog({
		closed:true,
		left:55,
		top:5,
	});
	
	/*初始化结算对话框*/
	$('#checkout').dialog({
		closed:true,
		left:300,
		top:3,
	});
	
	/*初始化选择商品对话框*/
	$('#select_goods').dialog({
		closed:true,
		top:10,
	});
	
	/*初始化提取账单对话框*/
	$('#get_suspend_order').dialog({
		closed:true,
		top:10,
	});
	
	/*初始化小票对话框*/
	$('#receipt_dialog').dialog({
		closed:true,
		top:1,
	});
	
	/*初始化重打小票对话框*/
	$('#p_receipt').dialog({
		closed:true,
		top:1,
	});
	
	/*初始化数据表格*/
	loadgrid();
	
	/*加载表格*/
	function loadgrid(){
		var editRow = undefined;//进行编辑的行
		
		$('#checkout_grid').datagrid({
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'id',
	        sortOrder : 'asc',
	        striped : true,  // 隔行变色  
	        pagination : false,
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 每页10条记录
	        pageList : [ 10, 20, 30, 50 ],
	        idField : 'id', // 主键属性
            rownumbers : true,
            onDblClickCell: function () {
                var row = $("#checkout_grid").datagrid('getSelected');
                if (row !=null) {
                    if (editRow != undefined) {
                        $("#checkout_grid").datagrid('endEdit', editRow);
                        editRow = undefined;
                    }
                    if (editRow == undefined) {
                        var index = $("#checkout_grid").datagrid('getRowIndex', row);
                        $("#checkout_grid").datagrid('beginEdit', index);
                        editRow = index;
                    }
                } else {
                	$.messager.alert('警告','未选中商品！','info');
                }
            },//双击可进行单元格编辑
	        frozenColumns : [ [
               {
		            title : '商品编码', 
		            width : '90', 
		            field : 'id', 
		            sortable : false,
		            align:'center'
	            }, 
	            {
	            	title : '商品条码',
	            	width : '100', 
	            	field : 'barcode', 
	            	sortable : false,
	            	align:'center'
	            }, 
	            {
	            	title : '商品名称', 
	            	width : '170', 
	            	field : 'name',
	            	sortable : false,
	            	align:'center'
	            },
		        {
	            	title : '单位', 
	            	width:'80',
	            	field:'unit',
	            	sortable :false,
	            	align:'center'
	            },
		        {
	            	title : '售价', 
	            	width:'90',
	            	field:'price',
	            	sortable :false,
	            	align:'center'
	            },
		        {
	            	title : '数量', 
	            	width:'90',
	            	field:'amount',
	            	sortable :false, 
	            	editor:{type:'numberbox',options:{required:true,missingMessage:'请输入正整数'}},
	            	/*styler:cellStyler,*/
	            	align:'center',
	            },
		        {
	            	title : '金额', 
	            	width:'90',
	            	field:'totalprice',
	            	sortable :false,
	            	align:'center'
	            },
	        ] ],
	        columns : [ [ 
	            {
	            	title : '规格/备注', 
	            	width:'280',
	            	field:'description',
	            	sortable:false
	            }
	     	] ],
	     	
	     	toolbar:[ '-', {
	            text: '修改数量', iconCls: 'icon-edit', handler: function () {
	                var row = $("#checkout_grid").datagrid('getSelected');
	                if (row !=null) {
	                    if (editRow != undefined) {
	                        $("#checkout_grid").datagrid('endEdit', editRow);
	                        editRow = undefined;
	                    }
	                    if (editRow == undefined) {
	                        var index = $("#checkout_grid").datagrid('getRowIndex', row);
	                        $("#checkout_grid").datagrid('beginEdit', index);
	                        editRow = index;
//	                        $("#checkout_grid").datagrid('unselectAll');//暂时去掉
	                    }
	                } else {
	                	$.messager.alert('警告','未选中商品！','info');
	                }
	            }
	        },'-', {
	            text: '取消修改', iconCls: 'icon-redo', handler: function () {
	                $("#checkout_grid").datagrid('cancelEdit',editRow);
	                $("#checkout_grid").datagrid('unselectAll');
	            }
	        }, '-', {
	            text: '保存修改', iconCls: 'icon-save', handler: function () {
	            	var row = $("#checkout_grid").datagrid('getSelected');
	            	var change = $("#checkout_grid").datagrid('getChanges','updated');
//	            	console.log(typeof(change));
	            	if(row!=null){
	            		//结束编辑
	            		$("#checkout_grid").datagrid('endEdit', editRow);
	            		//更新行
	                	$('#checkout_grid').datagrid('updateRow',{
	 	            		index: editRow,
	 	            		row: {
	 	            			totalprice:parseInt(row.amount)*row.price,//修改金额
	 	            		}
	 	            	});
	                	$("#checkout_grid").datagrid('unselectAll');
	                	computeTotal();//计算应收金额、合计数量、合计金额
 	            	}
	            }
	        },'-', {
	            text: '删除行', iconCls: 'icon-clear', handler: function () {
	            	var row = $("#checkout_grid").datagrid('getSelected');
	            	if(row!=null){
		                var rowIndex=$('#checkout_grid').datagrid('getRowIndex',row);//获取index
		                $("#checkout_grid").datagrid('deleteRow',rowIndex);
		                computeTotal();//计算应收金额、合计数量、合计金额
 	            	}else{
	            		$.messager.alert('警告','请选择要删除的商品！','info');
	            	}
	            }
	        },
	        '-', {
	            text: '清空列表', iconCls: 'icon-cancel', handler: function () {
	            	$.messager.confirm('提示', '确定清空商品列表？', function(r){
	            		if (r){
	            			editRow = undefined;
//	    	                $("#checkout_grid").datagrid('rejectChanges');
	    	                $('#checkout_grid').datagrid('loadData',{total:0,rows:[]});//清空datagrid
	    	                $("#checkout_grid").datagrid('unselectAll');
	    	                computeTotal();//计算应收金额、合计数量、合计金额
	            		}
	            	});
	                
	            }
	        },
	        '-',{
	        	text: '数量+(ctrl+↑)', iconCls: 'icon-add', handler: function () {
	        		addAmount();//数量+1
	        	}
	        },
	        '-',{
	        	text: '数量-(ctrl+↓)', iconCls: 'icon-remove', handler: function () {
	        		subtractAmount();//数量-1
	        	}
	        }
	        ]
	    });
	}
	
	/*生成销售单号*/
	function gainOrder(){
		 $.ajax({
             type:'POST',
             url:'gainOrder.action',
             success:function(data){
            	 if (data.success){
            		 $('#orderid').attr('value',data.message);//把订单号填到
 				 }
             }
         });
	}
	
	
	/*打开结算窗口*/
	function open_checkout_Dialog(title){
		//获取支付单号
		gainPaymentId();
		//把应收金额填到文本框
	    $('#receivables').attr('value',$('#receivable').val());
	    computeCheckout();//结账计算
	    $("#checkout").dialog({
	        resizable: false,
	        modal: true,
	        buttons: [{
	            text: '结算',
	            iconCls: 'icon-save',
	            handler: function () {
	               //支付足够的金额才允许结算
	               if($('#change').val()!=""){
	            	   checkout();//结算
		               close_checkout_Dialog();
		               
	               }
	            }
	        }, {
	            text: '取消',
	            iconCls: 'icon-cancel',
	            handler: function () {
	            	close_checkout_Dialog();
	            	$("#goods_input").focus();//输入框获取焦点
	            }
	        }]
	    });
	    $("#checkout").dialog('setTitle', title);
	    $("#checkout").dialog('open');
	}
	
	/*打印小票*/
	function outputReceipt(){
		$("#receipt_table").find("tr").remove();//删除节点
		var rows = $('#checkout_grid').datagrid('getRows');
		$('#receipt_table').append('<tr><td>商品</td><td></td><td>单价</td><td>数量</td><td>金额</td></tr>');
		for (var i = 0; i < rows.length; i++) {
			 var tableRow = '<tr>' +
             '<td>' + rows[i]['barcode'] + '</td>' +
             '<td>' + rows[i]['name'] + '</td>' +
             '<td>' + rows[i]['price'].toFixed(2) + '</td>' +
             '<td>' + rows[i]['amount'] + '</td>' +
             '<td>' + rows[i]['totalprice'].toFixed(2) + '</td>' +
             '</tr>';
			 $('#receipt_table').find('tr').css('display','table-row');
			 $('#receipt_table').append(tableRow);
			
        }
		//把购物信息填写到小票
		$('#t_paymentId').html($('#checkout_id').val());
		$('#t_cashier').html($('#cashier').val());
		$('#t_discount').html($('#discount').val());
		$('#t_customer').html($('#customerId').val());
		$('#t_amount').html($('#amount').val());
		$('#t_totalprice').html(parseFloat($('#totalPrice').val()).toFixed(2));//总金额
		$('#t_receivables').html(parseFloat($('#receivables').val()).toFixed(2));//应收金额
		
		//积分
		if($('#customerId').val()!=""){
			$('#t_integral').html(parseInt($('#customerIntegral').val())+parseInt($('#receivables').val()));//积分
		}
		$('#t_realpay').html(parseFloat($('#accounts').val()).toFixed(2));//实收金额
		$('#t_change').html(parseFloat($('#change').val()).toFixed(2));//找零
		$('#t_time').html((new Date()).toLocaleString());//显示时间
		
		//小票对话框
		$('#receipt_dialog').dialog({
			resizable: false,
			title: "小票",
		    modal: true,
		});
		$('#receipt_dialog').dialog('open');
		
	}
	
	
	/*获取支付单号*/
	function gainPaymentId(){
		$.ajax({
            type:'POST',
            url:'gainPaymentId.action',
            success:function(data){
           	   if (data.success){
           	  	 $('#checkout_id').attr('value',data.message);//把支付单号填到表单
			   }
            }
        });
	}
	
	/*支付*/
	function pay(){
		var rows = $('#checkout_grid').datagrid('getRows');
        var rowObj = JSON.stringify(rows);//把json对象转为字符串
        $.ajax({
            type: "POST",
            url: "checkout.action",
            data: {
            	rowObj:rowObj,
            	cashierId:$('#cashierId').val(), //收银员
            	realPay:$('#accounts').val(), //实收金额
            	change:$('#change').val(), //找款
            	saleId:$('#orderid').val(), //销售单号
            	paymentId:$('#checkout_id').val(), //支付单号
            	paymentMethod:$('#checkout_way').val(), //支付方式
            },
            dataType: "json",
            success: function(data){
            	if (data.success){
					$.messager.show({
						timeout:1000,
						title: '提示',
						msg: data.message
					});
					close_checkout_Dialog();//关闭收银窗口
					resetOrder();//清空单据
					$("#accounts").val("");//清空实收金额
				} else {				
					$.messager.show({
						timeout:1000,
						title: '错误',
						msg: data.message
					});
				}
            }
        });
	}
	
	
	/*结算*/
	function checkout(){
		//销售开单，如果成功则进行支付
		makeSaleOrder();	
		
	}
	
	
	/*关闭结算窗口*/
	function close_checkout_Dialog() {  
		$("#checkout").dialog('close');
	}
	
	
	/*输入框回车事件*/
	$(function(){
        $('#goods_input').bind('keypress',function(event){
            if(event.keyCode == "13"){
            	//去掉默认的按enter键自动提交表单
            	event.preventDefault ? event.preventDefault() : (event.returnValue = false);
            	
            	var value = $("#goods_input");//获取输入框的值
                if (value.val() == "") {
                	//判断单据上是否有商品，有才能弹出结账窗口
                	var rows = $('#checkout_grid').datagrid('getRows');//获取当前的数据行
                	if(rows.length<=0){
                		$.messager.alert('警告','请录入要购买的商品！','info',function(){
            				$("#goods_input").select();//输入框获取焦点并全选文字
            			});
                	}else{
                		open_checkout_Dialog('结算'); 
                    	//实收金额输入框获取焦点
                	    $('#accounts').select();
                	}
                }else{
                	var input_val=$('#goods_input').val();
                	var url='searchGoodsById.action?barcode='+input_val;
                	$.getJSON(url,function(date){
                		if(date == "" || date == undefined || date == null){
                			$.messager.alert('警告','商品不存在！','info',function(){
                				$("#goods_input").select();//输入框获取焦点并全选文字
                			});
            			}else{
            				var rows = $('#checkout_grid').datagrid('getRows');//获取当前的数据行
                        	if(rows.length<=0){
                        		gainOrder();//生成订单号
//                        		$("#receipt_table").find("tr").remove();//删除节点
                        	}
            				$.each(date,function(index,value){
            					//判断是否已经添加此商品，是则数量+1，否则新增一行
            					if(isExist(value)){
            						
            					}else{
            						$('#checkout_grid').datagrid('insertRow',
                    					{
                        					index:0,
                        					row:{
                        						id:value.id,
                        						barcode:value.barcode,
                        						name:value.name,
                        						unit:value.unit,
                        						price:value.price,
                        						amount:value.amount,
                        						totalprice:value.totalprice,
                        						description:value.description,
                        					}
                    					}
                            		);
            					}
            					computeTotal();//计算应收金额、合计数量、合计金额
                     			reset_goods_input();//清空输入框
                    		});
            			}
                	});
                }
            }
        });
    });
	
	/*销售开单*/
	function makeSaleOrder(){
		var rows = $('#checkout_grid').datagrid('getRows');
        var rowObj = JSON.stringify(rows);//把json对象转为字符串
        var customerId=$('#customerId').textbox('getValue');//会员号
        if(customerId == "" || customerId == undefined || customerId == null){
        	customerId=0;//0代表不是会员
        }
		$.ajax({
            type: "POST",
            url: "makeSaleOrder.action",//销售开单
            data: {
            	rowObj:rowObj,
            	customerId:customerId,
            	totalPrice:$('#totalPrice').val(),//总金额
            	receivables:$('#receivables').val(), //应收金额
            	amount:$('#amount').val(),
            	saleId:$('#orderid').val(),
            },
            dataType: "json",
            success: function(data){
            	if (data.success){
            		pay();//支付
            		//判断是否打印小票
	                if ($('#outputReceipt').is(":checked")) { 
	            	    outputReceipt();//打印小票
	                } 
				} else {
					return;
				}
            }
        });
	}
	
	
	/*判断是否已经添加此商品*/
	function isExist(value){
		var rows = $('#checkout_grid').datagrid('getRows');
        for (var i = 0; i < rows.length; i++) {
        	if(rows[i]['id']==value.id){
        		//说明同一商品已经存在
        		rows[i]['amount']+=1;
        		$('#checkout_grid').datagrid('updateRow',{
            		index: i,
            		row: {
            			amount: rows[i]['amount'],
            			totalprice:rows[i]['amount']*rows[i]['price'],//修改金额
            		}
            	});
            	return true;
        	}
        }
	}
	
	/*合计数量的计算*/
	function computeAmount() {//计算合计数量的函数
        var rows = $('#checkout_grid').datagrid('getRows');//获取当前的数据行
        var amount = 0;//计算合计金额
        for (var i = 0; i < rows.length; i++) {
        	amount +=  parseInt(rows[i]['amount']);
        }
        $('#amount').attr('value',amount);//把数据填写到合计数量的文本框中
    }
	
	/*合计金额的计算*/
	 function computeTotalPrice() {//计算合计金额的函数
         var rows = $('#checkout_grid').datagrid('getRows');//获取当前的数据行
         var totalprice = 0;//计算合计金额
         for (var i = 0; i < rows.length; i++) {
        	 totalprice += rows[i]['totalprice'];
         }
         $('#totalPrice').attr('value',totalprice.toFixed(1));//把数据填写到合计金额的文本框中，并保留两位小数
     }
	 
	 /*计算应收金额*/
	 function computeReceivable(){
		 var totalprice =$('#totalPrice').val();
		 var discount =$('#discount').val();
		 if(discount == "" || discount == undefined || discount == null){
			 discount=1;
		 }
		 var receivable=totalprice*discount;
		 $('#receivable').attr('value',receivable.toFixed(1));//把数据填写到应收金额的文本框中，并保留两位小数
		 
	 }
	
	/*找款的计算*/
	 function computeCheckout(){
		var receivables=$('#receivables').val();//应收金额
		$("#change").attr("value","");
		$("#accounts").keyup(function(event){ 
			var accounts=$('#accounts').val();//实收金额
			var change=accounts-receivables;//找款
			if(change<0){
				$("#change").attr("value","");
			}else{
				$("#change").attr("value",change.toFixed(1));//保留两位小数
			}
		});  
	 }
	 
	 /*计算应收金额、合计数量、合计金额*/
	 function computeTotal(){
		 computeTotalPrice();//计算合计金额
		 computeAmount();//计算合计数量
		 computeReceivable();//计算应收金额
	 }
	 
	/*F4查找商品*/
	$(document).keypress(function(event) { 
		if (event.keyCode == "115"){
			open_search_goods("查找商品");
		} 
	}); 
	
	/*F2输入框获取焦点*/
	$(document).keypress(function(event) { 
		if (event.keyCode == "113"){
			$('#goods_input').focus();
		} 
	}); 
	
	/*数量+(ctrl+↑)快捷键*/
	$(document).keypress(function(event) { 
		if(event.ctrlKey && event.keyCode == "38") {
			addAmount();
		}
	}); 
	
	/*数量-(ctrl+↓)快捷键*/
	$(document).keypress(function(event) { 
		if(event.ctrlKey && event.keyCode == "40") {
			subtractAmount();
		}
	}); 
	
	/*查找会员快捷键(ctrl+→)*/
	$(document).keypress(function(event) { 
		if(event.ctrlKey && event.keyCode == "39") {
			open_select_customer("选择会员");
		}
	}); 
	
	
	/*F11全屏*/
	/*var flag=true;
	$(document).keypress(function(event) {
		var height=window.screen.height;//屏幕高度
		if (event.keyCode == "122"&&flag==true){
			console.log("1");
			console.log(height-289);
			var full=height-289;
			$('#checkout_grid_div').css("height",full+"px");
			flag=false;
		} 
	}); 
	$(document).keypress(function(event) {
		if (event.keyCode == "122"&&flag==false){
			console.log("2");
			$('#checkout_grid_div').css("height","310px");
			flag=true;
		} 
	});*/
	
	
	/*选择会员*/
	$('#customerId').textbox({
		icons: [{
			iconCls:'icon-add',
			handler: function(e){
				open_select_customer("选择会员");
			}
		}]
	});
	
	/*打开选择会员窗口*/
	function open_select_customer(title){
		//加载datagrid的内容
		get_customer();
		
	   $("#select_customer").dialog({
	       resizable: false,
	       modal: true,
	       buttons: [{
	           text: '选择',
	           iconCls: 'icon-save',
	           handler: function () {
	        	   choose_into_checkout();
	           }
	       }, {
	           text: '取消',
	           iconCls: 'icon-cancel',
	           handler: function () {
	        	   $("#select_customer").dialog('close');
	           }
	       }]
	   });
	   $("#select_customer").dialog('setTitle', title);
	   $("#select_customer").dialog('open');
	}
	
	
	/*选择会员，把会员的信息填到前台收银的表单里*/
	function choose_into_checkout(){
		var customer_row = $('#select_customer_grid').datagrid('getSelected'); // 得到选中的一行数据
	    if(customer_row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
		$("#customer_details").form('load', customer_row); // 加载选择行数据
		var discount=$("#hidden_discount").val();
		$("#discount").attr('value',discount);
		computeReceivable();//计算折扣后的实收金额
		$("#select_customer").dialog('close');
		$("#goods_input").focus();//输入框获取焦点
	}
	
	
	/*查找商品*/
	$(function(){
        $('#search_good').bind('click',function(){
        	open_search_goods("查找商品");
        });
    });
	
	/*打开查找商品的窗口*/
	function open_search_goods(title){
		//加载datagrid的内容
		get_goods('getAllGoods.action');
		
		$("#select_goods").dialog({
		       resizable: false,
		       modal: true,
		       buttons: [{
		           text: '选择',
		           iconCls: 'icon-save',
		           handler: function () {
		        	   choose_good();
		           }
		       }, {
		           text: '取消',
		           iconCls: 'icon-cancel',
		           handler: function () {
		        	   $("#select_goods").dialog('close');
		           }
		       }]
		   });
		   $("#select_goods").dialog('setTitle', title);
		   $("#select_goods").dialog('open');
	}
	
	/*注册新会员*/
	$(function(){
        $('#add_customer').bind('click',function(){
        	$("#form").form('clear');
    		open_add_Dialog('添加新会员'); 
    	    $('#add_discount').textbox('setValue','0.95');
    	    $('#add_integral').textbox('setValue','0');
    	    $("#add_gender").combobox('setValue','男');
    	    url = 'addCustomer.action';
        });
    });
	
	/*打开重打小票对话框*/
	function open_p_receipt(title){
	   $("#p_receipt").dialog({
	       resizable: false,
	       modal: true,
	       buttons: [{
	           text: '选择',
	           iconCls: 'icon-add',
	           handler: function () {
	              outputHistoryReceipt();//重打小票
	           }
	       }, {
	           text: '取消',
	           iconCls: 'icon-cancel',
	           handler: function () {
	        	   $("#p_receipt").dialog('close');
	           }
	       }]
	   });
	   $("#p_receipt").dialog('setTitle', title);
	   $("#p_receipt").dialog('open');
	}
	
	/*双击重打小票*/
	function outputHistoryReceipt(){
		var row = $('#receipt_grid').datagrid('getSelected'); // 得到选中的一行数据
	    if(row == null){
	        $.messager.alert("提示", "请选择要打印的单据",'info');
	        return;
	    }else{
	    	$.ajax({
                type: "POST",
                url: "outputReceipt.action",
                data: {
                	paymentId:row.paymentId,
                },
                dataType: "json",
                success: function(data){
                	if(data.success){
                		$("#receipt_table").find("tr").remove();//删除节点
                		//打印小票
                		$('#receipt_table').append('<tr><td>商品</td><td></td><td>单价</td><td>数量</td><td>金额</td></tr>');
                		for (var i = 0; i < data.goods.length; i++) {
                			 var tableRow = '<tr>' +
                             '<td>' + data.goods[i]['barcode'] + '</td>' +
                             '<td>' + data.goods[i]['name'] + '</td>' +
                             '<td>' + data.goods[i]['price'].toFixed(2) + '</td>' +
                             '<td>' + data.goods[i]['amount'] + '</td>' +
                             '<td>' + data.goods[i]['totalprice'].toFixed(2) + '</td>' +
                             '</tr>';
                			 $('#receipt_table').find('tr').css('display','table-row');
                			 $('#receipt_table').append(tableRow);
                			
                        }
                		//把购物信息填写到小票
                		$('#t_paymentId').html(data.saleDetails[0].paymentId);
                		$('#t_cashier').html(data.saleDetails[0].cashier);
                		$('#t_discount').html(data.saleDetails[0].discount);
                		$('#t_customer').html(data.saleDetails[0].customerId);
                		$('#t_amount').html(data.saleDetails[0].amount);
                		$('#t_totalprice').html(parseFloat(data.saleDetails[0].totalPrice).toFixed(2));//总金额
                		$('#t_receivables').html(parseFloat(data.saleDetails[0].receivables).toFixed(2));//应收金额
                		$('#t_integral').html(data.saleDetails[0].integral);//积分
                		$('#t_realpay').html(parseFloat(data.saleDetails[0].realPay));//实收金额
                		$('#t_change').html(parseFloat(data.saleDetails[0].changes).toFixed(2));//找零
                		$('#t_time').html(data.saleDetails[0].createTime);//显示时间
                		//小票对话框
                		$('#receipt_dialog').dialog({
                			resizable: false,
                			title: "小票",
                		    modal: true,
                		});
                		$('#receipt_dialog').dialog('open');
                		$('#p_receipt').dialog('close');
                		
                	}
                }
            });
	    }
	}
	
	/*重打小票*/
	$(function(){
		$('#regain_bill').bind('click',function(){
			open_p_receipt("销售记录");
			get_payments('getPayments.action');
			
        });
	});
	
	/*提取单据*/
	$(function(){
		$('#collect_order').bind('click',function(){
			open_suspend_dialog();//打开提取单据对话框
			get_suspend("gainSuspend.action");//数据表格
        });
	});
	
	/*打开提取单据对话框*/
	function open_suspend_dialog(){
		$("#get_suspend_order").dialog({
		       resizable: false,
		       modal: true,
		       buttons: [{
		           text: '选择',
		           iconCls: 'icon-add',
		           handler: function () {
		        	   choose_order();//选择单据信息到前台
		           }
		       }, {
		           text: '取消',
		           iconCls: 'icon-cancel',
		           handler: function () {
		           	close_suspend_Dialog();
		           }
		       }]
		   });
		   $("#get_suspend_order").dialog('setTitle', "提取单据");
		   $("#get_suspend_order").dialog('open');
	}
	
	/*关闭提取单据对话框*/
	function close_suspend_Dialog(){
		$("#get_suspend_order").dialog('close');
	}
	
	/*清空单据*/
	$(function(){
        $('#reset').bind('click',function(){
        	$.messager.confirm({
				title:'提示',
				msg:'清空单据会清空当前账单的全部信息，是否继续？',
				width:420,
				height:130,
				icon:'info',
				fn:function(r){
					if (r){
	        			resetOrder();//清空单据
	        		}
				}
			});
        });
    });
	
	/*清空单据*/
	function resetOrder(){
		$('#checkout_grid').datagrid('loadData',{total:0,rows:[]});//清空datagrid
    	$('#amount').attr('value','0');//清空合计数量
    	$('#totalPrice').attr('value','0.00');//清空合计金额
		$("#discount").attr('value',"");//清空折扣
    	$('#receivable').attr('value','0.00');//清空应收金额
    	$('#customer_details').form('clear');//清空会员信息
    	$('#orderid').attr('value'," ");
     	reset_goods_input();//清空输入框
     	$("#checkout_grid").datagrid('unselectAll');
	}
	
	
	/*挂账*/
	$(function(){
        $('#suspend_order').bind('click',function(){
        	//使用JSON序列化datarow对象，发送到后台
			var rows = $('#checkout_grid').datagrid('getRows');
            var rowObj = JSON.stringify(rows);//把json对象转为字符串
            var customerId=$('#customerId').textbox('getValue');
            if(customerId == "" || customerId == undefined || customerId == null){
            	customerId=0;
            }
            var rows = $('#checkout_grid').datagrid('getRows');//获取当前的数据行
        	if(rows.length<=0){
        		$.messager.alert('警告','请录入要购买的商品再进行挂账！','info',function(){
    				$("#goods_input").select();//输入框获取焦点并全选文字
    			});
        	}else{
        		$.messager.confirm('提示', '确定将当前账单挂起，稍后再进行结算？', function(r){
            		if (r){
            			$.ajax({
                            type: "POST",
                            url: "suspendBill.action",
                            data: {
                            	rowObj:rowObj,
                            	customerId:customerId,
                            	totalPrice:$('#totalPrice').val(),//总金额
                            	amount:$('#amount').val(),
                            	saleId:$('#orderid').val(), //销售单号
                            	receivables:$('#receivables').val(), //应收金额
                            },
                            dataType: "json",
                            success: function(data){
                            	if (data.success){
        	            			if($.messager.alert({
        	            				title:'提示',
        	            				msg:data.message,
        	            				width:300,
        	            				height:130,
        	            				icon:'info',
        	            			})){
        	            				resetOrder();//清空单据
        	            			}
                				 }
                            }
                        });
            		}
            	});
        	}
         });
    });
	
	/*数量+*/
	function addAmount(){
    	var rows = $('#checkout_grid').datagrid('getSelections');
    	if(rows == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
    	//数量+1
    	for (var i = 0; i < rows.length; i++) {
    		if(parseInt(rows[i]['amount'])<=0){
    			rows[i]['amount']=1;
    		}
    		var quantity=parseInt(rows[i]['amount']);
        	rows[i]['amount']+=1;
        	quantity+=1;
//            	console.log(quantity);
        	var index = $('#checkout_grid').datagrid('getRowIndex',rows[i]['id']);
        	//更新选中的每一行
        	$('#checkout_grid').datagrid('updateRow',{
        		index: index,
        		row: {
        			amount: quantity,
        			totalprice:parseInt(quantity)*rows[i]['price'],//修改金额
        		}
        	});
        	computeTotal();
        }
    }
	
	/*数量-*/
	function subtractAmount(){
    	var rows = $('#checkout_grid').datagrid('getSelections');
    	if(rows == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }
    	//数量-1
    	for (var i = 0; i < rows.length; i++) {
        	rows[i]['amount']-=1;
        	if(rows[i]['amount']<=0){
        		return;
        	}
        	var index = $('#checkout_grid').datagrid('getRowIndex',rows[i]['id']);
        	//更新选中的每一行
        	$('#checkout_grid').datagrid('updateRow',{
        		index: index,
        		row: {
        			amount: rows[i]['amount'],
        			totalprice:rows[i]['amount']*rows[i]['price'],//修改金额
        		}
        	});
        	computeTotal();
         }
    }
	
	/*选择商品，把商品的信息填到表格里*/
	function choose_good(){
		var good_row = $('#select_goods_grid').datagrid('getSelected'); // 得到选中的一行数据
	    if(good_row == null){
	        $.messager.alert("提示", "请选择一条记录",'info');
	        return;
	    }else{
			var obj=JSON.stringify(good_row);//把json对象转为字符串
			var jsonObj=JSON.parse(obj);//string转为json
	    	if(isExist(jsonObj)){
	    		computeTotal();//计算应收金额、合计数量、合计金额
 			}else{
				$('#checkout_grid').datagrid('insertRow',
					{
						index:0,
						row:{
							id:good_row.id,
							barcode:good_row.barcode,
							name:good_row.name,
							unit:good_row.unit,
							price:good_row.price,
							amount:1,
							totalprice:good_row.price,
							description:good_row.description,
						}
					}
				);
				computeTotal();//计算应收金额、合计数量、合计金额
			}
	    }
	    
	}
	
	function loadgrid_customer(url){
		$('#select_customer_grid').datagrid({
		    url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'id',
	        sortOrder : 'desc',
	        striped : true,  // 隔行变色  
	        pagination : true,
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 每页10条记录
	        pageList : [ 10, 20, 30, 50 ],
	        idField : 'id', // 主键属性
	        rownumbers : false,
	        onDblClickCell: choose_into_checkout,//选择会员，把会员的信息填到前台收银的表单里
	        frozenColumns : [ [
	           	            {title : '', width : '80',field : 'ck', sortable : false,checkbox:true},
	           		        {title : '会员编号', width : '60', field : 'id', sortable : true}, 
	           		        {title : '姓名', width : '90',    field : 'name',sortable : true, editor:'text'},
	           		        {title : '性别', width:'50',field:'gender',sortable:true, editor:'text'},
	           		        {title : '折扣', width:'65',field:'discount',sortable:true, editor:{type:'numberbox',options:{precision:2}}},
	           		        {title : '会员积分', width:'65',field:'integral',sortable:true, editor:'numberbox'},
	           		        {title : '注册时间', width:'200',field:'createTime',sortable:true, editor:'text'}
	           		        
	           	        ] ],
	        columns : [ [ 
		        {title : '联系电话', width : '140', field : 'cellphone', sortable : false, editor:'numberbox'}, 
		        {title : '地址', width : '180',    field : 'address', sortable : false, editor:'text'}
	        ] ],
	        toolbar:'#customer_tb'
	    });
	}
	
	/*选择会员的datagrid*/
	function get_customer(){
		loadgrid_customer('getAllCustomer.action');
	}
	
	/*选择商品的datagrid*/
	function get_goods(url){
		$('#select_goods_grid').datagrid({
//		    url:'getAllGoods.action',
		    url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'id',
	        sortOrder : 'asc',
	        striped : true,  // 隔行变色  
	        pagination : true,
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 每页10条记录
	        pageList : [ 10, 20, 30, 50 ],
	        idField : 'id', // 主键属性
	        rownumbers : true,
	        onDblClickCell: choose_good,//选择商品，把商品的信息填到表格里
	        frozenColumns : [ [
	           		        {title : '商品编号', width : '90', field : 'id', sortable : true},
	           		        {title : '商品条码', width : '90', field : 'barcode', sortable : true},
	           		        {title : '产品类型', width : '100', field : 'typeName',sortable : false, editor:'text'},
	           		        {title : '产品名称', width : '100', field : 'name',sortable : true, editor:'text'},
	           		        {title : '出售价格', width:'100',field:'price',sortable:true, editor:{type:'numberbox',options:{precision:2}}},
	           		        {title : '单位', width : '50', field : 'unit',sortable : true, editor:'text'}
	           	        ] ],
   	        columns : [ [ 
   		        {title : '备注', width : '200',    field : 'description', sortable : false, editor:'text'}
   	        ] ], 
   	        toolbar:'#tb_goods'
	    });
	}
	
	/*提取单据的datagrid表格*/
	function get_suspend(url){
		$('#suspend_grid').datagrid({
		    url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'id',
	        striped : true,  // 隔行变色  
	        pagination : false,
	        idField : 'id', // 主键属性
	        rownumbers : true,
	        onDblClickCell: choose_order,//选择被挂起的单据，把账单的信息填到前台
	        frozenColumns : [ [
	           		        {title : '挂账号', width : '100', field : 'suspendId', sortable : false},
	           		        {title : '挂账时间', width : '170', field : 'date',sortable : false},
	           		        {title : '会员号', width : '100', field : 'id', sortable : false},
	           		        {title : '会员姓名', width : '100', field : 'name',sortable : false},
	           		        {title : '积分', width : '100', field : 'integral',sortable : false},
	           		        
	           	        ] ],
   	        columns : [ [ 
   	            {title : '联系方式', width : '100',    field : 'cellphone', sortable : false, editor:'text'},
   		        {title : '折扣', width : '100',    field : 'discount', sortable : false, editor:'text'}
   	        ] ], 
	    });
	}
	
	/*提取账单填充到前台*/
	function choose_order(){
		var suspend_row = $('#suspend_grid').datagrid('getSelected'); // 得到选中的一行数据
	    if(suspend_row == null){
	        $.messager.alert("提示", "请选择要提取的单据",'info');
	        return;
	    }else{
	    	$("#customer_details").form('load', suspend_row); // 加载选择行数据
			var discount=$("#hidden_discount").val();
			$("#discount").attr('value',discount);//更新折扣
			//获取该账单的商品
			$.ajax({
                type: "POST",
                url: "gainSuspendGoods.action",
                data: {
                	suspendId:suspend_row.suspendId,//把挂账号传到后台
                },
                dataType: "json",
                success: function(data){
            		//删除挂账记录
            		$.ajax({
            			type:"POST",
            			url:"deleteRecord.action",
            			data: {
            				suspendId:suspend_row.suspendId,//把挂账号传到后台
                        },
                        dataType: "json",
                        success:function(result){
                        	close_suspend_Dialog();//关闭窗口
                        	if (result.success){
            					$.messager.show({
            						title: '提示',
            						timeout:1000,
            						msg: result.message,
            					});
            				} else {				
            					$.messager.show({
            						title: '错误',
            						timeout:1000,
            						msg: result.message
            					});
            				}
                        }
            		});
            		//加载商品数据
                	console.log(data);
            		$('#checkout_grid').datagrid('loadData',{
        				total:data.total,
        				rows:data.rows,
        			});
            		gainOrder();//获取单号
            		computeTotal();//计算应收金额、合计数量、合计金额
                }
            });
	    }
	}
	
	/*重打小票的datagrid表格*/
	function get_payments(url){
		$('#receipt_grid').datagrid({
		    url:url,
	        method:'post',
	        loadMsg : '正在加载…',
	        fit : true, 
	        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
	        fitColumns : true, 
	        singleSelect : true,
	        sortName : 'paymentId',
	        sortOrder : 'desc',
	        striped : true,  // 隔行变色  
	        pagination : true,
	        pageNumber : 1, // 初始化页码 
	        pageSize : 10,  // 每页10条记录
	        pageList : [ 10, 20, 30, 50 ],
	        idField : 'paymentId', // 主键属性
	        rownumbers : true,
	        onDblClickCell: outputHistoryReceipt,//选择打印小票
	        frozenColumns : [ [
   		        {title : '支付单号', width : '170', field : 'paymentId', sortable : true},
   		        {title : '交易时间', width : '170', field : 'createTime',sortable : true},
   		        {title : '会员号', width : '100', field : 'customerId', sortable : false },
   		        {title : '会员姓名', width : '100', field : 'customerName',sortable : false},
   		        {title : '收银员', width : '100', field : 'cashier',sortable : false},
   		        {title : '应收金额', width : '100', field : 'receivables',sortable : false},
	           		        
	        ] ],
	    });
	}
	
	/* 搜索框 */
	$("#goods_search").searchbox({
		width : 300,
		height : 25,
		menu : '#goods_options',
		prompt : '请输入关键字进行搜索',
		searcher : function(name, value) {
			// 编码，不然传到后台会变成乱码
			name = escape(encodeURIComponent(name));
			url = 'searchGoods.action?' + value + '=' + name;
			// console.log(url);
			get_goods(url);

		},
	});

	
	/*通过日期区间搜索*/
	$(function(){
	    $('#btn_search_date').bind('click', function(){
	    	$('#search_date').form('submit',{
				onSubmit: function(){
					return $(this).form('validate'); //前台字段格式校验
				},
				success: function(){
					var firstDate=($("#firstDate").datebox("getValue"));
					var lastDate=($("#lastDate").datebox("getValue"));
					url = 'searchByDate.action?firstDate='+firstDate+'&lastDate='+lastDate;
					loadgrid_customer(url);
				}
			});
	    });
	});
	
	/*搜索框*/
	$("#search").searchbox({
		width:300,
		height:25,
		menu:'#options',
		prompt:'请输入关键字进行搜索',
		searcher:function(name,value){
//			编码，不然传到后台会变成乱码
			name = escape(encodeURIComponent(name));
			url = 'searchCustomer.action?'+value+'='+name;
			loadgrid_customer(url);
			url='getAllCustomer.action';
		},
	});
	
	/*新增会员*/
	$("#tb_add").click(function add(){
		$("#form").form('clear');
		open_add_Dialog('添加新会员'); 
//	         表单清空后默认值就没了，重新设置
	    $('#add_discount').textbox('setValue','0.95');
	    $('#add_integral').textbox('setValue','0');
	    $("#add_gender").combobox('setValue','男');
	    url = 'addCustomer.action';
	    
	});
	
	/*添加新会员*/
	function open_add_Dialog(title){
	   $("#customer_add_dialog").dialog({
	       resizable: false,
	       modal: true,
	       buttons: [{
	           text: '保存',
	           iconCls: 'icon-save',
	           handler: function () {
	              save_add();
	           }
	       }, {
	           text: '取消',
	           iconCls: 'icon-cancel',
	           handler: function () {
	           	close_add_Dialog();
	           }
	       }]
	   });
	   $("#customer_add_dialog").dialog('setTitle', title);
	   $("#customer_add_dialog").dialog('open');
	}
	
	/* 保存新增会员的数据*/ 
	function save_add(){
		$('#form').form('submit',{
			url: url,
			onSubmit: function(){
				return $(this).form('validate'); //前台字段格式校验
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success){
					close_add_Dialog();	
					$.messager.show({
						title: '提示',
						msg: result.message
					});
				} else {				
					$.messager.show({
						title: '错误',
						msg: result.message
					});
				}
			}
		});
	}
	/*关闭添加新会员的窗口*/
	function close_add_Dialog() {  
		$("#form").form('clear');
		$("#customer_add_dialog").dialog('close');
	}
	
	/*结账框只能输入整数*/
	$(function(){
		$("#accounts").keyup(function(event){ 
//			this.value=this.value.replace(/[^0-9\.]/g,'');
		}); 
	});
	
	/*输入框只能输入数字和字母*/
	$(function(){
		$("#goods_input").keyup(function(event){ 
			this.value=this.value.replace(/[^\w\.\/]/ig,'');
		}); 
	});
	
	
});
		</script>
		<style type="text/css">
		/*表格行高*/
		.datagrid-row {
	        height: 42px;/* 本来是34 */
	    }
	    #select_customer .datagrid-row {
	        height: 30px;
	    }
	    #select_goods .datagrid-row {
	        height: 30px;
	    }
	    #p_receipt .datagrid-row {
	        height: 30px;
	    }
	    .quick {
	    	background-color:#ccc;
	    	margin:3px;
	    	padding:7px 0 0 3px;
	    	float:left;
	    	height:50px;
	    	width:61px;
	    	border-radius:5px;
			-moz-border-radius:5px;
			font-size:12px;
			cursor:pointer;
	    }
	    fieldset {
	    	border-radius:3px;
	    }
	    input {
	    	font-family:Microsoft YaHei;
	    }
	    /*表格行样式*/
        .datagrid-row td div {
        	font-size:18px;/* 本来是18 */
        	font-family:Microsoft YaHei;
        	color:black;
        }
        #select_goods .datagrid-row td div,#select_customer .datagrid-row td div,#get_suspend_order .datagrid-row td div{
        	font-size:12px;
        }
        #receipt_dialog .datagrid-row td div,#p_receipt .datagrid-row td div{
        	font-size:12px;
        }
	</style>
    </head>
<body style="margin-bottom:0;font-family:Microsoft YaHei;">
    <div style="width:100%;height:100%;padding:0;overflow:hidden;color:#333;">
	    <div style="font-family:Microsoft YaHei;margin:0;padding-bottom:8px;font-size:14px;">
	    	
		    	<fieldset id="checkout_head" style="padding:6px;margin:0;">
		    		<div style="float:left;margin:0;padding-right:13px;">
		    			<span>销售单号:</span>
		    			<input id="orderid" readonly
		    				style="font-size:18px;width:180px;height:25px;border:none;" />
		    		</div>
			    	<form action="" method="post" id="customer_details" style="margin:0;padding:0;">	
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员号:</span>
			    			<input id="customerId" name="id" class="easyui-textbox" data-options="prompt:'快捷键ctrl+→',editable:false"
			    				style="width:120px;height:25px;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员姓名:</span>
			    			<input readonly id="customerName" name="name" class="easyui-textbox" 
			    				style="width:80px;height:25px;font-weight:bold;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员积分:</span>
			    			<input readonly id="customerIntegral" class="easyui-textbox" name="integral"  
			    				style="width:80px;height:25px;font_weight:bold;" />
			    		</div>
			    		<input type="hidden" name="discount" id="hidden_discount" />
		    		</form>
		    		<div style="float:left;margin:0;padding-right:10px;">
		    			<span>收银员:</span>
		    			<input readonly class="easyui-textbox" value='陆希' id="cashier"
		    				style="width:80px;height:25px;" />
		    			<input type="hidden" value="5" id="cashierId">
		    		</div>
		    		
		    		<div id="time" style="float:right;font-family:'Verdana';font-size:14px;"></div>
		    	
		    	</fieldset>
	    	
	    </div>	
	    
    	<div style="height:310px;width:100%;margin-top:0;" id="checkout_grid_div">
	    	<!-- 表格 -->
			<table id="checkout_grid"></table>
		</div>
		
		<div style="font-family:Microsoft YaHei;height:100%;font-size:14px;">
			<fieldset style="float:left;height:142px;width:260px;">
				<legend>输入框</legend>
				<div style="margin:7px 0;font-weight:bold;">请输入商品条码（回车键结算）</div>
				<form action="" method="post" id="goods_input_form">
					<input name="goods.barcode" type="text" id="goods_input" 
						style="border: 1px #666 solid;border-radius:5px;height:60px;width:240px;font-size:36px;">
				</form>
				<div style="padding:0;">* F2：输入框获取焦点</div>
			</fieldset>
			
			<fieldset style="float:left;height:142px;width:560px;">
				<legend>账单信息</legend>
				<div style="margin:0;padding:0;width:225px;font-size:18px;float:left;">
					<div style="float:right;margin-top:0;">
						<span>合计数量:</span>
						<input type="text" readonly value="0" id="amount" 
							style="border:none;width:140px;font-size:30px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>合计金额:</span>
						<input type="text" readonly id="totalPrice"  value="0.0" 
							style="border:none;width:140px;font-size:30px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>整单打折:</span>
						<input type="text" readonly name="discount"  value="" id="discount" 
							style="border:none;width:140px;font-size:30px;">
					</div>
				</div>
					
				<div style="margin:5px;font-size:22px;float:left;">
					<div style="color:black;padding-left:10px;">应收金额:</div>
					<input type="text" readonly id="receivable"  value="0.0" 
						style="border:none;width:320px;font-size:70px;">
				</div>
				
			</fieldset>
			
			<fieldset id="" style="float:left;height:142px;width:230px;">
				<legend>快捷功能</legend>
				<div class="quick" id="search_good">查找商品(F4)</div>
				<div class="quick" id="add_customer">注册新会员</div>
				<div class="quick" id="reset">清空单据</div>
				<div class="quick" id="suspend_order">挂账</div>
				<div class="quick" id="collect_order">提取单据</div>
				<div class="quick" id="regain_bill">重打小票</div>
			</fieldset>
		</div>
		
    </div>
    
    <!--  结算窗口 -->
    <div class="easyui-dialog" id="checkout" style="color:black;font-size:36px;padding-right:40px;width:560px;height: 520px;text-align:right">
    	<form action="" id="checkout_form">
    		<div style="padding-top:30px;margin:15px;">
	    		<span>单号:</span>
	    		<input id="checkout_id" readonly style="border:none;width:280px;height:50px;font-size:30px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>应收金额:</span>
	    		<input id="receivables" value="0.0" readonly style="border:none;width:280px;height:50px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>实收金额:</span>
	    		<input id="accounts" style="border: 1px #ccc solid;border-radius:5px;width:280px;height:55px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>找款:</span>
	    		<input id="change" readonly value="0.0" style="border:none;color:red;font-weight:bold;width:280px;height:50px;font-size:50px;">
	    	</div>
	    	<div style="margin:15px;">
	    		<span>结算方式:</span>
	    		<select  id="checkout_way" style="border: 1px #ccc solid;border-radius:5px;width:280px;height:50px;font-size:28px;">
	    			<option value="0">现金支付</option>
	    			<option value="1">银行卡支付</option>
	    		</select>
	    	</div>
	    	<div style="font-size:14px;height:15px;margin-right:5px;">
	    		<input type="checkbox" checked="checked" name="outputReceipt" id="outputReceipt" />打印小票
	    	</div>
	    	
	    	<!-- <button id="submit_checkout">结算</button>
	    	<button id="close_checkout">取消</button> -->
    	</form>
    </div>
    
    <!-- 小票 -->
    <div id="receipt_dialog" style="font-size:14px;padding:0 5px;">
    	<div style="text-align:center;font-size:18px;margin:6px;">Dainty-Bits</div>
    	<div>流水号：<span id="t_paymentId"></span></div>
    	<div>收银员：<span id="t_cashier"></span></div>
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<div>
    		<table id="receipt_table" style="font-size:14px;"></table>
    	</div>
    	
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<table style="font-size:14px;">
    		<tr>
    			<td><div>会员：<span id="t_customer"></span></div></td>
    			<td><div>数量：<span id="t_amount"></span></div></td>
    		</tr>
    		<tr>
    			<td><div style="width:150px;">折扣：<span id="t_discount"></span></div></td>
    			<td><div>合计金额：<span id="t_totalprice"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>积分：<span id="t_integral"></span></div></td>
    		</tr>
    	</table>
    	
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<div>应收金额：<span id="t_receivables"></span></div>
    	<div>实收金额：<span id="t_realpay"></span></div>
    	<div>找款：<span id="t_change"></span></div>
    	<div>交易时间：<span id="t_time"></span></div>
    	<div style="text-align:center;">-----------------------------------------------------</div>
    	<div>欢迎光临Dainty-Bits！</div>
    	<div>本小票为退货凭据，请保留小票以便查核</div>
    	<div>发票于当月内开具，过期无效</div>
    	<div>多谢惠顾，欢迎再次光临</div>
    	<br />
    </div>
    
    <!-- 选择会员窗口 -->
    <div id="select_customer">
    	<div style="height:430px;width:1000px;margin-top:0;">
    		<table id="select_customer_grid"></table>
    	</div>
    </div>
    
    <!-- 选择会员datagrid的工具栏 -->
	<fieldset id="customer_tb">
		<div style="margin:4px;float:left;">
			<!-- 搜索框 -->
	        <div id="search"></div>
			<div id="options">
				<div data-options="name:'id',iconCls:'icon-ok'">会员编号</div>
				<div data-options="name:'name',iconCls:'icon-ok'">姓名/姓名关键字</div>
				<div data-options="name:'cellphone',iconCls:'icon-ok'">联系电话</div>
			</div>
		</div>
		<!-- 通过日期区间搜索 -->
		<div style="float:left;margin:4px;">
			<form id="search_date" method="post" >
			注册日期  从: <input class="easyui-datebox" style="width:120px" name="firstDate" id="firstDate" />
			到: <input class="easyui-datebox" style="width:120px" name="lastDate" id="lastDate" />
			<a href="javascript:void(0)" id="btn_search_date" class="easyui-linkbutton" iconCls="icon-search">搜索</a>
		</form>
		</div>
		
	</fieldset> 
    
    <!-- 选择商品窗口的表格 -->
    <div id="select_goods">
    	<div style="height:400px;width:800px;margin-top:0;">
    		<table id="select_goods_grid"></table>
    	</div>
    </div>
    
    <!-- 添加会员的对话框 -->
	<div id="customer_add_dialog" style="padding-right:60px;width:400px;height: 320px;text-align:right">	
		<form id="form" method="post">
			<div style="margin:10px;">
				<span>姓名：</span> 
				<input name="name" class="easyui-textbox" style="width:200px;"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<span>性别：</span> 
				<select name="gender" id="add_gender" style="width:200px;"
					data-options="editable:false,panelHeight:'auto'">
					<option>男</option>
					<option>女</option>
				</select>
			</div>
			<div style="margin:10px;">
				<span>折扣：</span> 
				<input id="add_discount" name="discount" class="easyui-textbox" style="width:200px;"
					data-options="editable:false,value:'0.95'" />
			</div>
			<div style="margin:10px;">
				<span>会员积分：</span> 
				<input name="integral" id="add_integral" class="easyui-textbox" style="width:200px;" 
				data-options="editable:false,value:'0'"/>
			</div>
			<div style="margin:10px;">
				<span>联系电话：</span> 
				<input name="cellphone" class="easyui-textbox" style="width:200px;"
					data-options="required:true" />
			</div>
			<div style="margin:10px;">
				<span>地址：</span> 
				<input name="address" class="easyui-textbox"
					data-options="required:true,multiline:true"
					style="width:200px;height:60px" />
			</div>
		</form>
	</div>
	
	<!-- 商品datagrid的工具栏 -->
	<fieldset id="tb_goods">
		<div style="margin:4px;float:left;">
			<!-- 搜索框 -->
			<div id="goods_search"></div>
			<div id="goods_options">
				<div data-options="name:'barcode',iconCls:'icon-ok'">商品条码</div>
				<div data-options="name:'name',iconCls:'icon-ok'">商品名/商品名关键字</div>
				<div data-options="name:'typeName',iconCls:'icon-ok'">商品类型/商品类型关键字</div>
			</div>
		</div>
	</fieldset>
	
	<!-- 提取账单 -->
    <div id="get_suspend_order">
    	<div style="height:400px;width:800px;margin-top:0;">
    		<table id="suspend_grid"></table>
    	</div>
    </div>
    
    <!-- 重打小票 -->
    <div id="p_receipt">
    	<div style="height:400px;width:800px;margin-top:0;">
    		<table id="receipt_grid"></table>
    	</div>
    </div>
    
    
    
  </body>
</html>