<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
        <style type="text/css">
        #d_return_table td{
	        width:100px;
	    }
		#d_receipt_table td{
	        width:100px;
	    }
		</style>
		<script type="text/javascript">
		
		function pay() {
            var row = $('#grid').datagrid('getSelected'); //// 得到选中的一行数据
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            if(row.state == 1){
            	$.messager.alert("提示", "已付款",'info');
            	return;
            }
            openDialog('付款'); // 显示更新数据dialog窗口
            /* document.getElementById('totalPrice').innerText=row.totalPrice; */
            $("#totalPrice").val()==row.totalPrice;
            $("#form").form('load', { "totalPrice":row.totalPrice , "cash":row.totalPrice }); // 加载选择行数据
            url = 'payAfter.action?saleorder.saleorderId='+row.saleorderId; //后台更新数据action
        }
        function openDialog(title){
            $("#dialog").dialog({
                resizable: false,
                modal: true,
                buttons: [{ //设置下方按钮数组
                    text: '确认付款',
                    iconCls: 'icon-save',
                    handler: function () {
                       save();
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        closeDialog();
                    }
                }]
            });
            $("#dialog").dialog('setTitle', title);
            $("#dialog").dialog('open');
            $("#cash").focus();
        }
        function closeDialog() {  
            $("#form").form('clear'); // 清空form的数据
            $("#dialog").dialog('close');// 关闭dialog
        }
        function oddChange(){
    	$('#change').attr('value',parseFloat($('#cash').val()-$('#totalPrice').val()).toFixed(2));
    	}
    	function save(){
            $('#form').form('submit',{
                url: url,  //提交地址
                onSubmit: function(){
                    return $(this).form('validate'); //前台字段格式校验
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        closeDialog();// 调用closeDialog;    
                        reload();// 重新加载
                        $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        });
                        $('#receipt_table').html('');
                        $('#receipt_table').append('<tr><td>商品</td><td>单价</td><td>数量</td><td>金额</td></tr>');
							for (var i = 0; i < result.receipts.length; i++) {
			 				var tableRow = '<tr>' +
             				'<td>' + result.receipts[i].productName + '</td>' +
             				'<td>' + result.receipts[i].productPrice.toFixed(2) + '</td>' +
             				'<td>' + result.receipts[i].quantity + '</td>' +
             				'<td>' + result.receipts[i].price.toFixed(2) + '</td>' +
             				'</tr>';
			 				$('#receipt_table').find('tr').css('display','table-row');
			 				$('#receipt_table').append(tableRow);
			
        				}
                        $('#r_saleorderNo').html(result.saleorderNo);
						$('#r_username').html(result.username);
						$('#r_customerName').html(result.customerName);
						$('#r_quantity').html(result.quantity);
						$('#r_discount').html(result.discount+"%");
						$('#r_totalprice').html(parseFloat(result.totalprice).toFixed(2));
						var date1 = new Object(result.paytime).time;
        				var date = new Date(date1);
						$('#r_paytime').html(date.toLocaleString());
                        $("#receipt_dialog").dialog({
                			resizable: false,
                			modal: true,
                			buttons: [{ //设置下方按钮数组
                				id:'receiptComfirm',
                    			text: '确定',
                    			iconCls: 'icon-save',
                    			handler: function () {
                       			$("#receipt_dialog").dialog('close');
                    			}
                			}]
            			});
            			$("#receipt_dialog").dialog('setTitle',"小票");
            			$("#receipt_dialog").dialog('open');
            			$("#receiptComfirm").focus();
                    } else {              
                        $.messager.show({   //显示错误信息
                            title: '错误',
                            msg: result.msg
                        });
                    }
                }
            });
        }
        function saleorderDetail(){
        	var row = $('#grid').datagrid('getSelected');
        	if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
        	$.ajax({
             	type:'POST',
             	url:'getSaleorderDetail.action?id='+row.saleorderId,
             	dataType : 'json',
             	cache : false,
             	success:function(result){
             			$('#d_receipt_table').html('');
        				 $('#d_receipt_table').append('<tr><td>商品</td><td>单价</td><td>数量</td><td>金额</td></tr>');
							for (var i = 0; i < result.receipts.length; i++) {
			 				var tableRow = '<tr>' +
             				'<td>' + result.receipts[i].productName + '</td>' +
             				'<td>' + result.receipts[i].productPrice.toFixed(2) + '</td>' +
             				'<td>' + result.receipts[i].quantity + '</td>' +
             				'<td>' + result.receipts[i].price.toFixed(2) + '</td>' +
             				'</tr>';
			 				$('#d_receipt_table').find('tr').css('display','table-row');
			 				$('#d_receipt_table').append(tableRow);
			
        				}
        				$('#d_return_table').html('');
        				 $('#d_return_table').append('<tr><td>商品</td><td>单价</td><td>数量</td><td>金额</td><td>退货时间</td></tr>');
							for (var i = 0; i < result.returnList.length; i++) {
							var date3 = new Object(result.returnList[i].returnDatetime).time;
        					var date2 = new Date(date3);
			 				var tableRow = '<tr>' +
             				'<td>' + result.returnList[i].productName + '</td>' +
             				'<td>' + result.returnList[i].productPrice.toFixed(2) + '</td>' +
             				'<td>' + result.returnList[i].quantity + '</td>' +
             				'<td>' + result.returnList[i].price.toFixed(2) + '</td>' +
             				'<td>' + date2.toLocaleString() + '</td>' +
             				'</tr>';
			 				$('#d_return_table').find('tr').css('display','table-row');
			 				$('#d_return_table').append(tableRow);
			
        				}
                        $('#d_saleorderNo').html(result.saleorderNo);
						$('#d_username').html(result.username);
						$('#d_customerName').html(result.customerName);
						$('#d_quantity').html(result.quantity);
						$('#d_discount').html(result.discount+"%");
						$('#d_totalprice').html(parseFloat(result.totalprice).toFixed(2));
						var paytime = new Object(result.paytime).time;
        				var d_paytime = new Date(paytime);
						$('#d_paytime').html(d_paytime.toLocaleString());
						var saleorderCreatetime = new Object(result.saleorderCreatetime).time;
        				var d_saleorderCreatetime = new Date(saleorderCreatetime);
						$('#d_saleorderCreatetime').html(d_saleorderCreatetime.toLocaleString());
                        $("#saleorderDetail_dialog").dialog({
                			resizable: false,
                			modal: true,
                			buttons: [{ //设置下方按钮数组
                				id:'saleorderDetailComfirm',
                    			text: '确定',
                    			iconCls: 'icon-save',
                    			handler: function () {
                       			$("#saleorderDetail_dialog").dialog('close');
                    			}
                			}]
            			});
            			$("#saleorderDetail_dialog").dialog('setTitle',"详情");
            			$("#saleorderDetail_dialog").dialog('open');
            			$("#saleorderDetailComfirm").focus();
        		}
        	});
        }
        function returnsale(){
        	var row = $('#grid').datagrid('getSelected'); //// 得到选中的一行数据
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            if(row.state == 2){
            	$.messager.alert("提示", "还未付款",'info');
            	return;
            }
            if(row.totalPrice == 0){
            	$.messager.alert("提示", "该订单无可退换商品",'info');
            	return;
            }
            $.ajax({
             type:'POST',
             url:'returnloadReturn.action?id='+row.saleorderId,
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            	 	$("#return_dialog").dialog({
                		resizable: false,
                		modal: true,
                		buttons: [{ //设置下方按钮数组
                    		text: '确认退货',
                    		iconCls: 'icon-save',
                    		handler: function () {
                       			submitRuturn();
                    		}
                		}, {
                    		text: '取消',
                    		iconCls: 'icon-cancel',
                    		handler: function () {
                        		$("#return_dialog").dialog('close');
                    		}
                		}]
            		});
            	/* $("#return_dialog").dialog('setTitle', title); */
            	$("#returnSalereturnNo").val(r.salereturnNo);
            	$("#returnSaleorderNo").val(r.saleorderNo);
            	$("#returnCustomerDisount").val(r.returnCustomerDisount/100);
            	$("#returnTotalprice").val('');
             	$("#returnReturnprice").val('');
             	$("#returnSalereturnitemAmount").numberbox('setValue','');
            	$("#returnSaleorderitemSelects").combobox({ 
					url: 'returnSaleorderitemSelects.action?id='+row.saleorderId,
					valueField:'saleorderitemId', 
					textField:'productName',
					width:'200',
					onLoadSuccess: function () {
                		var val = $(this).combobox("getData");
                		/* if(val[0]==null){
                			$.messager.alert("提示", "该订单已无商品可退换",'info');
                		}else{ */
                			for (var item in val[0]) {
                    			if (item == "saleorderitemId") {
                       				$(this).combobox("select", val[0][item]);
                    			}
              				}
              			/* } */
           			}
				}); 
            	$("#return_grid").datagrid({
        			title : '退货单',
        			url : 'returnGetSalereturnitems.action',
        			loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        			fit : true,  //窗口自适应
        			nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        			fitColumns : true, // 自动适应列宽
        			singleSelect : true, // 每次只选中一行
        			sortName : 'saleorderId', //默认排序字段
        			sortOrder : 'desc', // 升序asc/降序desc
        			striped : true,  // 隔行变色  
        			pagination : false,  // 在底部显示分页工具栏
        			pageNumber : 1, //初始化页码 
        			pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        			pageList : [ 5, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        			//rownumbers : true, // 在最前面显示行号 
        			idField : 'id', // 主键属性
        			columns : [ [ 
        				{title : '商品id', width : '100', field : 'productId', sortable : false
        				},
        				{title : '商品名', width : '300', field : 'productName', sortable : false
        				},
        				{title : '类别名', width : '100', field : 'producttypeName', sortable : false
        				},
        				{title : '单价格', width : '100', field : 'price', sortable : false
        				},
        				{title : '总价格', width : '100', field : 'totalprice', sortable : false
        				},
        				{title : '数量', width : '200', field : 'quantity', sortable : false
        				}
        			] ],       
        			// 工具栏按钮
        			toolbar: [
						"-", {id: 'plus', text: '数量+1',    iconCls: 'icon-add', handler: function () { modifySaleorderitem("plus")} },
						"-", {id: 'reduce', text: '数量-1',   iconCls: 'icon-remove',   handler: function () {  modifySaleorderitem("reduce")} }, 
						"-", {id: 'remove', text: '删除',iconCls: 'icon-no', handler: function () {removeSaleorderitem()} }, 
						"-", {id: 'reload',  text: '清空',iconCls: 'icon-print', handler: function () {clearSaleorderitem()} },
						"-" 
        			]
    			});
    			$("#return_dialog").dialog('open');
 				 }else{
 				 }
             }
         	});
        }
        function addSalereturnitem(){
        	if($("#returnSalereturnitemAmount").val()==''||$("#returnSalereturnitemAmount").val()==0){
        		$("#returnSalereturnitemAmount").textbox("setValue",1);
        	}
        	/* alert($("#returnSaleorderitemSelects").combobox("getValue"));
        	if($("#returnSaleorderitemSelects").combobox("getValue")==null){
        		alert("test111");
        	} */
        	$.ajax({
             type:'POST',
             url:'returnaddSalereturnitem.action',
             dataType : 'json',
             data: {id:$("#returnSaleorderitemSelects").combobox("getValue"),quantity:$("#returnSalereturnitemAmount").val()},
             cache : false,
             success:function(r){
             	if(r.success){
             		$("#return_grid").datagrid('reload');
             		$("#returnTotalprice").val(r.returnTotalprice.toFixed(2));
             		$("#returnReturnprice").val((r.returnTotalprice*$("#returnCustomerDisount").val()).toFixed(2));
             	}else{
             		$.messager.alert("提示", r.msg,'info');
             	}
             }
            });
        }
        function submitRuturn(){
        	var data = $("#return_grid").datagrid("getData");
        	if(data.rows.length>=1){
        		$.ajax({
             	type:'POST',
             	url:'returnSubmitRuturn.action',
             	dataType : 'json',
             	data: {reason:$("#returnReturnReason").val(),quantity:$("#returnReturnprice").val()},
             	cache : false,
             	success:function(r){
             		$("#return_dialog").dialog('close');
             		$('#grid').datagrid('reload');
             		$.messager.show({
                            	title: '提示',
                            	msg: r.msg
                        		});
             		}
            	});
            }else{
            	$.messager.alert("提示", "当前退货单为空",'info');
            }
        }
        
		
		
		
/* 刷新grid*/
        function reload(){
            $('#grid').datagrid('reload');
        }
/* jquery入口*/
$(function() {
    loadgrid(); //加载datagrid
    $('#searchbox').searchbox({
		    searcher:function(value,name){
		    	searchFun();
		    },
		    menu:'#option',
		    prompt:'可进行模糊查询'
		});
});
/* 加载datagrid列表*/
    function loadgrid(){
        $('#grid').datagrid({
        title : '交易单',
        url : 'getSaleorders.action',
        loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        fit : true,  //窗口自适应
        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        fitColumns : true, // 自动适应列宽
        singleSelect : true, // 每次只选中一行
        sortName : 'saleorderId', //默认排序字段
        sortOrder : 'desc', // 升序asc/降序desc
        striped : true,  // 隔行变色  
        pagination : true,  // 在底部显示分页工具栏
        pageNumber : 1, //初始化页码 
        pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        pageList : [ 5, 10, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        //rownumbers : true, // 在最前面显示行号 
        idField : 'id', // 主键属性
        // 冻结列,当很多咧出现滚动条时该列不会动
        frozenColumns : [ [
        {title : '序号', width : '50', field : 'saleorderId', sortable : true}, 
        {title : '订单编号', width : '150', field : 'saleorderNo', sortable : true}, 
        {title : '状态', width : '50',    field : 'state',sortable : true,
        formatter: function(value,row,index){
        	if(value=="1"){
        	return "已付款";
        	}else{
        	return "未付款";
        	}
		} 
        }
        ] ],
        columns : [ [ 
        {title : '顾客名', width : '100', field : 'customerName', sortable : false
        },
        {title : '收款人', width : '100', field : 'username', sortable : false
        },
        {title : '创建时间', width : '200', field : 'createDatetime', sortable : false,
        formatter: function(value,row,index){
        	var date1 = new Object(row["createDatetime"]).time;
        	var date = new Date(date1);
			return date.toLocaleString();
		} 
        },
        {title : '付款时间', width : '200', field : 'payDatetime', sortable : false,
        formatter: function(value,row,index){
        	var date1 = new Object(row["payDatetime"]).time;
        	if(date1!=null){
        		var date = new Date(date1);
				return date.toLocaleString();
			}else{
				return "";
			}
		} 
        }, 
        {title : '总价格', width : '100',    field : 'totalPrice', sortable : false},
        {title : '退货价格', width : '100',    field : 'returnPrice', sortable : false}
        ] ],       
        // 工具栏按钮
        toolbar: [
"-", {id: 'add', text: '付款',    iconCls: 'icon-add', handler: function () { pay()} },
"-", {id: 'edit', text: '退货',   iconCls: 'icon-edit',   handler: function () { returnsale()} },
"-", {id: 'remove', text: '订单详情',iconCls: 'icon-remove', handler: function () {saleorderDetail()} }, 
/* "-", {id: 'reload',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload()} }  */
        ]
    });
}

	function searchFun() {
		/* alert($('#searchbox').searchbox('getName')); */
		var startdate=$('#startdate').datebox("getValue").replace(/\-/gi,"");
      	var enddate=$('#enddate').datebox("getValue").replace(/\-/gi,""); 
     	if(startdate-enddate>0){
     	$.messager.alert("提示", "开始日期要在截止日期之前!",'info');
     	}else{
		key={keyword:$('#searchbox').val(),startdate:$('#startdate').datebox("getValue"),enddate:$('#enddate').datebox("getValue"),keyname:$('#searchbox').searchbox('getName')};
		$('#grid').datagrid('load',key); }
	}
	function clearFun() {
		$('#searchbox').searchbox("setValue","");
		$('#startdate').datebox("setValue","");
		$('#enddate').datebox("setValue","");
		$('#grid').datagrid('load', {});
	}
</script>
    </head>
    <body class="easyui-layout">
    <div region="north" data-options="border:false,title:'选项'" style="height:75px;overflow:hidden;">
		<form id="form2" >
			<div style="float:right;margin:5px;"><input id="searchbox" style="width:300px;"></input>
			<div id="option" style="width:120px">
			<div data-options="name:'customerId',iconCls:'icon-ok'">会员ID</div>
    		<div data-options="name:'userId',iconCls:'icon-ok'">收银员</div>
    		<div data-options="name:'saleorderNo',iconCls:'icon-ok'">订单号</div>
			</div>
			</div>
			<div style="float:left;margin:5px;"><label>开单时间&nbsp;:&nbsp;从&nbsp;</label><input id="startdate" name="startdate"  class="easyui-datebox" /><label>&nbsp;至&nbsp;</label><input id="enddate" name="enddate" class="easyui-datebox" />
			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="searchFun();" >查询</a>
			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true" onclick="clearFun();">清除条件</a></div>
		</form>
	</div>
    <div id="mainPanle" data-options="region:'center'">
       <div style="width:100%;height:100%;padding:0px;overflow:hidden">
           <table id="grid"></table>
       </div>
    </div>
       <div id="dialog" class="easyui-dialog" data-options="closed:true" title="付款" style="width:300px;height:350px;text-align:center" >
              <form id="form" method="post">
                 <div style="margin-top:30px">
                        <label style="font-size:25px">总金额</label>
						<input id="totalPrice" readonly name="totalPrice" data-options="field:'totalPrice',required:true"  style="text-align:center;border:none;font-size:25px;width:180px">
                 </div>
                 <div style="margin-top:30px">
                        <label style="font-size:25px">实收款</label>
                   	    <input id="cash" name="cash" data-options="field:'cash',required:true" type="text" style="text-align:center;font-size:25px;width:180px" onchange="oddChange()">
                 </div>
                 <div style="margin-top:30px">
                        <label style="font-size:25px">找零</label>
                        <input id="change" readonly name="change" data-options="field:'change',required:true"  value="0.0" style="text-align:center;border:none;font-size:25px;width:180px">
                        <%-- <span id="change" style="width:100px;text-align:center">0.0</span> --%>
                 </div>
              </form>
       </div>
       <!-- 小票 -->
    <div id="receipt_dialog" class="easyui-dialog" data-options="closed:true" title="小票" style="width:250px;font-size:14px;padding:0 5px;">
    	<div style="text-align:center;font-size:18px;margin:6px;">12345</div>
    	<div>流水号：<span id="r_saleorderNo"></span></div>
    	<div>收银员：<span id="r_username"></span></div>
    	<div style="text-align:center;"><hr></div>
    	<div>
    		<table id="receipt_table" style="font-size:14px;"></table>
    	</div>
    	
    	<div style="text-align:center;"><hr></div>
    	<table style="font-size:14px;">
    		<tr>
    			<td><div>会员：<span id="r_customerName"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>数量：<span id="r_quantity"></span></div></td>
    		</tr>
    		<tr>
    			<td><div style="width:150px;">折扣：<span id="r_discount"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>合计金额：<span id="r_totalprice"></span></div></td>
    		</tr>
    	</table>
    	
    	<div style="text-align:center;"><hr></div>
    	<div>交易时间：<span id="r_paytime"></span></div>
    	<div style="text-align:center;"><hr></div>
    	<div>本小票为退货凭据，请保留小票以便查核</div>
    	<div>发票于当月内开具，过期无效</div>
    	<div>多谢惠顾，欢迎再次光临</div>
    	<br />
    </div>
    <div id="saleorderDetail_dialog" class="easyui-dialog" data-options="closed:true" title="小票" style="height:500px;width:600px;font-size:14px;padding:0 5px;">
    	<div style="text-align:center;font-size:18px;margin:6px;">交易信息</div>
    	<div>交易号：<span id="d_saleorderNo"></span></div>
    	<div>收银员：<span id="d_username"></span></div>
    	<div style="text-align:center;"><hr></div>
    	<div>
    		商品列表:
    		<table id="d_receipt_table" style="font-size:14px;"></table>
    	</div>
    	<div style="text-align:center;"><hr></div>
    	<div>
    		退货情况：
    		<table id="d_return_table" style="font-size:14px;"></table>
    	</div>
    	
    	<div style="text-align:center;"><hr></div>
    	<table style="font-size:14px;">
    		<tr>
    			<td><div>会员：<span id="d_customerName"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>数量：<span id="d_quantity"></span></div></td>
    		</tr>
    		<tr>
    			<td><div style="width:150px;">折扣：<span id="d_discount"></span></div></td>
    		</tr>
    		<tr>
    			<td><div>合计金额：<span id="d_totalprice"></span></div></td>
    		</tr>
    	</table>
    	
    	<div style="text-align:center;"><hr></div>
    	<div>开单时间：<span id="d_saleorderCreatetime"></span></div>
    	<div>付款时间：<span id="d_paytime"></span></div>
    	<div style="text-align:center;"><hr></div>
    	<br />
    </div>
    <div id="return_dialog" class="easyui-dialog" data-options="closed:true" title="退货" style="width:800px;height:500px;text-align:center" >
              <form id="form" method="post">
                 <div style="margin-top:20px">
                        <label style="margin-left:20px;font-size:20px">订单号:</label>
						<input id="returnSaleorderNo" readonly style="text-align:center;border:none;font-size:15px;width:160px">
						<label style="margin-left:20px;font-size:20px">退货号:</label>
                   	    <input id="returnSalereturnNo" readonly type="text" style="text-align:center;border:none;font-size:15px;width:160px">
                   	     <label style="margin-left:20px;font-size:20px">顾客折扣:</label>
                   	    <input id="returnCustomerDisount" readonly type="text" style="text-align:center;border:none;font-size:15px;width:50px">
                 </div>
                 <div style="margin-top:20px">
                        <label style="font-size:25px">退货商品选择:</label>
                        <select id="returnSaleorderitemSelects" name="product.producttype.producttypeId" class="easyui-combobox"/></select>
                       	<label style="margin-left:30px;font-size:25px">数量:</label><input id="returnSalereturnitemAmount" class="easyui-numberbox" style="text-align:center;font-size:15px;width:80px">
                        <a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addSalereturnitem();" >确定</a>
                 </div>
                 <div style="margin-top:20px;height:200px;width:100%;">
                        <table id="return_grid"></table>
                 </div>
                 <div style="margin-top:20px">
                        <label style="margin-left:20px;font-size:20px">总金额:</label>
						<input id="returnTotalprice" readonly style="text-align:center;border:none;font-size:15px;width:80px">
						<label style="margin-left:20px;font-size:20px">应退还金额:</label>
						<input id="returnReturnprice" readonly style="text-align:center;border:none;font-size:15px;width:80px">
						<label style="margin-left:20px;font-size:20px">退货原因:</label>
						<input id="returnReturnReason" style="text-align:center;font-size:15px;width:200px">
						<!-- <label style="margin-left:20px;font-size:20px">退货号:</label>
                   	    <input id="returnSalereturnNo" readonly type="text" style="text-align:center;border:none;font-size:15px;width:160px">
                   	     <label style="margin-left:20px;font-size:20px">顾客折扣:</label>
                   	    <input id="returnCustomerDisount" readonly type="text" style="text-align:center;border:none;font-size:15px;width:50px"> -->
                 </div>
              </form>
       </div>
    </body>
</html>