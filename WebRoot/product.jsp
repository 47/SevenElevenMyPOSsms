<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
		<script type="text/javascript">
/* 显示Dialog*/
        function openDialog(title,producttypeId){
        	$("#select").combobox({ 
				url:'getProducttypesSelect.action', 
				valueField:'producttypeId', 
				textField:'producttypeName',
				width:'100',
				onLoadSuccess: function () { //加载完成后,设置选中第一项
				/* $.messager.alert("提示", 'sdasdsa','info'); */
					if( producttypeId==null ){ 
                		var val = $(this).combobox("getData");
                			for (var item in val[0]) {
                    			if (item == "producttypeId") {
                       				$(this).combobox("select", val[0][item]);
                    			}
              				}
           			 }else{
           			$(this).combobox("select", producttypeId);
           			} 
           		}
			}); 
        	
            $("#dialog").dialog({
                resizable: false,
                modal: true,
                buttons: [{ //设置下方按钮数组
                    text: '保存',
                    iconCls: 'icon-save',
                    handler: function () {
                       save();
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        closeDialog();
                    }
                }]
            });
            $("#dialog").dialog('setTitle', title);
            $("#dialog").dialog('open');
        }
/* 关闭Dialog*/
        function closeDialog() {  
            $("#form").form('clear'); // 清空form的数据
            $("#dialog").dialog('close');// 关闭dialog
        }
/* 添加数据*/
        function add() {
            openDialog('添加商品'); // 显示添加数据dialog窗口
            $("#form").form('clear'); // 清空form的数据
            url = 'addProduct.action'; //后台添加数据action
        }
/* 修改数据*/
        function edit() {
            var row = $('#grid').datagrid('getSelected'); //// 得到选中的一行数据
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            
            /* $("#form").form('load', row); // 加载选择行数据 */
            /* $.messager.alert("提示", row[0],'info');
            $.messager.alert("提示", row[4],'info');
            $.messager.alert("提示", row[3],'info'); */
            $("#form").form('load', {"product.productNo":row.productNo,"product.productName":row.productName,"product.description":row.description,"product.price":row.price}); // 加载选择行数据
            openDialog('修改商品',row.producttypeId); // 显示更新数据dialog窗口
            url = 'modifyProduct.action?product.productId='+row.productId; //后台更新数据action
        }
/* 保存数据*/
        function save(){
            $('#form').form('submit',{
                url: url,  //提交地址
                onSubmit: function(){
                    return $(this).form('validate'); //前台字段格式校验
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        closeDialog();// 调用closeDialog;    
                        reload();// 重新加载
                        $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        });
                    } else {              
                        $.messager.show({   //显示错误信息
                            title: '错误',
                            msg: result.msg
                        });
                    }
                }
            });
        }
/* 删除数据*/
        function remove(){
            var row = $('#grid').datagrid('getSelected');
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            $.messager.confirm('确认', '确定要删除吗？', function (r) {
                    if (r) {
                        //提交到后台的action
                        $.post('deleteProduct.action', { id: row.productId }, function (result) { 
                            if (result.success) {
                                reload();
                                $.messager.show({   //显示正确信息
                                    title: '提示',
                                    msg: result.msg
                                });                                 
                            } else {
                                $.messager.show({   //显示错误信息
                                    title: '错误',
                                    msg: result.msg
                                });
                            }
                        }, 'json');
                    }
                });
             
        }
/* 刷新grid*/
        function reload(){
            $('#grid').datagrid('reload');
        }
/* jquery入口*/
$(function() {
    loadgrid(); //加载datagrid
});
/* 加载datagrid列表*/
    function loadgrid(){
        $('#grid').datagrid({
        title : '商品',
        url : 'getProducts.action',
        loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        fit : true,  //窗口自适应
        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        fitColumns : true, // 自动适应列宽
        singleSelect : true, // 每次只选中一行
        sortName : 'productId', //默认排序字段
        sortOrder : 'asc', // 升序asc/降序desc
        striped : true,  // 隔行变色  
        pagination : true,  // 在底部显示分页工具栏
        pageNumber : 1, //初始化页码 
        pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        pageList : [ 5, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        //rownumbers : true, // 在最前面显示行号 
        idField : 'id', // 主键属性
        // 冻结列,当很多咧出现滚动条时该列不会动
        frozenColumns : [ [
        {title : '序号', width : '100', field : 'productId', sortable : true}, 
        {title : '商品编号', width : '100', field : 'productNo', sortable : true}, 
        {title : '商品名字', width : '100',    field : 'productName',sortable : true}
        ] ],
        columns : [ [ 
        {title : '分类名', width : '200', field : 'producttypeName', sortable : false
        /* formatter: function(value,row,index){
		return new Object(row["producttype"]).producttypeName;
		}  */
        },
        {title : '分类id', width : '200', field : 'producttypeId', sortable : false /* , hidden:true */
        /* formatter: function(value,row,index){
		return new Object(row["producttype"]).producttypeId;
		}  */
        },
        {title : '描叙', width : '200', field : 'description', sortable : false}, 
        {title : '价格', width : '200',    field : 'price', sortable : false}
        ] ],       
        // 工具栏按钮
        toolbar: [
"-", {id: 'add', text: '添加',    iconCls: 'icon-add', handler: function () { add()} },
"-", {id: 'edit', text: '修改',   iconCls: 'icon-edit',   handler: function () { edit()} }, 
"-", {id: 'remove', text: '删除',iconCls: 'icon-remove', handler: function () {remove()} }, 
"-", {id: 'reload',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload()} } 
        ]
    });
}
</script>
    </head>
    <body>
       <div style="width:100%;height:100%;padding:0px;overflow:hidden">
           <table id="grid"></table>
       </div>
       <div id="dialog" class="easyui-dialog" data-options="closed:true" title="客户管理" style="width:250px;height:200px;text-align:center" >
              <form id="form" method="post">
                     <div>
                        <label>商品编号</label>
                        <input name="product.productNo" class="easyui-textbox"  required="true"  />
                 </div>
                 <div>
                        <label>商品名字</label>
                        <input name="product.productName" class="easyui-textbox"  data-options="field:'productName',required:true" />
                 </div>
                 <div>
                        <label>描叙</label>
                        <input name="product.description" class="easyui-textbox"  data-options="field:'description',required:true" />
                 </div>
                 <div>
                        <label>价格</label>
                        <input name="product.price" class="easyui-numberbox"  missingMessage="必须填写1~1000之间的数字" min="0" max="1000" precision="2"  data-options="field:'price',required:true"/>
                 </div>
                 <div>
                        <label>商品分类</label>
                        <select id="select" name="product.producttype.producttypeId" class="easyui-combobox" /></select>
                 </div>
              </form>
       </div>
    </body>
</html>