<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<base target="_self">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<style type="text/css">
<!--
table{
border-collapse: collapse;
}
th,td{
border: thin solid black;
width:200px;
}
-->
</style>
  </head>
  <script type="text/javascript">
  /* window.onload = function () {
	var oTable = document.getElementById("table");
	var colCells = oTable.cells;
	for (var i=0; i<colCells.length; i++)
	{
    	colCells[i].onclick = function ()
    		{
        	alert("行" + (this.parentNode.rowIndex + 1) + " : 列" + (this.cellIndex + 1));
    		}	
	}
} */

window.onload = function (){  
    var console=document.getElementById('console');  
    var tab=document.getElementById('table');  
    var trs=tab.getElementsByTagName('tr');  
    for(var i=1;i<trs.length;i++){  
        var tr=trs[i];  
        tr.onmouseover=function(){  
            this.style.background="#00CC33";
            this.style.color="white";  
        }  
        tr.onmouseout=function(){  
            this.style.background="#ffffff";  
            this.style.color="black"; 
        }  
        tr.ondblclick=(function(){  
            var rowNum=i;  
            return function(){  
                console.innerHTML="点击了第"+rowNum+"行";  
                window.returnValue=tab.rows[rowNum].cells[0].innerText;
    			window.close();
            }  
        })(); 
    }  
}  
	</script>
	<body>
       <table id="table">
       <tr><th>商品ID</th><th>商品名</th><th>商品号</th><th>描述</th><th>价格</th><th>类别</th></tr>
       <s:if test="#session['products']==null">
      	 为什么是空的呢？
       </s:if>
       <s:iterator value="#session['products']" id="product"> 
       <tr><td><s:property value="#product.productId"/></td>
       <td><s:property value="#product.productName"/></td>
       <td><s:property value="#product.productNo"/></td>
       <td><s:property value="#product.description"/></td>
       <td><s:property value="#product.price"/></td>
       <td><s:property value="#product.producttype.producttypeName"/></td>
       <%-- <td><a href="detailProduct.action?product.productId=<s:property value="#product.productId"/>">修改</a></td>
       <td><a href="deleteProduct.action?product.productId=<s:property value="#product.productId"/>"  onclick="if(!confirm('你确定要删除吗？')){return false;};">确认删除</a></td> --%>
       </tr>
       <%-- <tr>
       <s:iterator value="#producttype.products" id="product">
       <td><s:property value="#product.productId"/></td>
       <td><s:property value="#product.productName"/></td>
       <td><s:property value="#product.productNo"/></td>
       </s:iterator>
       </tr> --%>
       </s:iterator>
        </table>
         <div id="console" style="background:#ffff00"></div> 
</body>
</html>
