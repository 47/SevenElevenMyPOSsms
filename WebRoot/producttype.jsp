<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
		<script type="text/javascript">
/* 显示Dialog*/
        function openDialog(title){
            $("#dialog").dialog({
                resizable: false,
                modal: true,
                buttons: [{ //设置下方按钮数组
                    text: '保存',
                    iconCls: 'icon-save',
                    handler: function () {
                       save();
                    }
                }, {
                    text: '取消',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        closeDialog();
                    }
                }]
            });
            $("#dialog").dialog('setTitle', title);
            $("#dialog").dialog('open');
        }
/* 关闭Dialog*/
        function closeDialog() {  
            $("#form").form('clear'); // 清空form的数据
            $("#dialog").dialog('close');// 关闭dialog
        }
/* 添加数据*/
        function add() {
            openDialog('添加商品分类'); // 显示添加数据dialog窗口
            $("#form").form('clear'); // 清空form的数据
            url = 'addProducttype.action'; //后台添加数据action
        }
/* 修改数据*/
        function edit() {
            var row = $('#grid').datagrid('getSelected'); //// 得到选中的一行数据
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            openDialog('修改客户'); // 显示更新数据dialog窗口
            $("#form").form('load', {"producttype.producttypeName":row.producttypeName,"producttype.description":row.description}); // 加载选择行数据
            url = 'modifyProducttype.action?producttype.producttypeId='+row.producttypeId; //后台更新数据action
        }
/* 保存数据*/
        function save(){
            $('#form').form('submit',{
                url: url,  //提交地址
                onSubmit: function(){
                    return $(this).form('validate'); //前台字段格式校验
                },
                success: function(result){
                    var result = eval('('+result+')');
                    if (result.success){
                        closeDialog();// 调用closeDialog;    
                        reload();// 重新加载
                        $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        });
                    } else {              
                        $.messager.show({   //显示错误信息
                            title: '错误',
                            msg: result.msg
                        });
                    }
                }
            });
        }
/* 删除数据*/
        function remove(){
            var row = $('#grid').datagrid('getSelected');
            //如果没有选中记录
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            $.messager.confirm('确认', '确定要删除吗？', function (r) {
                    if (r) {
                        //提交到后台的action
                        $.post('deleteProducttype.action', { id: row.producttypeId }, function (result) { 
                            if (result.success) {
                                reload();
                                $.messager.show({   //显示正确信息
                                    title: '提示',
                                    msg: result.msg
                                });                                 
                            } else {
                                $.messager.show({   //显示错误信息
                                    title: '错误',
                                    msg: result.msg
                                });
                            }
                        }, 'json');
                    }
                });
             
        }
/* 刷新grid*/
        function reload(){
            $('#grid').datagrid('reload');
        }
/* jquery入口*/
$(function() {
    loadgrid(); //加载datagrid
});
/* 加载datagrid列表*/
    function loadgrid(){
        $('#grid').datagrid({
        title : '客户',
        url : 'getProducttypes',
        loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        fit : true,  //窗口自适应
        nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        fitColumns : true, // 自动适应列宽
        singleSelect : true, // 每次只选中一行
        sortName : 'producttypeId', //默认排序字段
        sortOrder : 'asc', // 升序asc/降序desc
        striped : true,  // 隔行变色  
        pagination : true,  // 在底部显示分页工具栏
        pageNumber : 1, //初始化页码 
        pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        pageList : [ 5, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        //rownumbers : true, // 在最前面显示行号 
        idField : 'id', // 主键属性
        // 冻结列,当很多咧出现滚动条时该列不会动
        frozenColumns : [ [
        {title : '序号', width : '100', field : 'producttypeId', sortable : true}, 
        {title : '商品分类名', width : '100', field : 'producttypeName', sortable : true}
        ] ],
        columns : [ [ 
        {title : '描述', width : '100', field : 'description', sortable : false}
        ] ],       
        // 工具栏按钮
        toolbar: [
"-", {id: 'add', text: '添加',    iconCls: 'icon-add', handler: function () { add()} },
"-", {id: 'edit', text: '修改',   iconCls: 'icon-edit',   handler: function () { edit()} }, 
"-", {id: 'remove', text: '删除',iconCls: 'icon-remove', handler: function () {remove()} }, 
"-", {id: 'reload',  text: '刷新',iconCls: 'icon-reload', handler: function () {reload()} } 
        ]
    });
}
</script>
    </head>
    <body>
       <div style="width:100%;height:100%;padding:0px;overflow:hidden">
           <table id="grid"></table>
       </div>
       <div id="dialog" class="easyui-dialog" data-options="closed:true" title="客户管理" style="width:250px;height:200px;text-align:center" >
              <form id="form" method="post">
                     <div>
                        <label>商品分类名</label>
                        <input name="producttype.producttypeName" data-options="required:true" />
                 </div>
                 <div>
                        <label>描述</label>
                        <input name="producttype.description" data-options="required:true" />
                 </div>
              </form>
       </div>
    </body>
</html>