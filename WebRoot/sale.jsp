<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="utf-8">
        <title>easyui学习笔记</title>
        <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
        <style type="text/css">
		/*表格行高*/
		.datagrid-row {
	        height: 20px;/* 本来是34 */
	    }
	    #select_customer .datagrid-row {
	        height: 30px;
	    }
	    #select_goods .datagrid-row {
	        height: 30px;
	    }
	    #p_receipt .datagrid-row {
	        height: 30px;
	    }
	    .quick {
	    	background-color:#c0ffff;
	    	margin:3px 5px;
	    	padding:15px 0 0 0px;
	    	float:left;
	    	height:100px;
	    	width:61px;
	    	border-radius:5px;
			-moz-border-radius:5px;
			font-size:22px;
			cursor:pointer;
			text-align:center;
			border: 1px #666 solid;
	    }
	    fieldset {
	    	border-radius:3px;
	    }
	    input {
	    	font-family:Microsoft YaHei;
	    }
	    /*表格行样式*/
        .datagrid-row td div {
        	font-size:12px;/* 本来是18 */
        	font-family:Microsoft YaHei;
        	color:black;
        }
        #select_goods .datagrid-row td div,#select_customer .datagrid-row td div,#get_suspend_order .datagrid-row td div{
        	font-size:12px;
        }
        #receipt_dialog .datagrid-row td div,#p_receipt .datagrid-row td div{
        	font-size:12px;
        }
	</style>
		<script type="text/javascript">
		$(function() {
			loadOrder();
			loadSalegrid();
		});
		function loadOrder(){
			$.ajax({
             type:'POST',
             url:'saleLoadOrder.action',
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            	 	$('#checkout_grid').datagrid('reload');
            		$('#saleorderNo').attr('value',r.saleorderNo);
            		$('#cashierName').attr('value',r.cashierName);
            		$('#cashierId').attr('value',r.cashierId);
            		$('#s_totalprice').attr('value',"0.0");
            		$('#s_amount').attr('value',"0");
            		$('#s_discount').attr('value',"1.0");
            		$('#receivable').attr('value',"0.00");
            		$('#customerId').val('');
            		$('#customerName').attr('value',"");
            		$('#discount').attr('value',"");
            		$('#inputCustomerId').val('');
            		$('#goodsinput').val('');
            		$('#goodsinput').focus();
 				 }
             }
         	});
		}
		function loadSalegrid(){
			$('#checkout_grid').datagrid({
        		title : '交易单',
        		url : 'saleGetSaleorderitems.action',
        		loadMsg : '正在加载…',  //当从远程站点载入数据时，显示的一条快捷信息。
        		fit : true,  //窗口自适应
        		nowrap: false, //设置为true，当数据长度超出列宽时将会自动截取
        		fitColumns : true, // 自动适应列宽
        		singleSelect : true, // 每次只选中一行
        		sortName : 'saleorderId', //默认排序字段
        		sortOrder : 'desc', // 升序asc/降序desc
        		striped : true,  // 隔行变色  
        		pagination : false,  // 在底部显示分页工具栏
        		pageNumber : 1, //初始化页码 
        		pageSize : 5,  // 指定每页的大小，服务器要加上page属性和total属性
        		pageList : [ 5, 20, 30, 50 ], // 可以设置每页记录条数的列表，服务器要加上rows属性
        		//rownumbers : true, // 在最前面显示行号 
        		idField : 'id', // 主键属性
        		columns : [ [ 
        		{title : '商品id', width : '100', field : 'productId', sortable : false
        		},
        		{title : '商品名', width : '300', field : 'productName', sortable : false
        		},
        		{title : '类别名', width : '100', field : 'producttypeName', sortable : false
        		},
        		{title : '单价格', width : '100', field : 'price', sortable : false
        		},
        		{title : '总价格', width : '100', field : 'totalprice', sortable : false
        		},
        		{title : '数量', width : '200', field : 'quantity', sortable : false
        		}
        		] ],       
        		// 工具栏按钮
        		toolbar: [
				"-", {id: 'plus', text: '数量+1',    iconCls: 'icon-add', handler: function () { modifySaleorderitem("plus")} },
				"-", {id: 'reduce', text: '数量-1',   iconCls: 'icon-remove',   handler: function () {  modifySaleorderitem("reduce")} }, 
				"-", {id: 'remove', text: '删除',iconCls: 'icon-no', handler: function () {removeSaleorderitem()} }, 
				"-", {id: 'reload',  text: '清空',iconCls: 'icon-print', handler: function () {clearSaleorderitem()} },
				"-"
        		],
        		onLoadSuccess: function () {
            		$('.datagrid-toolbar').append($('#searchTool'));
            		$("#searchTool").css('display','inline'); 
        		}
    		});
		}
		function loadCustomer(){
			$.ajax({
             type:'POST',
             url:'saleLoadCustomer.action?id='+$('#inputCustomerId').val(),
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            	 	$('#customerId').val(r.customerId);
            		$('#customerName').attr('value',r.customerName);
            		$('#discount').attr('value',r.discount+"%");
            		$('#s_discount').attr('value',r.discount/100);
            		$('#receivable').attr('value',($('#s_discount').val()*$('#s_totalprice').val()).toFixed(2));
            		$('#goodsinput').focus();
 				 }else{
 				 	$.messager.alert("提示", "该客户不存在",'info');
 				 	$('#inputCustomerId').val('');
 				 	$('#customerId').val('');
 				 	$('#customerName').attr('value',"");
            		$('#discount').attr('value',"");
            		$('#s_discount').attr('value','1.0');
            		$('#receivable').attr('value',($('#s_discount').val()*$('#s_totalprice').val()).toFixed(2));
            		$('#customerId').focus();
 				 }
             }
         	});
		}
		function addSaleorderitem(){
			$.ajax({
             type:'POST',
             url:'saleaddSaleorderitem.action?id='+$('#goodsinput').val(),
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            		checkoutGridReload();
            		$('#goodsinput').val('');
            		$('#s_totalprice').attr('value',r.totalprice.toFixed(2));
            		$('#s_amount').attr('value',r.amount);
            		$('#receivable').attr('value',($('#s_discount').val()*$('#s_totalprice').val()).toFixed(2));
            		$('#goodsinput').focus();
 				 }else{
 				 	$('#goodsinput').val('');
 				 	$.messager.alert("提示", "该商品不存在",'info');
 				 	$('#goodsinput').focus();
 				 }
             }
         	});
		}
		function modifySaleorderitem(order){
			var row = $('#checkout_grid').datagrid('getSelected');
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
			$.ajax({
             type:'POST',
             url:'salemodifySaleorderitem.action?id='+row.productId+'&order='+order+'&quantity='+$('#modifySaleorderitemAmount').val(),
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            		checkoutGridReload();
            		$('#s_totalprice').attr('value',r.totalprice);
            		$('#s_amount').attr('value',r.amount);
            		$('#receivable').attr('value',($('#s_discount').val()*$('#s_totalprice').val()).toFixed(2));
            		/* $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        }); */
 				 }else{
 				 	// $.messager.alert("提示", "该商品不存在",'info');
 				 }
             }
         	});
		}
		function removeSaleorderitem(){
			var row = $('#checkout_grid').datagrid('getSelected');
            if(row == null){
                $.messager.alert("提示", "请选择一条记录",'info');
                return;
            }
            $.ajax({
             type:'POST',
             url:'saleremoveSaleorderitem.action?id='+row.productId,
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            		checkoutGridReload();
            		$('#s_totalprice').attr('value',r.totalprice);
            		$('#s_amount').attr('value',r.amount);
            		$('#receivable').attr('value',($('#s_discount').val()*$('#s_totalprice').val()).toFixed(2));
            		/* $.messager.show({    //显示正确信息
                            title: '提示',
                            msg: result.msg
                        }); */
 				 }else{
 				 	// $.messager.alert("提示", "该商品不存在",'info');
 				 }
             }
         	});
		}
		function clearSaleorderitem(){
			$.ajax({
             type:'POST',
             url:'saleclearSaleorderitem.action',
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            		$('#checkout_grid').datagrid('reload');
            		$('#s_totalprice').attr('value',"0.0");
            		$('#s_amount').attr('value',"0");
            		$('#receivable').attr('value',"0.00");
            		$('#goodsinput').attr('value',"");
 				 }else{
 				 	// $.messager.alert("提示", "该商品不存在",'info');
 				 }
             }
         	});
		}
		function openCheckoutDialog(){
			if($('#s_amount').val()!='0'){
				if($('#customerName').val()==""){
					$('#inputCustomerId').val(1);
					loadCustomer();
				}
				$("#checkout_dialog").dialog({
                	resizable: false,
                	modal: true,
                	buttons: [{ //设置下方按钮数组
                    	text: '确认付款',
                    	iconCls: 'icon-save',
                    	handler: function () {
                       		checkout();
                    	}
                	}, {
                    	text: '取消',
                    	iconCls: 'icon-cancel',
                    	handler: function () {
                        	$("#checkout_dialog").dialog('close');
                    	}
                	}]
            	});
            	$("#checkout_dialog").dialog('open');
            	$('#c_totalprice').attr('value',$('#s_totalprice').val());
            	$('#c_discount').attr('value',$('#s_discount').val());
            	$('#c_receivable').attr('value',$('#receivable').val());
            	$('#c_cash').attr('value',$('#receivable').val());
            }else{
            	$.messager.alert("提示", "当前销售为空",'info');
            }
		}
		function oddChange(){
			$('#c_change').attr('value',($('#c_cash').val()-$('#c_receivable').val()).toFixed(2));
		} 
		function checkout(){
			$.ajax({
             type:'POST',
             url:'salecheckout.action?id='+$('#customerId').val()+'&quantity='+$('#receivable').val(),
             dataType : 'json',
             cache : false,
             success:function(r){
            	 if (r.success){
            	 	loadOrder();
            		checkoutGridReload();
            		$("#checkout_dialog").dialog('close');
            		$.messager.show({    //显示正确信息
                            title: '提示',
                            msg: '付款成功'
                        });
                    openReceiptdialog(r.paytime);
 				 }else{
 				 	// $.messager.alert("提示", "该商品不存在",'info');
 				 }
             }
         	});
		}
		function openReceiptdialog(paytime){
			var rows = $('#checkout_grid').datagrid('getRows');
			$('#receipt_table').append('<tr><td>商品</td><td>单价</td><td>数量</td><td>金额</td></tr>');
						for (var i = 0; i < rows.length; i++) {
			 				var tableRow = '<tr>' +
             				'<td>' + rows[i]['productName'] + '</td>' +
             				'<td>' + rows[i]['price'].toFixed(2) + '</td>' +
             				'<td>' + rows[i]['quantity'] + '</td>' +
             				'<td>' + rows[i]['totalprice'].toFixed(2) + '</td>' +
             				'</tr>';
			 				$('#receipt_table').find('tr').css('display','table-row');
			 				$('#receipt_table').append(tableRow);
			
        				}
        				
                        $('#r_saleorderNo').html($('#saleorderNo').val());
						$('#r_username').html($('#cashierName').val());
						$('#r_customerName').html($('#customerName').val());
						$('#r_quantity').html($('#s_amount').val());
						$('#r_discount').html($('#s_discount').val()*100+"%");
						$('#r_totalprice').html($('#s_totalprice').val());
						var date1 = new Object(paytime).time;
        				var date = new Date(date1);
						$('#r_paytime').html(date.toLocaleString());
                        $("#receipt_dialog").dialog({
                			resizable: false,
                			modal: true,
                			buttons: [{ //设置下方按钮数组
                				id:'receiptComfirm',
                    			text: '确定',
                    			iconCls: 'icon-save',
                    			handler: function () {
                       			$("#receipt_dialog").dialog('close');
                    			}
                			}]
            			});
            			$("#receipt_dialog").dialog('setTitle',"小票");
            			$("#receipt_dialog").dialog('open');
            			$("#receiptComfirm").focus();
		}
		function saveOrder(){
			if($('#s_amount').val()!='0'){
				if($('#customerName').val()==""){
					$('#inputCustomerId').val(1);
					loadCustomer();
				}
				$.ajax({
             		type:'POST',
             		url:'salesaveOrder.action?id='+$('#customerId').val()+'&quantity='+$('#receivable').val(),
             		dataType : 'json',
             		cache : false,
             		success:function(r){
            	 		if (r.success){
            	 			loadOrder();
            				checkoutGridReload();
            				$.messager.alert("提示","订单号为"+r.saleorderNo,'info');
            				$.messager.show({    //显示正确信息
                            		title: '提示',
                            		msg: '保存订单成功'
                        		});
 						}else{
 				 			// $.messager.alert("提示", "该商品不存在",'info');
 				 		}
             		}
         		});
         	}else{
            	$.messager.alert("提示", "当前销售为空",'info');
            }
		}
		function reloadOrder(){
			$.messager.confirm('提示', '确定新建销售？', function(r){
	            if (r){
	            	loadOrder();
	            }else{
	            	return ;
	            }
	        });
		}
		function checkoutGridReload(){
			$('#checkout_grid').datagrid('reload');
		}
		</script>
    </head>
    <body>
    <div style="width:100%;height:100%;padding:0;overflow:hidden;color:#333;">
	    <div style="font-family:Microsoft YaHei;margin:0;padding-bottom:8px;font-size:14px;">
	    	
		    	<fieldset id="top" style="padding:6px;margin:0;">
		    		<div style="float:left;margin:0;padding-right:13px;">
		    			<span>销售单号:</span>
		    			<input id="saleorderNo" readonly
		    				style="font-size:18px;width:180px;height:25px;border:none;" />
		    		</div>
		    		<div style="float:left;margin:0;padding-right:10px;">
		    			<span>收银员:</span>
		    			<input readonly value='' id="cashierName"
		    				style="font-size:18px;width:80px;height:25px;border:none;" />
		    			<input id="cashierId" type="hidden" >
		    		</div>
			    	<form action="" method="post" id="customer_details" style="margin:0;padding:0;">	
			    		<div style="float:left;margin-left:50px;padding-right:10px;">
			    			<span>会员号:</span>
			    			<input id="inputCustomerId" name="customerId" type="text" maxlength="5"  onkeyup="value=value.replace(/[^\d]/g,'')" style="width:120px;height:25px;" />
			    			<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="loadCustomer();" >查询</a>
			    		</div>
			    	</form>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员Id:</span>
			    			<input readonly id="customerId" style="width:30px;height:25px;font-size:18px;border:none;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员姓名:</span>
			    			<input readonly id="customerName" name="name"  style="width:80px;height:25px;font-size:18px;border:none;" />
			    		</div>
			    		<div style="float:left;margin:0;padding-right:10px;">
			    			<span>会员折扣:</span>
			    			<input readonly name="discount" id="discount" style="width:80px;height:25px;font-size:18px;border:none;"/>
			    		</div>
		    		
		    		<div id="time" style="float:right;font-family:'Verdana';font-size:14px;"></div>
		    	
		    	</fieldset>
	    	
	    </div>	
	    
    	<div style="height:280px;width:100%;margin-top:0;" id="checkout_grid_div">
	    	<!-- 表格 -->
			<table id="checkout_grid"></table>
			<span id="searchTool" style="display: none;">
       			 <input class="easyui-numberbox"  min="0" max="1000" precision="0" id="modifySaleorderitemAmount"/><a href="javascript:modifySaleorderitem('modify');" iconcls="icon-search" class="easyui-linkbutton" plain="true"></a>
    		</span>
		</div>
		
		<div style="font-family:Microsoft YaHei;height:100%;font-size:14px;">
			<fieldset style="float:left;height:142px;width:280px;">
				<legend>输入框</legend>
				<div style="margin:20px 0 5px 0;font-weight:bold;">请输入商品条码</div>
					<input type="text" id="goodsinput" maxlength="5"  onkeyup="value=value.replace(/[^\d]/g,'')" onkeydown="if(event.keyCode==13) addSaleorderitem();" style="border: 1px #666 solid;border-radius:5px;height:50px;width:215px;font-size:36px;">
					<a href="#" class="easyui-linkbutton" data-options="plain:true" style="border: 1px #666 solid;background-color:#c0ffff;margin-top:-23px;height:50px;width:60px;font-size:30px" onclick="addSaleorderitem();" >加入</a>
			</fieldset>
			
			<fieldset style="float:left;height:142px;width:500px;margin-left:20px">
				<legend>账单信息</legend>
				<div style="margin:0;padding:0;width:200px;font-size:12px;float:left;">
					<div style="float:right;margin-top:0;">
						<span>合计数量:</span>
						<input type="text" readonly value="0" id="s_amount" 
							style="border:none;width:140px;font-size:20px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>合计金额:</span>
						<input type="text" readonly id="s_totalprice"  value="0.0" 
							style="border:none;width:140px;font-size:20px;">
					</div>
					<div style="margin-top:3px;float:right">
						<span>整单打折:</span>
						<input type="text" readonly name="discount"  value="1.0" id="s_discount" 
							style="border:none;width:140px;font-size:20px;">
					</div>
				</div>
					
				<div style="margin:5px;font-size:22px;float:left;">
					<div style="color:black;padding-left:10px;">应收金额:</div>
					<input type="text" readonly id="receivable"  value="0.0" 
						style="border:none;width:200px;font-size:50px;">
				</div>
				
			</fieldset>
			
			<fieldset id="" style="float:left;height:142px;width:255px;margin-left:20px">
				<legend>功能</legend>
				<div class="quick" id="paybutton" onclick="openCheckoutDialog()">付款</div>
				<div class="quick" id="reloadbutton" onclick="reloadOrder()">重开销售</div>
				<div class="quick" id="savebutton" onclick="saveOrder()">保存销售</div>
			</fieldset>
		</div>
		<div id="checkout_dialog" class="easyui-dialog" data-options="closed:true" title="付款" style="width:300px;height:350px;text-align:center" >
              <form id="form" method="post">
                 <div style="margin-top:10px">
                        <label style="font-size:25px">合计金额</label>
						<input id="c_totalprice" readonly name="c_totalprice" data-options="field:'totalPrice',required:true"  style="text-align:center;border:none;font-size:25px;width:180px">
                 </div>
                 <div style="margin-top:10px">
                        <label style="font-size:25px">折扣</label>
						<input id="c_discount" readonly name="c_discount" data-options="field:'totalPrice',required:true"  style="text-align:center;border:none;font-size:25px;width:180px">
                 </div>
                 <div style="margin-top:10px">
                        <label style="font-size:25px">应收金额</label>
						<input id="c_receivable" readonly name="c_receivable"  style="text-align:center;border:none;font-size:25px;width:180px">
                 </div>
                 <div style="margin-top:10px">
                        <label style="font-size:25px">实收款</label>
                   	    <input id="c_cash" name="c_cash" data-options="field:'cash',required:true" type="text" style="text-align:center;font-size:25px;width:180px" onchange="oddChange()">
                 </div>
                 <div style="margin-top:10px">
                        <label style="font-size:25px">找零</label>
                        <input id="c_change" readonly name="c_change" data-options="field:'change',required:true"  value="0.0" style="text-align:center;border:none;font-size:25px;width:180px">
                 </div>
              </form>
       </div>
       <div id="receipt_dialog" class="easyui-dialog" data-options="closed:true" title="小票" style="width:250px;font-size:14px;padding:0 5px;">
    		<div style="text-align:center;font-size:18px;margin:6px;">12345</div>
    		<div>流水号：<span id="r_saleorderNo"></span></div>
    		<div>收银员：<span id="r_username"></span></div>
    		<div style="text-align:center;"><hr></div>
    		<div>
    			<table id="receipt_table" style="font-size:14px;"></table>
    		</div>
    	
    		<div style="text-align:center;"><hr></div>
    		<table style="font-size:14px;">
    			<tr>
    				<td><div>会员：<span id="r_customerName"></span></div></td>
    			</tr>
    			<tr>
    				<td><div>数量：<span id="r_quantity"></span></div></td>
    			</tr>
    			<tr>
    				<td><div style="width:150px;">折扣：<span id="r_discount"></span></div></td>
    			</tr>
    			<tr>
    				<td><div>合计金额：<span id="r_totalprice"></span></div></td>
    			</tr>
    		</table>
    	
    		<div style="text-align:center;"><hr></div>
    		<div>交易时间：<span id="r_paytime"></span></div>
    		<div style="text-align:center;"><hr></div>
    		<div>本小票为退货凭据，请保留小票以便查核</div>
    		<div>发票于当月内开具，过期无效</div>
    		<div>多谢惠顾，欢迎再次光临</div>
    		<br />
    	</div>
    </div>
    </body>
</html>