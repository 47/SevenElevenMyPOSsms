<%@page contentType="text/html; charset=utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <link id="easyuiTheme" rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/default/easyui.css" charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="jslib/easyui-1.4.2/themes/icon.css" charset="utf-8"/>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/jquery.easyui.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="jslib/easyui-1.4.2/locale/easyui-lang-zh_CN.js" charset="utf-8"></script>
    <script type="text/javascript" src="dwr/engine.js"></script>
	<script type="text/javascript" src="dwr/util.js"></script>
	<script type="text/javascript" src="dwr/interface/SaleorderDAOAjax.js"></script>
	<script type="text/javascript" src="dwr/interface/ProductServiceAjax.js"></script>
    <title>My JSP 'showCart.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<style type="text/css">
<!--
table{
border-collapse:collapse;
}
th,td{
border: thin solid black;
width:150px;
height:50px;
text-align: center;
}
/* button{
border-style: none;
background-color:#FF0000;
} */
-->
</style>
  </head>
  
  
  
  <script type="text/javascript">
    function addCart(){
    	/* alert(table1.rows[2].cells[0].innerText);
    	alert(selectRow.cells[0].innerText); */
      var productId=enterProductId.value;
      SaleorderDAOAjax.addCart(productId,showProduct);
    }
    function showProduct(arr){
    if(arr[0]!='商品不存在'){
    for(var i=0;i<table1.rows.length;i++){
    	if(table1.rows[i].cells[0].innerText==arr[0]){
    		table1.deleteRow(i);
    	}
    }
     var newTr = table1.insertRow(3);
     var newTd0 = newTr.insertCell();
     var newTd1 = newTr.insertCell();
     var newTd2 = newTr.insertCell();
     var newTd3 = newTr.insertCell();
     var newTd4 = newTr.insertCell();
     var newTd5 = newTr.insertCell();
     var newTd6 = newTr.insertCell();
     var newTd7 = newTr.insertCell();
     newTd0.innerText= arr[0];
     newTd1.innerText= arr[1];
     newTd2.innerText= arr[2];
     newTd3.innerText= arr[3];
     newTd4.innerText= arr[4];
     newTd5.innerText= arr[5];
     newTd6.innerHTML= '<input type="text" name="quantity" value="'+arr[5]+'" onchange="updateCart('+arr[0]+',this.value)">';
     newTd7.innerHTML= '<input type="submit" value="删除" onclick="deleteCart('+arr[0]+');">';
     totalPrice.innerText= "总金额为"+arr[6];
     totalPrice1.style.display="none";
     totalPrice.style.display="block";
     /* submitProductId.focus(); */}
     else{
     alert(arr[0]);
     }
    } 
    
    
    
    function clearCart(){
    	if(table1.rows.length>3){
    	SaleorderDAOAjax.clearCart(clearTable);
    	}
    }
    function clearTable(respond){
     if(respond=="SUCCESS"){
     	for(var i=3;i<table1.rows.length;){
    		table1.deleteRow(i);
        }
      totalPrice.innerText= "总金额为0.0";
      totalPrice1.innerText= "总金额为0.0";
     }
    }
    
    
    function deleteCart(productId){
    	SaleorderDAOAjax.deleteCart(productId,deleteLine);
    }
    function deleteLine(arr){
     	for(var i=0;i<table1.rows.length;i++){
    		if(table1.rows[i].cells[0].innerText==arr[0]){
    		table1.deleteRow(i);
    		}
    	}
    	totalPrice.innerText= "总金额为"+arr[1];
     	totalPrice1.style.display="none";
     	totalPrice.style.display="block";
    }
    
    
    function updateCart(productId,quantity){
    	SaleorderDAOAjax.updateCart(productId,quantity,updateLine);
    } 
    function updateLine(arr){
        for(var i=0;i<table1.rows.length;i++){
    		if(table1.rows[i].cells[0].innerText==arr[0]){
    			table1.rows[i].cells[4].innerText=arr[1];
    			table1.rows[i].cells[5].innerText=arr[2];
    		}
    	}
    	totalPrice.innerText= "总金额为"+arr[3];
     	totalPrice1.style.display="none";
     	totalPrice.style.display="block";
    }
    function checkout(){
    	if(table1.rows.length>3){
    	/* customerId = window.prompt("有会员卡吗？",1,"center:yes;"); */
    	customerId =window.showModalDialog("enterCustomerIdWindow.jsp",1,"dialogWidth=350px;dialogHeight=400px;center:yes;help:no;resizable:no;status:no");
    		if(customerId!=null&&customerId!="no"){
    		SaleorderDAOAjax.checkout(customerId,checkourWindow);
    		}
    	}else{
    		alert("当前订单为空");
    	}
    }
    function checkourWindow(arr){
    	str =window.showModalDialog("checkoutWindow.jsp",arr,"dialogWidth=350px;dialogHeight=400px;center:yes;help:no;resizable:no;status:no");
    	if(str!="no"&&str!=null){
    		SaleorderDAOAjax.pay(str);
    		clearTable("SUCCESS"); 
    		alert("付款成功");
    	}else if(str=="no"){
    		boolean = confirm("要保存订单吗？");
    		if(boolean==true){
    			/* customerId = window.prompt("有会员卡吗？",1); */
    			if(customerId!=null){
    			SaleorderDAOAjax.saveOrder(/* customerId */);
    			clearTable("SUCCESS");
    			alert("订单保存成功"); }
    		}
    	}
    }
    function productSelect(){
    	ProductServiceAjax.browseProduct1();
    	hi = 1;
    	productId =window.showModalDialog("productSelectWindow.jsp",hi,"dialogWidth=800px;dialogHeight=400px;center:yes;help:no;resizable:no;status:no");
    	if(productId!=null){
    	addSelectCart(productId);}
    }
    function addSelectCart(productId){
      SaleorderDAOAjax.addCart(productId,showProduct);
    }
    
  </script>
  
  
  
  
  <body>
产品ID<input id="enterProductId" class="easyui-numberbox"  validType="length[0,3]" invalidMessage="不能超过3个字符！" type="text" name="product.productId" >
	<input id="submitProductId"  type="button" value="选择"  onclick="productSelect()">&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="submitProductId" type="button" value="提交"  onclick="addCart()">
    <input type="button" value="清空当前订单"  onclick="clearCart()" style="margin-left: 150px;">
    <input type="button" value="提交当前订单"  onclick="checkout()" style="margin-left: 150px;">
	<br />
        商品:
    <table id="table1">
       <s:if test="#session.cart.totalPrice!=null">
       <tr><td colspan="8" id="totalPrice1" style="width:1000px;text-align: left;">总金额为<s:property value="#session.cart.totalPrice"/></td></tr>
       </s:if>
       <s:else>
       <tr><td colspan="8" id="totalPrice1" style="width:1000px;text-align: left;">总金额为0.0</td></tr>
       </s:else>
       <tr><td colspan="8" id="totalPrice" style="display:none;width:1000px;text-align: left;"></td>
       <!-- <td><a href="checkout.action">结算</a></td> -->
       </tr>
       <tr><th>商品id</th><th>商品名</th><th>类别名</th><th>单价格</th><th>总价格</th><th>数量</th><th>修改数量</th><th>删除商品</th></tr>
       <s:iterator value="#session.cart.items"> 
       <tr><td><s:property value="value.product.productId"/></td>
       <td><s:property value="value.product.productName"/></td>
       <td><s:property value="value.product.producttype.producttypeName"/></td>
       <td><s:property value="value.product.price"/></td>
       <td><s:property value="value.price"/></td>
       <td><s:property value="value.quantity"/></td>
       <td><input type="text" name="quantity" value="<s:property value="value.quantity"/>"  onchange="updateCart(<s:property value="value.product.productId"/>,this.value)"></td>
       <td><input type="submit" value="删除" onclick="deleteCart(<s:property value="value.product.productId"/>);"></td>
       </tr>
       </s:iterator>
       </table>
  </body>
</html>
