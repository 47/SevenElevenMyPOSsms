package action;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSON;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.struts2.ServletActionContext;









/*import net.sf.json.JSONObject;*/
import service.CustomerService;
import util.Pager;
import vo.Customer;


public class CustomerAction extends ActionSupport{
    public Customer customer;
    public int rows;
	public int page;
	public String order;
	public String sort;
	public int id;
	private CustomerService customerService;
	
	
	public void registerCustomer() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
    	customerService.registerCustomer(customer);
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","注册成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
    }
	public void getCustomers() throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		List allCustomers = customerService.browseCustomer();
		int total = allCustomers.size();
		List customers = customerService.getCustomers(page, rows, order, sort);
		this.toBeJson(customers, total);
    }
	public void toBeJson(List list,int total) throws Exception{  
        HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("total",total);
        /*jobj.accumulate("total",total );//total代表一共有多少数据 */
		JsonConfig config = new JsonConfig();
        config.setExcludes(new String[]{"saleorders"});//除去emps属性
        /*config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);  */ 
		JSONArray json = JSONArray.fromObject(list, config);
		/*JSONArray rows =JSONArray.fromObject(list);
		System.out.println(rows.toString());*/
         jobj.accumulate("rows", json);
		/*jobj.put("rows", list);*/
       /* jobj.accumulate("rows", list);//row是代表显示的页的数据 */
        response.setCharacterEncoding("utf-8");//指定为utf-8  
        response.getWriter().write(jobj.toString());//转化为JSOn格式    
   }  
	
	
	
	
	
	public String browseCustomer(){
		List customers = customerService.browseCustomer();
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("customers",customers);
    	return SUCCESS;
	}
	public void deleteCustomer() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","删除成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		customerService.deleteCustomer(customerService.findById(id));
	}
	public void modifyCustomer() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","修改成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		customerService.modifyCustomer(customer);
	}
	public String detailCustomer(){
		Customer customerInformation = customerService.detailCustomer(customer);
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("customer",customerInformation);
    	return SUCCESS;
	}
	/*public String login() throws Exception{
		Customer customer=CustomerService.validateUsername(customer.getCustomerName());
    	 if(customer==null){
    		 Map request=(Map)ActionContext.getContext().get("request");
     		String errorMessage = "该用户名不存在，请注册!!";
     		System.out.println(request.put("errorMessage",errorMessage));
    		 System.out.println(errorMessage);
     		request.put("errorMessage",errorMessage);
    		 return "error1";
         }
		Customer customer = CustomerService.validateUser(customer.getCustomerId());
        if(customer!=null){
            java.util.Map<String, Object> session=ActionContext.getContext().getSession();
            session.put("customer",customer);
            return SUCCESS;
        }
        Map request=(Map)ActionContext.getContext().get("request");
 		String errorMessage = "密码不正确，请重新输入!!";
     	request.put("errorMessage",errorMessage);
		 System.out.println(errorMessage);
        return ERROR;
   }*/
	
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public CustomerService getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
