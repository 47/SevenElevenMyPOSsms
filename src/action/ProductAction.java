package action;

import service.ProductService;
import service.ProducttypeService;
import vo.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;



public class ProductAction extends ActionSupport{
	protected ProductService productService;
	protected ProducttypeService producttypeService;
	public int rows;
	public int page;
	public String order;
	public String sort;
	public Product product;
	public int id;

	public void addProduct() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        productService.addProduct(product);
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","添加商品成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	public String browseProduct(){
		List products = productService.browseProduct();
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("products",products);
    	return SUCCESS;
	}
	public String browseProduct1(){
		List products = productService.browseProduct();
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
        session.put("products",products);
    	return SUCCESS;
	}
	/*public void deleteProduct(){
		productService.deleteProduct(product);
	}*/
	public String detailProduct(){
		Product productInformation = productService.detailProduct(product);
		List producttypes = producttypeService.browseProducttype();
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("producttypes",producttypes);
    	request.put("product",productInformation);
		return SUCCESS;
	}
	/*public void modifyProduct(){
		productService.modifyProduct(product);
	}*/
	public void modifyProduct() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","修改成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		productService.modifyProduct(product);
	}
	public void deleteProduct() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","删除成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		productService.deleteProduct(id);
	}
	
	
	
	
	
	
	
	public void getProducts() throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();
		List allProducts = productService.browseProduct();
		int total = allProducts.size();
		List products = productService.getProducts(page, rows, order, sort);
		List productsAfter = new ArrayList();
		for(Iterator it = products.iterator();it.hasNext();){
			Product product = (Product)it.next();
			product.setProducttypeName(product.getProducttype().getProducttypeName());
			product.setProducttypeId(product.getProducttype().getProducttypeId());
			productsAfter.add(product);
		}
		this.toBeJson(productsAfter, total);
    }
	public void toBeJson(List list,int total) throws Exception{  
        HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("total",total);
        /*jobj.accumulate("total",total );//total代表一共有多少数据 */
		JsonConfig config = new JsonConfig();
         config.setExcludes(new String[]{/*"producttype",*/"producttype","saleorderitems","salereturnitems"});//除去emps属性
         /*config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);*/
         JSONArray json = JSONArray.fromObject(list, config);
		/*JSONArray rows =JSONArray.fromObject(list);
		System.out.println(rows.toString());*/
         jobj.accumulate("rows", json);
		/*jobj.put("rows", list);*/
       /* jobj.accumulate("rows", list);//row是代表显示的页的数据 */
        response.setCharacterEncoding("utf-8");//指定为utf-8  
        response.getWriter().write(jobj.toString());//转化为JSOn格式    
   }  
	public void deleteProductAjax() throws IOException{
		productService.deleteProductAjax(id);
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","删除成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	
	
	
	
	
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	public ProducttypeService getProducttypeService() {
		return producttypeService;
	}
	public void setProducttypeService(ProducttypeService producttypeService) {
		this.producttypeService = producttypeService;
	}
}