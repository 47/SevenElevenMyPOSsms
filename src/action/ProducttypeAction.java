package action;

import service.ProducttypeService;
import vo.Producttype;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;






public class ProducttypeAction extends ActionSupport{
	protected ProducttypeService producttypeService;
	protected Producttype producttype;
	public int rows;
	public int page;
	public String order;
	public String sort;
	public int id;

	/*public void addProducttype(){
		producttypeService.addProducttype(producttype);
	}*/
	public String browseProducttype(){
		List producttypes = producttypeService.browseProducttype();
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("producttypes",producttypes);
    	return SUCCESS;
	}
	/*public void deleteProducttype(){
		producttypeService.deleteProducttype(producttype);
	}*/
	public String detailProducttype(){
		Producttype producttypeInformation=producttypeService.detailProducttype(producttype);
		Map request=(Map)ActionContext.getContext().get("request");
    	request.put("producttype",producttypeInformation);
    	return SUCCESS;
	}
	/*public void modifyProducttype(){
		producttypeService.modifyProducttype(producttype);
	}*/
	
	
	public void getProducttypesSelect() throws Exception{
		List producttypes = producttypeService.browseProducttype();
		this.toBeJson(producttypes);
    }
	public void toBeJson(List list) throws Exception{  
        HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        /*JSONObject jobj = new JSONObject();*///new一个JSON  
        /*jobj.accumulate("total",total );//total代表一共有多少数据 */
		JsonConfig config = new JsonConfig();
         config.setExcludes(new String[]{"description","products"});//除去emps属性
         JSONArray json = JSONArray.fromObject(list, config);
		/*JSONArray rows =JSONArray.fromObject(list);
		System.out.println(rows.toString());*/
         /*JSONObject jobj = JSONObject.fromObject(json);*/
		/*jobj.put("rows", list);*/
       /* jobj.accumulate("rows", list);//row是代表显示的页的数据 */
        response.setCharacterEncoding("utf-8");//指定为utf-8  
        response.getWriter().write(json.toString());//转化为JSOn格式    
   }  
	public void getProducttypes() throws Exception{
		/*Map<String, Object> map = new HashMap<String, Object>();*/
		List allProducttypes = producttypeService.browseProducttype();
		int total = allProducttypes.size();
		List producttypes = producttypeService.getProducttypes(page, rows, order, sort);
		this.toBeJson(producttypes, total);
    }
	public void toBeJson(List list,int total) throws Exception{  
        HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("total",total);
        /*jobj.accumulate("total",total );//total代表一共有多少数据 */
		JsonConfig config = new JsonConfig();
         config.setExcludes(new String[]{"products"});//除去emps属性
         JSONArray json = JSONArray.fromObject(list, config);
		/*JSONArray rows =JSONArray.fromObject(list);
		System.out.println(rows.toString());*/
         jobj.accumulate("rows", json);
		/*jobj.put("rows", list);*/
       /* jobj.accumulate("rows", list);//row是代表显示的页的数据 */
        response.setCharacterEncoding("utf-8");//指定为utf-8  
        response.getWriter().write(jobj.toString());//转化为JSOn格式    
   }  
	public void addProducttype() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        producttypeService.addProducttype(producttype);
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","添加商品类别成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
    }
	public void deleteProducttype() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest(); 
        producttypeService.deleteProducttype(id);
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","删除成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	public void modifyProducttype() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","修改成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		producttypeService.modifyProducttype(producttype);
	}
	
	
	
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public Producttype getProducttype() {
		return producttype;
	}

	public void setProducttype(Producttype producttype) {
		this.producttype = producttype;
	}

	public ProducttypeService getProducttypeService() {
		return producttypeService;
	}

	public void setProducttypeService(ProducttypeService producttypeService) {
		this.producttypeService = producttypeService;
	}
}