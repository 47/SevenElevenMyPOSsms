package action;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import service.SaleorderService;
import vo.Product;
import vo.Saleorder;
import vo.Saleorderitem;
import vo.User;




public class SaleorderAction extends ActionSupport{
	protected SaleorderService saleorderService;
	public Saleorder saleorder;
    public Saleorderitem saleorderitem;
    public Product product;
    public double quantity;

	
    
	public String addCart(){
		saleorderService.addCart(product.getProductId()/*,quantity*/);
		return SUCCESS;
	}
	/*public void checkout(){
		saleorderService.checkout();
	}*/
	/*public void pay(){
		saleorderService.pay(1);
	}*/
	public String browseSaleorders(){
    	List saleorders = saleorderService.browseSaleorders();
    	/*System.out.println(orders.size());*/
    	Map request=(Map)ActionContext.getContext().get("request");
        request.put("saleorders",saleorders);
        /*System.out.println(saleorders);
        System.out.println(saleorders.size());
        System.out.println(saleorders.get(0));*/
    	/*System.out.println(ordersDAO.findByUserid(user.getId()).size());*/
    	/*for(Iterator it = saleorders.iterator();it.hasNext();){
    		Saleorder saleorder =(Saleorder) it.next();
    		System.out.println(saleorder.getTotalPrice());
    		System.out.println(saleorder.getPayment().getPaymentId());
    	}*/
    	return SUCCESS;
    }
	
	
	
	
	
	
	
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Saleorderitem getSaleorderitem() {
		return saleorderitem;
	}

	public void setSaleorderitem(Saleorderitem saleorderitem) {
		this.saleorderitem = saleorderitem;
	}

	public Saleorder getSaleorder() {
		return saleorder;
	}

	public void setSaleorder(Saleorder saleorder) {
		this.saleorder = saleorder;
	}
	public SaleorderService getSaleorderService() {
		return saleorderService;
	}

	public void setSaleorderService(SaleorderService saleorderService) {
		this.saleorderService = saleorderService;
	}
}