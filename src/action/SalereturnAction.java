package action;

import java.math.BigDecimal;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cart;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;

import service.SaleordersService;
import vo.Product;
import vo.ReturnSaleorderitemSelect;
import vo.Saleorder;
import vo.Saleorderitem;
import vo.Saleordershow;
import vo.Salereturn;
import vo.Salereturnitem;
import vo.User;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;




public class SalereturnAction extends ActionSupport{
	protected int id;
	protected SaleordersService saleordersService;
	protected double quantity;
	protected String reason;
	



	public void returnloadReturn() throws IOException{
		java.util.Map<String, Object> session=ActionContext.getContext().getSession();
		Cart returnCart = (Cart)session.get("returnCart");
    	if(returnCart!=null){
    		session.remove("returnCart");
    	}
    	User user = (User)session.get("user");
		Saleorder saleorder = saleordersService.getSaleorder(id);
		Salereturn salereturn = new Salereturn();
		Date date = new Date();    
    	SimpleDateFormat myFmt1=new SimpleDateFormat("yyMMddHHmmss"); 
    	String salereturnNo = "rt-"+myFmt1.format(date)+"-"+String.format("%03d", user.getUserId());
		salereturn.setCreateDatetime(new Timestamp(date.getTime()));
		salereturn.setSalereturnNo(salereturnNo);
		session.put("saleorder", saleorder);
		session.put("salereturn", salereturn);
		
		
		
		
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("saleorderNo",saleorder.getSaleorderNo());
        jobj.put("salereturnNo",salereturnNo);
        jobj.put("returnCustomerDisount",saleorder.getCustomer().getTelephone());
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	public void returnSaleorderitemSelects() throws IOException{
		Saleorder saleorder = saleordersService.getSaleorder(id);
		List returnSaleorderitemSelects = new ArrayList();
		for(Iterator it = saleorder.getSaleorderitems().iterator();it.hasNext();){
			Saleorderitem saleorderitem = (Saleorderitem)it.next();
			if(saleorderitem.getQuantity()>=1){
			ReturnSaleorderitemSelect returnSaleorderitemSelect = new ReturnSaleorderitemSelect();
			returnSaleorderitemSelect.setProductName(saleorderitem.getProduct().getProductName());
			returnSaleorderitemSelect.setSaleorderitemId(saleorderitem.getSaleorderitemId());
			returnSaleorderitemSelects.add(returnSaleorderitemSelect);
			}
		}
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();   
		JSONArray json = JSONArray.fromObject(returnSaleorderitemSelects);
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(json.toString());
	}
	public void returnaddSalereturnitem() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        JSONObject jobj = new JSONObject(); 
		Saleorderitem saleorderitem = saleordersService.getSaleorderitem(id);
		if(saleorderitem.getQuantity()<quantity){
			jobj.put("success",false);
			jobj.put("msg","退货量大于购买量");
		}else{
    	
			java.util.Map<String, Object> session=ActionContext.getContext().getSession();
			Cart returnCart =(Cart)session.get("returnCart");
			if(returnCart==null){
				returnCart = new Cart();
			}
			if(returnCart.items.containsKey(saleorderitem.getProduct().getProductId())){
				saleorderitem = returnCart.items.get(saleorderitem.getProduct().getProductId());
				saleorderitem.setQuantity(quantity);
				saleorderitem.setPrice(Math.round(saleorderitem.getProduct().getPrice()*quantity * 100) / 100.0);
			}else{
				saleorderitem.setQuantity(quantity);
				saleorderitem.setPrice(Math.round(saleorderitem.getProduct().getPrice()*quantity * 100) / 100.0);
				returnCart.addProduct(saleorderitem.getProduct().getProductId(),saleorderitem);
			}
			session.put("returnCart", returnCart);
			jobj.put("success",true);
			jobj.put("returnTotalprice",returnCart.getTotalPrice());
			jobj.put("amount",returnCart.getItems().size());
		}
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	public void returnGetSalereturnitems() throws IOException{
		java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart returnCart = (Cart)session.get("returnCart");
    	if(returnCart!=null){
    		List saleordershows = new ArrayList();
    		for(Iterator it = returnCart.getItems().values().iterator();it.hasNext();){
    			Saleorderitem saleorderitem = (Saleorderitem)it.next();
    			Saleordershow saleordershow = new Saleordershow();
    			saleordershow.setPrice(saleorderitem.getProduct().getPrice());
    			saleordershow.setProductId(saleorderitem.getProduct().getProductId());
    			saleordershow.setProductName(saleorderitem.getProduct().getProductName());
    			saleordershow.setProducttypeName(saleorderitem.getProduct().getProducttype().getProducttypeName());
    			saleordershow.setQuantity(saleorderitem.getQuantity());
    			saleordershow.setTotalprice(saleorderitem.getPrice());
    			saleordershows.add(saleordershow);
    		}
    		HttpServletResponse response = ServletActionContext.getResponse();  
    		HttpServletRequest request = ServletActionContext.getRequest();  
    		JSONObject jobj = new JSONObject();//new一个JSON  
    		jobj.put("rows",saleordershows);
    		response.setCharacterEncoding("utf-8");
    		response.getWriter().write(jobj.toString());
    	}else{
    		List saleordershows = new ArrayList();
    		HttpServletResponse response = ServletActionContext.getResponse();  
            HttpServletRequest request = ServletActionContext.getRequest();  
    		JSONObject jobj = new JSONObject();//new一个JSON  
            jobj.put("rows",saleordershows);
            response.setCharacterEncoding("utf-8");
    		response.getWriter().write(jobj.toString());
    	}
	}
	public void returnSubmitRuturn() throws IOException{
		java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart returnCart = (Cart)session.get("returnCart");
    	Saleorder saleorder = (Saleorder)session.get("saleorder");
    	Salereturn salereturn = (Salereturn)session.get("salereturn");
    	double totalprice = new BigDecimal(saleorder.getTotalPrice()-quantity).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    	if(totalprice<0.5){
    		totalprice=0;
    	}
    	saleorder.setTotalPrice(totalprice);
    	salereturn.setReason(reason);
    	salereturn.setTotalPrice(quantity);
    	salereturn.setSaleorder(saleorder);
    	for(Iterator item = returnCart.getItems().values().iterator();item.hasNext();){
    		Saleorderitem returnSaleorderitem = (Saleorderitem)item.next();
    		Salereturnitem salereturnitem = new Salereturnitem();
    		salereturnitem.setPrice(returnSaleorderitem.getPrice());
    		salereturnitem.setProduct(returnSaleorderitem.getProduct());
    		salereturnitem.setQuantity(returnSaleorderitem.getQuantity());
    		salereturnitem.setSalereturn(salereturn);
    		salereturn.getSalereturnitems().add(salereturnitem);
    	}
    	saleorder.getSalereturns().add(salereturn);
    	saleordersService.updateSaleorder(saleorder);
    	for(Iterator item = returnCart.getItems().values().iterator();item.hasNext();){
    		Saleorderitem returnSaleorderitem = (Saleorderitem)item.next();
    		Saleorderitem saleorderitem = new Saleorderitem();
    		saleorderitem.setQuantity(saleordersService.getSaleorderitem(returnSaleorderitem.getSaleorderitemId()).getQuantity()-returnSaleorderitem.getQuantity());
    		saleorderitem.setPrice(saleorderitem.getQuantity()*returnSaleorderitem.getProduct().getPrice());
    		saleorderitem.setProduct(returnSaleorderitem.getProduct());
    		saleorderitem.setSaleorder(saleorder);
    		saleorderitem.setSaleorderitemId(returnSaleorderitem.getSaleorderitemId());
    		saleordersService.updateSaleorderitem(saleorderitem);
    	}
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("msg","退货成功");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	
	
	
	
	
	
	
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public SaleordersService getSaleordersService() {
		return saleordersService;
	}
	public void setSaleordersService(SaleordersService saleordersService) {
		this.saleordersService = saleordersService;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}