package action;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cart;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import service.UserService;
import util.Pager;
import vo.User;



public class UserAction extends ActionSupport{
    public User user;
    public int rows;
	public int page;
	public String order;
	public String sort;
	public int id;
	protected UserService userService;
	
	public String validateUser(){
		User user1 = userService.validateUser(user);
		java.util.Map<String, Object> session=ActionContext.getContext().getSession();
        session.put("user",user1);
		return SUCCESS;
	}
	public void loginAjax() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
		List loginUsers = userService.login(user);
		if(loginUsers.size()==0){
			jobj.put("success",false);
			jobj.put("msg","该用户不存在！！");
		}else if(((User)loginUsers.get(0)).getPassword().equals(user.getPassword())){
			User loginUser = (User)loginUsers.get(0);
			jobj.put("success",true);
			String msg = (loginUser.getUsername());
	        jobj.put("msg",msg+"登陆成功！！");
	        java.util.Map<String, Object> session=ActionContext.getContext().getSession();
	        session.put("user",loginUser);
	        jobj.put("position",loginUser.getPositionId());
		}else{
			jobj.put("success",false);
			jobj.put("msg","密码错误！！");
		}
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
	}
	public void getUsers() throws Exception{
		List allUsers = userService.browseUser();
		int total = allUsers.size();
		List users = userService.getUsers(page, rows, order, sort);
		this.toBeJson(users, total);
    }
	public void toBeJson(List list,int total) throws Exception{  
		
		
		
		/*java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart = (Cart)session.get("cart");
    	System.out.println("进来了");
    	System.out.println(cart.getItems().size());
    	System.out.println(cart.getItems().values().toString());
    	JsonConfig config1 = new JsonConfig();
        config1.setExcludes(new String[]{"saleorder","saleorderitemId","product","price"});//除去emps属性
    	JSONArray json1 = JSONArray.fromObject(cart.getItems(), config1);
    	System.out.println(json1.toString());*/
		
		
		
		
		
		
		
        HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
        JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("total",total);
        /*jobj.accumulate("total",total );//total代表一共有多少数据 */
		JsonConfig config = new JsonConfig();
        config.setExcludes(new String[]{"payments"});//除去emps属性
        /*config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);  */ 
		JSONArray json = JSONArray.fromObject(list, config);
		/*JSONArray rows =JSONArray.fromObject(list);
		System.out.println(rows.toString());*/
         jobj.accumulate("rows", json);
		/*jobj.put("rows", list);*/
       /* jobj.accumulate("rows", list);//row是代表显示的页的数据 */
        response.setCharacterEncoding("utf-8");//指定为utf-8  
        response.getWriter().write(jobj.toString());//转化为JSOn格式    
   }  
	public void registerUser() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
    	userService.registerUser(user);
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","注册成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
    }
	public void deleteUser() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","删除成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		userService.deleteUser(id);
	}
	public void modifyUser() throws IOException{
		HttpServletResponse response = ServletActionContext.getResponse();  
        HttpServletRequest request = ServletActionContext.getRequest();  
		JSONObject jobj = new JSONObject();//new一个JSON  
        jobj.put("success",true);
        jobj.put("msg","修改成功！！");
        response.setCharacterEncoding("utf-8");
		response.getWriter().write(jobj.toString());
		userService.modifyUser(user);
	}
	
	
	
	
	
	
	
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
    
	
	
}
