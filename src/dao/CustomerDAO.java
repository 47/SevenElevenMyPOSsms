package dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Customer;

/**
 * A data access object (DAO) providing persistence and search support for
 * Customer entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Customer
 * @author MyEclipse Persistence Tools
 */
public class CustomerDAO extends BaseHibernateDAO {
	private static final Log log = LogFactory.getLog(CustomerDAO.class);
	// property constants
	public static final String CUSTOMER_NO = "customerNo";
	public static final String CUSTOMER_NAME = "customerName";
	public static final String TELEPHONE = "telephone";
	public static final String ADDRESS = "address";

	protected void initDao() {
		// do nothing
	}

	public void save(Customer customer) {
		log.debug("saving Customer instance");
		try {
			Session session = getSession();
			Transaction transaction= session.beginTransaction(); 
			session.save(customer); 
            transaction.commit(); 
            session.close();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Customer customer) {
		log.debug("deleting Customer instance");
		try {
			Session session = getSession();
			Transaction transaction= session.beginTransaction(); 
			session.delete(customer);
			transaction.commit(); 
			session.close();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Customer findById(java.lang.Integer id) {
		log.debug("getting Customer instance with id: " + id);
		try {
			Session session = getSession();
			Customer instance = (Customer) session.get("vo.Customer", id);
			session.close();
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Customer instance) {
		log.debug("finding Customer instance by example");
		try {
			List results = getSession().createCriteria("vo.Customer")
					.add(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List findAll() {
		log.debug("finding all Customer instances");
		try {
			String queryString = "from Customer";
			Session session = getSession();
			session.clear();
			Query queryObject = session.createQuery(queryString);
			List customers=queryObject.list();
			session.close();
			return customers;
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	public List findByOption(int page,int rows,String order,String sort){
		String queryString = "from Customer order by "+sort+" "+order;
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		queryObject.setFirstResult((page-1)*rows);
		queryObject.setMaxResults(rows);
		List customers=queryObject.list();
		session.close();
		return customers;
	}
	
	public void update(Customer customer){
		Session session=getSession();
		session.clear();
		Transaction tx=session.beginTransaction();  
		session.update(customer);
	     /*String hql="update Customer set customerNo=?,customerName=?,telephone=?,address=? where customerId=? ";
	     Query query = session.createQuery(hql);
	     query.setParameter(0,customer.getCustomerNo());
	     query.setParameter(1,customer.getCustomerName());
	     query.setParameter(2,customer.getTelephone());
	     query.setParameter(3,customer.getAddress());
	     query.setParameter(4,customer.getCustomerId());
	     query.executeUpdate();*/
	     tx.commit();
	     session.close();
	}
	
	
	
	

	/*public List findByProperty(String propertyName, Object value) {
		log.debug("finding Customer instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Customer as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByCustomerNo(Object customerNo) {
		return findByProperty(CUSTOMER_NO, customerNo);
	}

	public List findByCustomerName(Object customerName) {
		return findByProperty(CUSTOMER_NAME, customerName);
	}

	public List findByTelephone(Object telephone) {
		return findByProperty(TELEPHONE, telephone);
	}

	public List findByAddress(Object address) {
		return findByProperty(ADDRESS, address);
	}


	public Customer merge(Customer detachedInstance) {
		log.debug("merging Customer instance");
		try {
			Customer result = (Customer) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Customer instance) {
		log.debug("attaching dirty Customer instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Customer instance) {
		log.debug("attaching clean Customer instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static CustomerDAO getFromApplicationContext(ApplicationContext ctx) {
		return (CustomerDAO) ctx.getBean("CustomerDAO");
	}*/
}