package dao;

import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Payment;
import vo.Saleorder;

/**
 * A data access object (DAO) providing persistence and search support for
 * Payment entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Payment
 * @author MyEclipse Persistence Tools
 */
public class PaymentDAO extends BaseHibernateDAO {
	public void save(Payment payment){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.save(payment); 
        transaction.commit(); 
        session.close();
	}
}