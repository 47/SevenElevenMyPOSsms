package dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Product;
import vo.Producttype;

/**
 * A data access object (DAO) providing persistence and search support for
 * Product entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Product
 * @author MyEclipse Persistence Tools
 */
public class ProductDAO extends BaseHibernateDAO {
	public void save(Product product){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.save(product); 
        transaction.commit(); 
        session.close();
	}
	public List findAll(){
		String queryString = "from Product";
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		List products=queryObject.list();
		session.close();
		return products;
	}
	public void delete(Product product){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.delete(product);
        transaction.commit(); 
        session.close();
	}
	public Product findById(java.lang.Integer id) {
		Session session = getSession();
		session.clear();
		Product instance = (Product) session.get("vo.Product", id);
		session.close();
		return instance;
	}
	public void update(Product product){
		Session session = getSession();
		session.clear();
		Transaction transaction= session.beginTransaction(); 
		session.update(product);
		transaction.commit(); 
        session.close();
	}
	public List findByOption(int page,int rows,String order,String sort){
		String queryString = "from Product order by "+sort+" "+order;
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		queryObject.setFirstResult((page-1)*rows);
		queryObject.setMaxResults(rows);
		List products=queryObject.list();
		session.close();
		return products;
	}
}