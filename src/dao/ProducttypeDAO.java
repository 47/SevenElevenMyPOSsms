package dao;

import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Customer;
import vo.Producttype;

/**
 * A data access object (DAO) providing persistence and search support for
 * Producttype entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see vo.Producttype
 * @author MyEclipse Persistence Tools
 */
public class ProducttypeDAO extends BaseHibernateDAO {

	public void save(Producttype producttype){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.save(producttype); 
        transaction.commit(); 
        session.close();
	}
	public void delete(Producttype producttype){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.delete(producttype);
        transaction.commit(); 
        session.close();
	}
	public List findAll(){
		String queryString = "from Producttype";
		Session session = getSession();
		session.clear();
		/*List producttypes=session.createCriteria("vo.Customer").list();*/
		Query queryObject = session.createQuery(queryString);
		List producttypes=queryObject.list();
		session.close();
		return producttypes;
	}
	public Producttype findById(java.lang.Integer id) {
		Session session = getSession();
		session.clear();
		Producttype instance = (Producttype) session.get("vo.Producttype", id);
		/*System.out.println(instance.getProducts().size());*/
		session.close();
		return instance;
	}
	public void update(Producttype producttype){
		Session session = getSession();
		session.clear();
		Transaction tx = session.beginTransaction();
		session.update(producttype);
		tx.commit();
		session.close();
	}
	public List findByOption(int page,int rows,String order,String sort){
		String queryString = "from Producttype order by "+sort+" "+order;
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		queryObject.setFirstResult((page-1)*rows);
		queryObject.setMaxResults(rows);
		List customers=queryObject.list();
		session.close();
		return customers;
	}
}