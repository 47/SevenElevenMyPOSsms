package dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Saleorder;

/**
 * A data access object (DAO) providing persistence and search support for
 * Product entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Product
 * @author MyEclipse Persistence Tools
 */
public class SaleorderDAO extends BaseHibernateDAO {
	public void save(Saleorder saleorder){
		Session session = getSession();
		Transaction transaction= session.beginTransaction(); 
		session.save(saleorder); 
        transaction.commit(); 
        session.close();
	}
	public List findAll(){
		String queryString = "from Saleorder";
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		List products=queryObject.list();
		session.close();
		return products;
	}
}