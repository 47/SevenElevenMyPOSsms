package dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Saleorder;
import vo.Saleorderitem;


/**
 * A data access object (DAO) providing persistence and search support for
 * Product entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Product
 * @author MyEclipse Persistence Tools
 */
public class SaleorderitemDAO extends BaseHibernateDAO {
	public List findBySaleorderId(java.lang.Integer saleorderId) {
		String queryString = "from Saleorderitem as model where model.saleorder.saleorderId = "+saleorderId;
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		List saleorderitems=queryObject.list();
		session.close();
		return saleorderitems;
	}
	public Saleorderitem findById(java.lang.Integer id) {
		Session session = getSession();
		Saleorderitem instance = (Saleorderitem) session.get("vo.Saleorderitem", id);
		session.close();
		return instance;
	}
	public void update(Saleorderitem saleorderitem){
		Session session = getSession();
		session.clear();
		Transaction transaction= session.beginTransaction(); 
		session.update(saleorderitem);
		transaction.commit(); 
        session.close();
	}
}