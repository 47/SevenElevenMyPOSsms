package dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Product;
import vo.Saleorder;
import vo.User;


	public class SaleordersDAO extends BaseHibernateDAO {

		public List findByOption(int page,int rows,String order,String sort/*,int id*/){
			/*if(id!=0){
				String queryString = "from Saleorder order by "+sort+" "+order;
			}*/
			String queryString = "from Saleorder order by "+sort+" "+order;
			Session session = getSession();
			session.clear();
			Query queryObject = session.createQuery(queryString);
			queryObject.setFirstResult((page-1)*rows);
			queryObject.setMaxResults(rows);
			List saleorders=queryObject.list();
			session.close();
			return saleorders;
		}
		public List findByMoreOption(int page,int rows,String order,String sort,String keyname,String keyword,String startdate,String enddate) throws ParseException{
			/*String queryString = "from Saleorder as model where model.customer.customerId= "+keyword+" and model.createDatetime>= :startdate and model.createDatetime<= :enddate order by "+sort+" "+order;*/
			String queryString = "from Saleorder as model where 1=1";
			if(!keyword.isEmpty()&&keyname.equals("customerId")){
			queryString = queryString+" and model.customer.customerId= "+keyword;}
			if(!keyword.isEmpty()&&keyname.equals("userId")){
				queryString = queryString+" and model.payment.user.userId= "+keyword;}
			if(!keyword.isEmpty()&&keyname.equals("saleorderNo")){
				queryString = queryString+" and model.saleorderNo like :saleorderNo ";}
			if(!startdate.isEmpty()){
			queryString = queryString+" and model.createDatetime>= :startdate";}
			if(!enddate.isEmpty()){
			queryString = queryString+" and model.createDatetime<= :enddate";}
			queryString = queryString+" order by "+sort+" "+order;
			Session session = getSession();
			session.clear();
			Query queryObject = session.createQuery(queryString);
			SimpleDateFormat myFmt1=new SimpleDateFormat("yyyy-MM-dd"); 
			if(!startdate.isEmpty()){
			Date date1 = myFmt1.parse(startdate);
			queryObject.setDate("startdate", date1);
			}
			if(!enddate.isEmpty()){
			Date date2 = myFmt1.parse(enddate);
			queryObject.setDate("enddate", date2);
			}
			if(!keyword.isEmpty()&&keyname.equals("saleorderNo")){
			queryObject.setString("saleorderNo", "%"+keyword+"%");}
			queryObject.setFirstResult((page-1)*rows);
			queryObject.setMaxResults(rows);
			List saleorders=queryObject.list();
			session.close();
			return saleorders;
		}
		public List findAll(){
			String queryString = "from Saleorder";
			Session session = getSession();
			session.clear();
			Query queryObject = session.createQuery(queryString);
			List saleorders=queryObject.list();
			session.close();
			return saleorders;
		}
		public List findAllByOption(String keyname,String keyword,String startdate,String enddate) throws ParseException{
			String queryString = "from Saleorder as model where 1=1";
			if(!keyword.isEmpty()&&keyname.equals("customerId")){
				queryString = queryString+" and model.customer.customerId= "+keyword;}
			if(!keyword.isEmpty()&&keyname.equals("userId")){
				queryString = queryString+" and model.payment.user.userId= "+keyword;}
			if(!keyword.isEmpty()&&keyname.equals("saleorderNo")){
				queryString = queryString+" and model.saleorderNo like :saleorderNo ";}
			if(!startdate.isEmpty()){
			queryString = queryString+" and model.createDatetime>= :startdate";}
			if(!enddate.isEmpty()){
			queryString = queryString+" and model.createDatetime<= :enddate";}
			Session session = getSession();
			session.clear();
			Query queryObject = session.createQuery(queryString);
			SimpleDateFormat myFmt1=new SimpleDateFormat("yyyy-MM-dd"); 
			if(!startdate.isEmpty()){
			Date date1 = myFmt1.parse(startdate);
			queryObject.setDate("startdate", date1);
			}
			if(!enddate.isEmpty()){
			Date date2 = myFmt1.parse(enddate);
			queryObject.setDate("enddate", date2);
			}
			if(!keyword.isEmpty()&&keyname.equals("saleorderNo")){
			queryObject.setString("saleorderNo", "%"+keyword+"%");}
			List saleorders=queryObject.list();
			session.close();
			return saleorders;
		}
		public Saleorder findById(java.lang.Integer id) {
			Session session = getSession();
			Saleorder instance = (Saleorder) session.get("vo.Saleorder", id);
			session.close();
			return instance;
		}
		public void update(Saleorder saleorder){
			Session session = getSession();
			session.clear();
			Transaction transaction= session.beginTransaction(); 
			session.update(saleorder);
			transaction.commit(); 
	        session.close();
		}
		public void save(Saleorder saleorder){
			Session session = getSession();
			Transaction transaction= session.beginTransaction(); 
			session.save(saleorder); 
	        transaction.commit(); 
	        session.close();
		}
	}