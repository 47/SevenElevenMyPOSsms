package dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Salereturn;

/**
 * A data access object (DAO) providing persistence and search support for
 * Salereturn entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see vo.Salereturn
 * @author MyEclipse Persistence Tools
 */
public class SalereturnDAO extends HibernateDaoSupport {
	private static final Log log = LogFactory.getLog(SalereturnDAO.class);
	// property constants
	public static final String SALERETURN_NO = "salereturnNo";
	public static final String REASON = "reason";
	public static final String TOTAL_PRICE = "totalPrice";

	protected void initDao() {
		// do nothing
	}

	public void save(Salereturn transientInstance) {
		log.debug("saving Salereturn instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Salereturn persistentInstance) {
		log.debug("deleting Salereturn instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Salereturn findById(java.lang.Integer id) {
		log.debug("getting Salereturn instance with id: " + id);
		try {
			Salereturn instance = (Salereturn) getHibernateTemplate().get(
					"vo.Salereturn", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Salereturn instance) {
		log.debug("finding Salereturn instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Salereturn instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Salereturn as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findBySalereturnNo(Object salereturnNo) {
		return findByProperty(SALERETURN_NO, salereturnNo);
	}

	public List findByReason(Object reason) {
		return findByProperty(REASON, reason);
	}

	public List findByTotalPrice(Object totalPrice) {
		return findByProperty(TOTAL_PRICE, totalPrice);
	}

	public List findAll() {
		log.debug("finding all Salereturn instances");
		try {
			String queryString = "from Salereturn";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Salereturn merge(Salereturn detachedInstance) {
		log.debug("merging Salereturn instance");
		try {
			Salereturn result = (Salereturn) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Salereturn instance) {
		log.debug("attaching dirty Salereturn instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Salereturn instance) {
		log.debug("attaching clean Salereturn instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static SalereturnDAO getFromApplicationContext(ApplicationContext ctx) {
		return (SalereturnDAO) ctx.getBean("SalereturnDAO");
	}
}