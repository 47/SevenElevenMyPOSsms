package dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.Salereturnitem;

/**
 * A data access object (DAO) providing persistence and search support for
 * Salereturnitem entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see vo.Salereturnitem
 * @author MyEclipse Persistence Tools
 */
public class SalereturnitemDAO extends HibernateDaoSupport {
	private static final Log log = LogFactory.getLog(SalereturnitemDAO.class);
	// property constants
	public static final String QUANTITY = "quantity";
	public static final String PRICE = "price";

	protected void initDao() {
		// do nothing
	}

	public void save(Salereturnitem transientInstance) {
		log.debug("saving Salereturnitem instance");
		try {
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Salereturnitem persistentInstance) {
		log.debug("deleting Salereturnitem instance");
		try {
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Salereturnitem findById(java.lang.Integer id) {
		log.debug("getting Salereturnitem instance with id: " + id);
		try {
			Salereturnitem instance = (Salereturnitem) getHibernateTemplate()
					.get("vo.Salereturnitem", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Salereturnitem instance) {
		log.debug("finding Salereturnitem instance by example");
		try {
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Salereturnitem instance with property: "
				+ propertyName + ", value: " + value);
		try {
			String queryString = "from Salereturnitem as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByQuantity(Object quantity) {
		return findByProperty(QUANTITY, quantity);
	}

	public List findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List findAll() {
		log.debug("finding all Salereturnitem instances");
		try {
			String queryString = "from Salereturnitem";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Salereturnitem merge(Salereturnitem detachedInstance) {
		log.debug("merging Salereturnitem instance");
		try {
			Salereturnitem result = (Salereturnitem) getHibernateTemplate()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Salereturnitem instance) {
		log.debug("attaching dirty Salereturnitem instance");
		try {
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Salereturnitem instance) {
		log.debug("attaching clean Salereturnitem instance");
		try {
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static SalereturnitemDAO getFromApplicationContext(
			ApplicationContext ctx) {
		return (SalereturnitemDAO) ctx.getBean("SalereturnitemDAO");
	}
}