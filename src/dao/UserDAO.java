package dao;

import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import vo.User;

/**
 * A data access object (DAO) providing persistence and search support for User
 * entities. Transaction control of the save(), update() and delete() operations
 * can directly support Spring container-managed transactions or they can be
 * augmented to handle user-managed Spring transactions. Each of these methods
 * provides additional information for how to configure it for the desired type
 * of transaction control.
 * 
 * @see vo.User
 * @author MyEclipse Persistence Tools
 */
public class UserDAO extends BaseHibernateDAO {
	public User validateUser(User user){
		Session session=getSession();
	     String hql="from User u where u.username=? and u.password=?";
	     Query query = session.createQuery(hql);
	     query.setParameter(0,user.getUsername());
	     query.setParameter(1,user.getPassword());
	     List users=query.list();
	     if(users.size()!=0){
	         User user1=(User)users.get(0);
	         session.close();
	         return user1;
	     }
	     session.close();
	     return null;
	}
	public List validateUsername(User user){
		Session session=getSession();
	     String hql="from User u where u.username=? ";
	     Query query = session.createQuery(hql);
	     query.setParameter(0,user.getUsername());
	     List users=query.list();
	         session.close();
	         return users;
	}
	public List findAll() {
		String queryString = "from User";
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		List customers=queryObject.list();
		session.close();
		return customers;
	}
	public List findByOption(int page,int rows,String order,String sort){
		String queryString = "from User order by "+sort+" "+order;
		Session session = getSession();
		session.clear();
		Query queryObject = session.createQuery(queryString);
		queryObject.setFirstResult((page-1)*rows);
		queryObject.setMaxResults(rows);
		List customers=queryObject.list();
		session.close();
		return customers;
	}
	public void save(User user) {
			Session session = getSession();
			Transaction transaction= session.beginTransaction(); 
			session.save(user); 
            transaction.commit(); 
            session.close();
	}

	public void delete(User user) {
			Session session = getSession();
			Transaction transaction= session.beginTransaction(); 
			session.delete(user);
			transaction.commit(); 
			session.close();	
	}
	public void update(User user){
		Session session=getSession();
		session.clear();
		Transaction tx=session.beginTransaction();  
		session.update(user);
	     /*String hql="update Customer set customerNo=?,customerName=?,telephone=?,address=? where customerId=? ";
	     Query query = session.createQuery(hql);
	     query.setParameter(0,customer.getCustomerNo());
	     query.setParameter(1,customer.getCustomerName());
	     query.setParameter(2,customer.getTelephone());
	     query.setParameter(3,customer.getAddress());
	     query.setParameter(4,customer.getCustomerId());
	     query.executeUpdate();*/
	     tx.commit();
	     session.close();
	}
	public User findById(java.lang.Integer id) {
			Session session = getSession();
			User instance = (User) session.get("vo.User", id);
			session.close();
			return instance;
	}
}