package model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import vo.Product;
import vo.Saleorderitem;






public class Cart {
     public Map<Integer,Saleorderitem> items;
     /*private double totalPrice;*/
     
     public Cart(){
    	 if(getItems()==null){
    		 setItems(new HashMap<Integer,Saleorderitem>());
    	 }
     }
     
     public void addProduct(Integer productid,Saleorderitem saleorderitem){
    	 if(getItems().containsKey(productid)){
    		 Saleorderitem _saleorderitem = getItems().get(productid);
    		 saleorderitem.setQuantity(_saleorderitem.getQuantity()+saleorderitem.getQuantity());
    		 saleorderitem.setPrice(_saleorderitem.getPrice()+saleorderitem.getPrice());
    		 getItems().put(productid, saleorderitem);
    	 }else{
    		 getItems().put(productid, saleorderitem);
    	 }
     }
     public void updateProduct(Integer productid,Saleorderitem saleorderitem){
    	 getItems().put(productid, saleorderitem);
     }
     public void deleteProduct(int productid){
    	 getItems().remove(productid);
     }
     public double getTotalPrice(){
    	 double totalPrice=0;
    	 for(Iterator it=items.values().iterator();it.hasNext();){
    		 Saleorderitem saleorderitem = (Saleorderitem)it.next();
    		 /*Product product = saleorderitem.getProduct();
    		 double quantity = saleorderitem.getQuantity();
    		 double price = product.getPrice();
    		 totalPrice=totalPrice+price*quantity;*/
    		 totalPrice = totalPrice+saleorderitem.getPrice();
    	 }
    	 return new BigDecimal(totalPrice).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
     }

	public Map<Integer,Saleorderitem> getItems() {
		return items;
	}

	public void setItems(Map<Integer,Saleorderitem> items) {
		this.items = items;
	}

	/*public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}*/
}
