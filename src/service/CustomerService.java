package service;

import java.util.List;

import vo.Customer;
import dao.CustomerDAO;



public class CustomerService {
	protected CustomerDAO customerDAO;
	
	public void registerCustomer(Customer customer) {
		customerDAO.save(customer);
	}
	public List browseCustomer(){
		return customerDAO.findAll();
	}
	public void deleteCustomer(Customer customer){
		customerDAO.delete(customer);
	}
	public void modifyCustomer(Customer customer){
		customerDAO.update(customer);
	}
	public Customer detailCustomer(Customer customer){
		return customerDAO.findById(customer.getCustomerId());
	}
	public Customer findById(int id){
		return customerDAO.findById(id);
	}
	public List getCustomers(int page,int rows,String order,String sort){
		return customerDAO.findByOption(page, rows, order, sort);
	}
	
	
	
	
	
	
	public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}
}
