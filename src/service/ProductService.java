package service;

import java.util.List;

import vo.Product;
import dao.ProductDAO;



public class ProductService{
	protected ProductDAO productDAO;

	public void addProduct(Product product){
		productDAO.save(product);
	}
	public List browseProduct(){
		return productDAO.findAll();
	}
	public void deleteProduct(int id){
		productDAO.delete(productDAO.findById(id));
	}
	public Product detailProduct(Product product){
		return productDAO.findById(product.getProductId());
	}
	public void modifyProduct(Product product){
		productDAO.update(product);
	}
	public List getProducts(int page,int rows,String order,String sort){
		return productDAO.findByOption(page, rows, order, sort);
	}
	public void deleteProductAjax(int id){
		productDAO.delete(productDAO.findById(id));
	}
	
	
	
	
	
	
	
	
	public ProductDAO getProductDAO() {
		return productDAO;
	}

	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	
}