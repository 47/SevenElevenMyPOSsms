package service;

import java.util.List;

import vo.Producttype;
import dao.ProducttypeDAO;




public class ProducttypeService{
	protected ProducttypeDAO producttypeDAO;

	
	public void addProducttype(Producttype producttype){
		producttypeDAO.save(producttype);
	}
	/*public void deleteProducttype(Producttype producttype){
		producttypeDAO.delete(producttypeDAO.findById(producttype.getProducttypeId()));
	}*/
	public void deleteProducttype(int id){
		producttypeDAO.delete(producttypeDAO.findById(id));
	}
	public List browseProducttype(){
		return producttypeDAO.findAll();
	}
	public void modifyProducttype(Producttype producttype){
		producttypeDAO.update(producttype);
	}
	public Producttype detailProducttype(Producttype producttype){
		return producttypeDAO.findById(producttype.getProducttypeId());
	}
	public List getProducttypes(int page,int rows,String order,String sort){
		return producttypeDAO.findByOption(page, rows, order, sort);
	}
	
	
	
	
	
	
	
	public ProducttypeDAO getProducttypeDAO() {
		return producttypeDAO;
	}

	public void setProducttypeDAO(ProducttypeDAO producttypeDAO) {
		this.producttypeDAO = producttypeDAO;
	}
}
