package service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import model.Cart;
import vo.Customer;
import vo.Payment;
import vo.Product;
import vo.Saleorder;
import vo.Saleorderitem;
import vo.User;

import com.opensymphony.xwork2.ActionContext;

import dao.CustomerDAO;
import dao.ProductDAO;
import dao.SaleorderDAO;
import dao.SaleorderitemDAO;




public class SaleorderService{
	protected SaleorderDAO saleorderDAO;
    protected SaleorderitemDAO saleorderitemDAO;
    protected ProductDAO productDAO;
    protected CustomerDAO customerDAO;
	
    
    
	
	public ArrayList addCart(int productId/*,double quantity*/){
		double quantity=1;
    	ArrayList arr=new ArrayList();	
    	Product addProduct = productDAO.findById(productId);
    	if(addProduct==null){
    		arr.add("商品不存在");
    		return arr;
    	}
    	Saleorderitem saleorderitem = new Saleorderitem();
    	saleorderitem.setProduct(addProduct);
    	saleorderitem.setQuantity(quantity);
    	saleorderitem.setPrice(Math.round(addProduct.getPrice()*quantity * 100) / 100.0);
    	
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart =(Cart)session.get("cart");
    	if(cart==null){
    		cart = new Cart();
    	}
    	cart.addProduct(productId,saleorderitem);
    	session.put("cart", cart);
    	saleorderitem=cart.getItems().get(productId);
    	arr.add(saleorderitem.getProduct().getProductId());
    	arr.add(saleorderitem.getProduct().getProductName());
    	arr.add(saleorderitem.getProduct().getProducttype().getProducttypeName());
    	arr.add(saleorderitem.getProduct().getPrice());
    	arr.add(saleorderitem.getPrice());
    	arr.add(saleorderitem.getQuantity());
    	arr.add(cart.getTotalPrice());
    	return arr;
    }
	public String clearCart(){
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart =(Cart)session.get("cart");
    	cart.getItems().clear();
    	session.remove("cart");
    	return "SUCCESS";
    }
	public ArrayList deleteCart(int productId){
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart =(Cart)session.get("cart");
    	if(cart==null){
    		cart = new Cart();
    	}
    	cart.deleteProduct(productId);
    	session.put("cart", cart);
    	ArrayList arr=new ArrayList();	
    	arr.add(productId);
    	arr.add(cart.getTotalPrice());
    	return arr;
    }
    public ArrayList updateCart(int productId,double quantity){
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart =(Cart)session.get("cart");
    	Saleorderitem saleorderitem=cart.getItems().get(productId);
    	saleorderitem.setPrice(Math.round(saleorderitem.getProduct().getPrice()*quantity * 100) / 100.0);
    	saleorderitem.setQuantity(quantity);
    	ArrayList arr=new ArrayList();	
    	arr.add(productId);
    	arr.add(saleorderitem.getPrice());
    	arr.add(saleorderitem.getQuantity());
    	arr.add(cart.getTotalPrice());
    	return arr;
    }
    public ArrayList checkout(int customerId){
    	ArrayList arr=new ArrayList();
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart = (Cart)session.get("cart");
    	Customer customer = customerDAO.findById(customerId);
    	session.put("customer", customer);
    	/*JSONArray json = JSONArray.fromObject((List)(cart.getItems().values()));*/
    	/*JSONObject json = JSONObject.fromObject(object)
    	System.out.println(json.toString());*/
    	/*System.out.println("杩欓噷閿欎簡锛�);
    	JSONArray json = JSONArray.fromObject(cart.getItems());
    	System.out.println(json.toString());*/
    	arr.add(cart.getTotalPrice());
    	arr.add(customer.getTelephone());
    	return arr;
    }
    public String pay(double realtotalprice){
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart = (Cart)session.get("cart");
    	Customer customer = (Customer)session.get("customer");
    	/*Customer customer = customerDAO.findById(customerId);*/
    	session.remove("customer");
    	if(cart==null||customer==null){
    		return "false";
    	}
    	Date date = new Date();    
    	SimpleDateFormat myFmt1=new SimpleDateFormat("yyMMddHHmmss"); 
    	String saleorderNo = myFmt1.format(date)+"-"+String.format("%03d", customer.getCustomerId());
    	Saleorder saleorder = new Saleorder();
    	saleorder.setCustomer(customer);
    	saleorder.setSaleorderNo(saleorderNo);
    	saleorder.setState(1);
    	saleorder.setTotalPrice(realtotalprice);
    	saleorder.setCreateDatetime(new Timestamp(date.getTime()));
    	for(Iterator item = cart.getItems().values().iterator();item.hasNext();){
    		Saleorderitem saleorderitem = (Saleorderitem)item.next();
    		saleorderitem.setSaleorder(saleorder);
    		saleorder.getSaleorderitems().add(saleorderitem);
    	}
    	Payment payment = new Payment();
    	payment.setAmount((double)cart.getItems().size());
    	payment.setCreateDatetime(new Timestamp(date.getTime()));
    	payment.setPaymentMethod(1);
    	payment.setSaleorder(saleorder);
    	User user = (User)session.get("user");
    	payment.setUser(user);
    	/*SimpleDateFormat myFmt2=new SimpleDateFormat("yyMMddHHmmss"); */
    	String paymentNo = myFmt1.format(date)+"-"+String.format("%03d", user.getUserId());
    	payment.setPaymentNo(paymentNo);
    	saleorder.setPayment(payment);
    	saleorderDAO.save(saleorder);
    	this.clearCart();
    	return "true";
    }
    public String saveOrder(){
    	java.util.Map<String, Object> session=ActionContext.getContext().getSession();
    	Cart cart = (Cart)session.get("cart");
    	/*Customer customer = (Customer)session.get("Customer");*/
    	Customer customer = (Customer)session.get("customer");
    	/*Customer customer = customerDAO.findById(customerId);*/
    	session.remove("customer");
    	if(cart==null||customer==null){
    		return "false";
    	}
    	Date date = new Date();    
    	SimpleDateFormat myFmt1=new SimpleDateFormat("yyMMddHHmmss"); 
    	String saleorderNo = myFmt1.format(date)+"-"+String.format("%03d", customer.getCustomerId());
    	Saleorder saleorder = new Saleorder();
    	saleorder.setCustomer(customer);
    	saleorder.setSaleorderNo(saleorderNo);
    	saleorder.setState(2);
    	saleorder.setTotalPrice(cart.getTotalPrice());
    	saleorder.setCreateDatetime(new Timestamp(date.getTime()));
    	
    	for(Iterator item = cart.getItems().values().iterator();item.hasNext();){
    		Saleorderitem saleorderitem = (Saleorderitem)item.next();
    		saleorderitem.setSaleorder(saleorder);
    		saleorder.getSaleorderitems().add(saleorderitem);
    	}
    	saleorderDAO.save(saleorder);
    	this.clearCart();	
    	return "true";
    }
    public List browseSaleorders(){
    	return saleorderDAO.findAll();
    }
    public ArrayList validateCustomer(int customerId){
    	Customer customer = customerDAO.findById(customerId);
    	ArrayList arr=new ArrayList();
    	if(customer!=null){
    		arr.add(customer.getCustomerName());
    		arr.add(customer.getTelephone());
    	}else{
    		arr.add("此用户不存在");
    	}
    	return arr;
    }
	
	
	
    /*public void addOrder(){
		System.out.println("Service");
		saleorderDAO.save();
	}*/
    
    
    
    
    
    
    
    
    
    public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}
    public ProductDAO getProductDAO() {
		return productDAO;
	}
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	public SaleorderitemDAO getSaleorderitemDAO() {
		return saleorderitemDAO;
	}

	public void setSaleorderitemDAO(SaleorderitemDAO saleorderitemDAO) {
		this.saleorderitemDAO = saleorderitemDAO;
	}

	public SaleorderDAO getSaleorderDAO() {
		return saleorderDAO;
	}

	public void setSaleorderDAO(SaleorderDAO saleorderDAO) {
		this.saleorderDAO = saleorderDAO;
	}
}