package service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import model.Cart;
import vo.Customer;
import vo.Payment;
import vo.Product;
import vo.Saleorder;
import vo.Saleorderitem;
import vo.User;

import com.opensymphony.xwork2.ActionContext;

import dao.CustomerDAO;
import dao.PaymentDAO;
import dao.ProductDAO;
import dao.SaleorderitemDAO;
import dao.SaleordersDAO;




public class SaleordersService{
	protected SaleordersDAO saleordersDAO;
	protected SaleorderitemDAO saleorderitemDAO;
    protected ProductDAO productDAO;
    protected CustomerDAO customerDAO;
    protected PaymentDAO paymentDAO;
    
    
    
    
	public List getSaleorders(int page,int rows,String order,String sort,String keyname,String keyword,String startdate,String enddate) throws ParseException{
		if(keyword==null&&startdate==null&&enddate==null){
			return saleordersDAO.findByOption(page, rows, order, sort);
	    }else{
	    	return saleordersDAO.findByMoreOption(page, rows, order, sort, keyname, keyword, startdate, enddate);
	    }
	}
    public List browseSaleorders(String keyname,String keyword,String startdate,String enddate) throws ParseException{
    	if(keyword==null&&startdate==null&&enddate==null){
    		return saleordersDAO.findAll();
    	}
    	return saleordersDAO.findAllByOption(keyname, keyword, startdate, enddate);
	}
    public Saleorder getSaleorder(int id){
    	return saleordersDAO.findById(id);
    }
    public void savePayment(Payment payment){
    	paymentDAO.save(payment);
    }
    public void updateSaleorder(Saleorder saleorder){
    	saleordersDAO.update(saleorder);
    }
    public List getSaleorderitems(int saleorderId){
		return saleorderitemDAO.findBySaleorderId(saleorderId);
	}
    public Saleorderitem getSaleorderitem(int saleorderitemId){
    	return saleorderitemDAO.findById(saleorderitemId);
    }
    public Customer getCustomer(int id){
    	return customerDAO.findById(id);
    }
    public Product getProduct(int id){
    	return productDAO.findById(id);
    }
    public void save(Saleorder saleorder){
    	saleordersDAO.save(saleorder);
    }
    public void updateSaleorderitem(Saleorderitem saleorderitem){
    	saleorderitemDAO.update(saleorderitem);
    }
    
    
    
    
    
    
    
    public PaymentDAO getPaymentDAO() {
		return paymentDAO;
	}
	public void setPaymentDAO(PaymentDAO paymentDAO) {
		this.paymentDAO = paymentDAO;
	}
    public CustomerDAO getCustomerDAO() {
		return customerDAO;
	}
	public void setCustomerDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}
    public ProductDAO getProductDAO() {
		return productDAO;
	}
	public void setProductDAO(ProductDAO productDAO) {
		this.productDAO = productDAO;
	}
	public SaleorderitemDAO getSaleorderitemDAO() {
		return saleorderitemDAO;
	}

	public void setSaleorderitemDAO(SaleorderitemDAO saleorderitemDAO) {
		this.saleorderitemDAO = saleorderitemDAO;
	}
	public SaleordersDAO getSaleordersDAO() {
		return saleordersDAO;
	}
	public void setSaleordersDAO(SaleordersDAO saleordersDAO) {
		this.saleordersDAO = saleordersDAO;
	}
	
}