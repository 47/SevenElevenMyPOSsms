package service;

import java.util.List;

import vo.Customer;
import vo.User;
import dao.UserDAO;




public class UserService {
	protected UserDAO userDAO;

	public User validateUser(User user){
		return userDAO.validateUser(user);
	}
	public List login(User user){
		return userDAO.validateUsername(user);
	}
	public List browseUser(){
		return userDAO.findAll();
	}
	public List getUsers(int page,int rows,String order,String sort){
		return userDAO.findByOption(page, rows, order, sort);
	}
	public void registerUser(User user) {
		userDAO.save(user);
	}
	public void deleteUser(int id){
		userDAO.delete(userDAO.findById(id));
	}
	public void modifyUser(User user){
		userDAO.update(user);
	}
	
	
	
	
	
	
	
	
	
	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	
	
}
