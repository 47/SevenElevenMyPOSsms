package util;

public class Pager {
	public int currentPage;
	public int pageSize=5;
	public int totalSize;
	public int totalPage;
	public boolean hasFirst;
	public boolean hasPrevious;
	public boolean hasNext;
	public boolean hasLast;
    public Pager(int currentPage,int totalSize){
    	this.setCurrentPage(currentPage);
    	this.setTotalSize(totalSize);
    	totalPage=totalSize/pageSize;
    	if((totalSize%pageSize)!=0)
    		totalPage++;
    	this.setTotalPage(totalPage);
    }
    public int getTotalPage(){
    	return totalPage;
    }
	public boolean isHasFirst() {
		if(getCurrentPage()==1)
			return false;
		return true;
	}
	public void setHasFirst(boolean hasFirst) {
		this.hasFirst = hasFirst;
	}
	public boolean isHasPrevious() {
		if(isHasFirst())
			return true;
		return false;
	}
	public void setHasPrevious(boolean hasPrevious) {
		this.hasPrevious = hasPrevious;
	}
	public boolean isHasNext() {
		if(isHasLast())
			return true;
		return false;
	}
	public void setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
	}
	public boolean isHasLast() {
		if(getCurrentPage()==getTotalPage())
			return false;
		return true;
	}
	public void setHasLast(boolean hasLast) {
		this.hasLast = hasLast;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalSize() {
		return totalSize;
	}
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
     
}
