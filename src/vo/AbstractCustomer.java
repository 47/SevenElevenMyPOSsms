package vo;

import java.util.HashSet;
import java.util.Set;

/**
 * AbstractCustomer entity provides the base persistence definition of the
 * Customer entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractCustomer implements java.io.Serializable {

	// Fields

	private Integer customerId;
	private String customerNo;
	private String customerName;
	private String telephone;
	private String address;
	private Set saleorders = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractCustomer() {
	}

	/** minimal constructor */
	public AbstractCustomer(String customerNo, String customerName) {
		this.customerNo = customerNo;
		this.customerName = customerName;
	}

	/** full constructor */
	public AbstractCustomer(String customerNo, String customerName,
			String telephone, String address, Set saleorders) {
		this.customerNo = customerNo;
		this.customerName = customerName;
		this.telephone = telephone;
		this.address = address;
		this.saleorders = saleorders;
	}

	// Property accessors

	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set getSaleorders() {
		return this.saleorders;
	}

	public void setSaleorders(Set saleorders) {
		this.saleorders = saleorders;
	}

}