package vo;

import java.sql.Timestamp;

/**
 * AbstractPayment entity provides the base persistence definition of the
 * Payment entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPayment implements java.io.Serializable {

	// Fields

	private Integer paymentId;
	private Saleorder saleorder;
	private User user;
	private String paymentNo;
	private Integer paymentMethod;
	private Double amount;
	private Timestamp createDatetime;

	// Constructors

	/** default constructor */
	public AbstractPayment() {
	}

	/** minimal constructor */
	public AbstractPayment(User user, String paymentNo, Integer paymentMethod,
			Double amount, Timestamp createDatetime) {
		this.user = user;
		this.paymentNo = paymentNo;
		this.paymentMethod = paymentMethod;
		this.amount = amount;
		this.createDatetime = createDatetime;
	}

	/** full constructor */
	public AbstractPayment(Saleorder saleorder, User user, String paymentNo,
			Integer paymentMethod, Double amount, Timestamp createDatetime) {
		this.saleorder = saleorder;
		this.user = user;
		this.paymentNo = paymentNo;
		this.paymentMethod = paymentMethod;
		this.amount = amount;
		this.createDatetime = createDatetime;
	}

	// Property accessors

	public Integer getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(Integer paymentId) {
		this.paymentId = paymentId;
	}

	public Saleorder getSaleorder() {
		return this.saleorder;
	}

	public void setSaleorder(Saleorder saleorder) {
		this.saleorder = saleorder;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPaymentNo() {
		return this.paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public Integer getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(Integer paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

}