package vo;

import java.util.HashSet;
import java.util.Set;

/**
 * AbstractProduct entity provides the base persistence definition of the
 * Product entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractProduct implements java.io.Serializable {

	// Fields

	private Integer productId;
	private Producttype producttype;
	private String productNo;
	private String productName;
	private String description;
	private String producttypeName;
	private Integer producttypeId;


	private Double price;
	private Set saleorderitems = new HashSet(0);
	private Set salereturnitems = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractProduct() {
	}

	/** minimal constructor */
	public AbstractProduct(Producttype producttype, String productNo,
			String productName, Double price) {
		this.producttype = producttype;
		this.productNo = productNo;
		this.productName = productName;
		this.price = price;
	}

	/** full constructor */
	public AbstractProduct(Producttype producttype, String productNo,
			String productName, String description, Double price,
			Set saleorderitems) {
		this.producttype = producttype;
		this.productNo = productNo;
		this.productName = productName;
		this.description = description;
		this.price = price;
		this.saleorderitems = saleorderitems;
	}

	// Property accessors
	
	
	
	
	public Integer getProducttypeId() {
		return producttypeId;
	}

	public void setProducttypeId(Integer producttypeId) {
		this.producttypeId = producttypeId;
	}
	public String getProducttypeName() {
		return producttypeName;
	}

	public void setProducttypeName(String producttypeName) {
		this.producttypeName = producttypeName;
	}
	public Integer getProductId() {
		return this.productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Producttype getProducttype() {
		return this.producttype;
	}

	public void setProducttype(Producttype producttype) {
		this.producttype = producttype;
	}

	public String getProductNo() {
		return this.productNo;
	}

	public void setProductNo(String productNo) {
		this.productNo = productNo;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Set getSaleorderitems() {
		return this.saleorderitems;
	}

	public void setSaleorderitems(Set saleorderitems) {
		this.saleorderitems = saleorderitems;
	}
	public Set getSalereturnitems() {
		return salereturnitems;
	}

	public void setSalereturnitems(Set salereturnitems) {
		this.salereturnitems = salereturnitems;
	}

}