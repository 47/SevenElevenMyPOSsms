package vo;

import java.util.HashSet;
import java.util.Set;

/**
 * AbstractProducttype entity provides the base persistence definition of the
 * Producttype entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractProducttype implements java.io.Serializable {

	// Fields

	private Integer producttypeId;
	private String producttypeName;
	private String description;
	private Set products = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractProducttype() {
	}

	/** minimal constructor */
	public AbstractProducttype(String producttypeName) {
		this.producttypeName = producttypeName;
	}

	/** full constructor */
	public AbstractProducttype(String producttypeName, String description,
			Set products) {
		this.producttypeName = producttypeName;
		this.description = description;
		this.products = products;
	}

	// Property accessors

	public Integer getProducttypeId() {
		return this.producttypeId;
	}

	public void setProducttypeId(Integer producttypeId) {
		this.producttypeId = producttypeId;
	}

	public String getProducttypeName() {
		return this.producttypeName;
	}

	public void setProducttypeName(String producttypeName) {
		this.producttypeName = producttypeName;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set getProducts() {
		return this.products;
	}

	public void setProducts(Set products) {
		this.products = products;
	}

}