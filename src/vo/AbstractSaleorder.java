package vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * AbstractSaleorder entity provides the base persistence definition of the
 * Saleorder entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSaleorder implements java.io.Serializable {

	// Fields

	private Integer saleorderId;
	private Customer customer;
	private String saleorderNo;
	private Double totalPrice;
	private Timestamp createDatetime;
	private Integer state;
	private Payment payment;
	public String customerName;
	public Timestamp payDatetime;
	private String username;
	private Double returnPrice;

	private Set saleorderitems = new HashSet(0);
	private Set salereturns = new HashSet(0);

	// Constructors


	/** default constructor */
	public AbstractSaleorder() {
	}

	/** minimal constructor */
	public AbstractSaleorder(String saleorderNo, Double totalPrice,
			Timestamp createDatetime, Integer state) {
		this.saleorderNo = saleorderNo;
		this.totalPrice = totalPrice;
		this.createDatetime = createDatetime;
		this.state = state;
	}

	/** full constructor */
	public AbstractSaleorder(Customer customer, String saleorderNo,
			Double totalPrice, Timestamp createDatetime, Integer state,
			Payment payment, Set saleorderitems) {
		this.customer = customer;
		this.saleorderNo = saleorderNo;
		this.totalPrice = totalPrice;
		this.createDatetime = createDatetime;
		this.state = state;
		this.payment = payment;
		this.saleorderitems = saleorderitems;
	}

	// Property accessors

	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public Timestamp getPayDatetime() {
		return payDatetime;
	}

	public void setPayDatetime(Timestamp payDatetime) {
		this.payDatetime = payDatetime;
	}
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getSaleorderId() {
		return this.saleorderId;
	}

	public void setSaleorderId(Integer saleorderId) {
		this.saleorderId = saleorderId;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getSaleorderNo() {
		return this.saleorderNo;
	}

	public void setSaleorderNo(String saleorderNo) {
		this.saleorderNo = saleorderNo;
	}

	public Double getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Payment getPayment() {
		return this.payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Set getSaleorderitems() {
		return this.saleorderitems;
	}

	public void setSaleorderitems(Set saleorderitems) {
		this.saleorderitems = saleorderitems;
	}
	public Set getSalereturns() {
		return salereturns;
	}

	public void setSalereturns(Set salereturns) {
		this.salereturns = salereturns;
	}
	public Double getReturnPrice() {
		return returnPrice;
	}

	public void setReturnPrice(Double returnPrice) {
		this.returnPrice = returnPrice;
	}
}