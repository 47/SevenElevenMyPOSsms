package vo;

/**
 * AbstractSaleorderitem entity provides the base persistence definition of the
 * Saleorderitem entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSaleorderitem implements java.io.Serializable {

	// Fields

	private Integer saleorderitemId;
	private Saleorder saleorder;
	private Product product;
	private Double quantity;
	private Double price;

	// Constructors

	/** default constructor */
	public AbstractSaleorderitem() {
	}

	/** minimal constructor */
	public AbstractSaleorderitem(Double quantity, Double price) {
		this.quantity = quantity;
		this.price = price;
	}

	/** full constructor */
	public AbstractSaleorderitem(Saleorder saleorder, Product product,
			Double quantity, Double price) {
		this.saleorder = saleorder;
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}

	// Property accessors

	public Integer getSaleorderitemId() {
		return this.saleorderitemId;
	}

	public void setSaleorderitemId(Integer saleorderitemId) {
		this.saleorderitemId = saleorderitemId;
	}

	public Saleorder getSaleorder() {
		return this.saleorder;
	}

	public void setSaleorder(Saleorder saleorder) {
		this.saleorder = saleorder;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}