package vo;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * AbstractSalereturn entity provides the base persistence definition of the
 * Salereturn entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSalereturn implements java.io.Serializable {

	// Fields

	private Integer salereturnId;
	private Saleorder saleorder;
	private String salereturnNo;
	private String reason;
	private Double totalPrice;
	private Timestamp createDatetime;
	private Set salereturnitems = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractSalereturn() {
	}

	/** minimal constructor */
	public AbstractSalereturn(Saleorder saleorder, String salereturnNo,
			Double totalPrice, Timestamp createDatetime) {
		this.saleorder = saleorder;
		this.salereturnNo = salereturnNo;
		this.totalPrice = totalPrice;
		this.createDatetime = createDatetime;
	}

	/** full constructor */
	public AbstractSalereturn(Saleorder saleorder, String salereturnNo,
			String reason, Double totalPrice, Timestamp createDatetime,
			Set salereturnitems) {
		this.saleorder = saleorder;
		this.salereturnNo = salereturnNo;
		this.reason = reason;
		this.totalPrice = totalPrice;
		this.createDatetime = createDatetime;
		this.salereturnitems = salereturnitems;
	}

	// Property accessors

	public Integer getSalereturnId() {
		return this.salereturnId;
	}

	public void setSalereturnId(Integer salereturnId) {
		this.salereturnId = salereturnId;
	}

	public Saleorder getSaleorder() {
		return this.saleorder;
	}

	public void setSaleorder(Saleorder saleorder) {
		this.saleorder = saleorder;
	}

	public String getSalereturnNo() {
		return this.salereturnNo;
	}

	public void setSalereturnNo(String salereturnNo) {
		this.salereturnNo = salereturnNo;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getTotalPrice() {
		return this.totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public Set getSalereturnitems() {
		return this.salereturnitems;
	}

	public void setSalereturnitems(Set salereturnitems) {
		this.salereturnitems = salereturnitems;
	}

}