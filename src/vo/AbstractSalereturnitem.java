package vo;

/**
 * AbstractSalereturnitem entity provides the base persistence definition of the
 * Salereturnitem entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractSalereturnitem implements java.io.Serializable {

	// Fields

	private Integer salereturnitemId;
	private Salereturn salereturn;
	private Product product;
	private Double quantity;
	private Double price;

	// Constructors

	/** default constructor */
	public AbstractSalereturnitem() {
	}

	/** full constructor */
	public AbstractSalereturnitem(Salereturn salereturn, Product product,
			Double quantity, Double price) {
		this.salereturn = salereturn;
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}

	// Property accessors

	public Integer getSalereturnitemId() {
		return this.salereturnitemId;
	}

	public void setSalereturnitemId(Integer salereturnitemId) {
		this.salereturnitemId = salereturnitemId;
	}

	public Salereturn getSalereturn() {
		return this.salereturn;
	}

	public void setSalereturn(Salereturn salereturn) {
		this.salereturn = salereturn;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}