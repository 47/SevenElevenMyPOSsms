package vo;

import java.util.HashSet;
import java.util.Set;

/**
 * AbstractUser entity provides the base persistence definition of the User
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractUser implements java.io.Serializable {

	// Fields

	private Integer userId;
	private String username;
	private String password;
	private Short positionId;
	private Short discount;
	private Set payments = new HashSet(0);

	// Constructors

	/** default constructor */
	public AbstractUser() {
	}

	/** minimal constructor */
	public AbstractUser(String username, String password, Short positionId,
			Short discount) {
		this.username = username;
		this.password = password;
		this.positionId = positionId;
		this.discount = discount;
	}

	/** full constructor */
	public AbstractUser(String username, String password, Short positionId,
			Short discount, Set payments) {
		this.username = username;
		this.password = password;
		this.positionId = positionId;
		this.discount = discount;
		this.payments = payments;
	}

	// Property accessors

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getPositionId() {
		return this.positionId;
	}

	public void setPositionId(Short positionId) {
		this.positionId = positionId;
	}

	public Short getDiscount() {
		return this.discount;
	}

	public void setDiscount(Short discount) {
		this.discount = discount;
	}

	public Set getPayments() {
		return this.payments;
	}

	public void setPayments(Set payments) {
		this.payments = payments;
	}

}