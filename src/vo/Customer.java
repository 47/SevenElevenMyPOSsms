package vo;

import java.util.Set;

/**
 * Customer entity. @author MyEclipse Persistence Tools
 */
public class Customer extends AbstractCustomer implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Customer() {
	}

	/** minimal constructor */
	public Customer(String customerNo, String customerName) {
		super(customerNo, customerName);
	}

	/** full constructor */
	public Customer(String customerNo, String customerName, String telephone,
			String address, Set saleorders) {
		super(customerNo, customerName, telephone, address, saleorders);
	}

}
