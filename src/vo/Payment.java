package vo;

import java.sql.Timestamp;

/**
 * Payment entity. @author MyEclipse Persistence Tools
 */
public class Payment extends AbstractPayment implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Payment() {
	}

	/** minimal constructor */
	public Payment(User user, String paymentNo, Integer paymentMethod,
			Double amount, Timestamp createDatetime) {
		super(user, paymentNo, paymentMethod, amount, createDatetime);
	}

	/** full constructor */
	public Payment(Saleorder saleorder, User user, String paymentNo,
			Integer paymentMethod, Double amount, Timestamp createDatetime) {
		super(saleorder, user, paymentNo, paymentMethod, amount, createDatetime);
	}

}
