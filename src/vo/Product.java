package vo;

import java.util.Set;

/**
 * Product entity. @author MyEclipse Persistence Tools
 */
public class Product extends AbstractProduct implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Product() {
	}

	/** minimal constructor */
	public Product(Producttype producttype, String productNo,
			String productName, Double price) {
		super(producttype, productNo, productName, price);
	}

	/** full constructor */
	public Product(Producttype producttype, String productNo,
			String productName, String description, Double price,
			Set saleorderitems) {
		super(producttype, productNo, productName, description, price,
				saleorderitems);
	}

}
