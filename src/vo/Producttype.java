package vo;

import java.util.Set;

/**
 * Producttype entity. @author MyEclipse Persistence Tools
 */
public class Producttype extends AbstractProducttype implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Producttype() {
	}

	/** minimal constructor */
	public Producttype(String productTypeName) {
		super(productTypeName);
	}

	/** full constructor */
	public Producttype(String productTypeName, String description, Set products) {
		super(productTypeName, description, products);
	}

}
