package vo;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Product entity. @author MyEclipse Persistence Tools
 */
public class Receipt  implements java.io.Serializable {
	private String productName;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	private Double productPrice;
	public Double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
	private Double quantity;
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	private Double price;
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	private Timestamp returnDatetime;

	
	public Timestamp getReturnDatetime() {
		return returnDatetime;
	}
	public void setReturnDatetime(Timestamp returnDatetime) {
		this.returnDatetime = returnDatetime;
	}
	public Receipt() {
	}

}
