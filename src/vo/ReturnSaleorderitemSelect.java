package vo;

import java.util.Set;

/**
 * Product entity. @author MyEclipse Persistence Tools
 */
public class ReturnSaleorderitemSelect  implements java.io.Serializable {
	protected int saleorderitemId;
	protected String productName;

	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getSaleorderitemId() {
		return saleorderitemId;
	}
	public void setSaleorderitemId(int saleorderitemId) {
		this.saleorderitemId = saleorderitemId;
	}



	public ReturnSaleorderitemSelect() {
	}

}
