package vo;

import java.sql.Timestamp;
import java.util.Set;

import vo.Payment;

/**
 * Saleorder entity. @author MyEclipse Persistence Tools
 */
public class Saleorder extends AbstractSaleorder implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Saleorder() {
	}

	/** minimal constructor */
	public Saleorder(String saleorderNo, Double totalPrice,
			Timestamp createDatetime, Integer state) {
		super(saleorderNo, totalPrice, createDatetime, state);
	}

	/** full constructor */
	public Saleorder(Customer customer, String saleorderNo,
			Double totalPrice, Timestamp createDatetime, Integer state,
			Payment payment, Set saleorderitems) {
		super(customer, saleorderNo, totalPrice, createDatetime, state, payment, saleorderitems);
	}

}
