package vo;

/**
 * Saleorderitem entity. @author MyEclipse Persistence Tools
 */
public class Saleorderitem extends AbstractSaleorderitem implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Saleorderitem() {
	}

	/** full constructor */
	public Saleorderitem(Saleorder saleorder, Product product, Double quantity,
			Double price) {
		super(saleorder, product, quantity, price);
	}

}
