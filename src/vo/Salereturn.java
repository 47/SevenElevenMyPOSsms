package vo;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Salereturn entity. @author MyEclipse Persistence Tools
 */
public class Salereturn extends AbstractSalereturn implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Salereturn() {
	}

	/** minimal constructor */
	public Salereturn(Saleorder saleorder, String salereturnNo,
			Double totalPrice, Timestamp createDatetime) {
		super(saleorder, salereturnNo, totalPrice, createDatetime);
	}

	/** full constructor */
	public Salereturn(Saleorder saleorder, String salereturnNo, String reason,
			Double totalPrice, Timestamp createDatetime, Set salereturnitems) {
		super(saleorder, salereturnNo, reason, totalPrice, createDatetime,
				salereturnitems);
	}

}
