package vo;

/**
 * Salereturnitem entity. @author MyEclipse Persistence Tools
 */
public class Salereturnitem extends AbstractSalereturnitem implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Salereturnitem() {
	}

	/** full constructor */
	public Salereturnitem(Salereturn salereturn, Product product,
			Double quantity, Double price) {
		super(salereturn, product, quantity, price);
	}

}
