package vo;

import java.util.Set;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
public class User extends AbstractUser implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String username, String password, Short positionId,
			Short discount) {
		super(username, password, positionId, discount);
	}

	/** full constructor */
	public User(String username, String password, Short positionId,
			Short discount, Set payments) {
		super(username, password, positionId, discount, payments);
	}

}
